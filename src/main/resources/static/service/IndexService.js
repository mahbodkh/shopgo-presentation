'use strict';

// demoApp.factory('IndexService',
//     ["$http", "CONSTANTS", function ($http, CONSTANTS) {

angular.module('shopgo.services', []).factory('IndexService',
    ["$http", "CONSTANTS", function ($http, CONSTANTS) {


        var service = {};

        service.getProductList = function () {
            return $http.get(CONSTANTS.BASE + CONSTANTS.PRODUCT_SERVICE_LIST);
            // console.log(CONSTANTS.BASE + CONSTANTS.PRODUCT_SERVICE_LIST);
        };

        return service;
    }]);
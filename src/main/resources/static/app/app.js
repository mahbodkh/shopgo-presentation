'use strict'

// var app = angular.module('crudApp',['ui.router','ngStorage']);
//
// app.constant('urls', {
//     BASE: 'http://localhost:8085/',
//     USER_SERVICE_API : 'http://localhost:8085/user/',
//     PRODUCT_SERVICE_API : 'http://localhost:8085/product/'
// });


var demoApp = angular.module('shopgo', ['ui.bootstrap', 'shopgo.controllers',
    'shopgo.services']);
demoApp.constant("CONSTANTS", {
    BASE: 'http://localhost:8085',

    //product
    PRODUCT_ADD:    '/product/add/',            //POST
    PRODUCT_ALL:    '/product/all/',            //GET
    PRODUCT_GET:    '/product/',                //GET
    PRODUCT_REMOVE: '/product/remove/',          //DELETE
    PRODUCT_EDIT:   '/product/edit/',            //POST


    //search


    //user
    getUserByIdUrl: "/user/getUser",
    getAllUsers: "/user/getAllUsers",
    saveUser: "/user/saveUser"
});

// app.config(['$stateProvider', '$urlRouterProvider',
//     function($stateProvider, $urlRouterProvider) {
//
//         $stateProvider
//             .state('home', {
//                 url: '/',
//                 templateUrl: 'partials/list',
//                 controller:'UserController',
//                 controllerAs:'ctrl',
//                 resolve: {
//                     users: function ($q, UserService) {
//                         console.log('Load all users');
//                         var deferred = $q.defer();
//                         UserService.loadAllUsers().then(deferred.resolve, deferred.resolve);
//                         return deferred.promise;
//                     }
//                 }
//             });
//         $urlRouterProvider.otherwise('/');
//     }]);


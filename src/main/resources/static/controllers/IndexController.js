'use strict';

// demoApp.controller("IndexController", ["$scope", "IndexService",


var module = angular.module('shopgo.controllers', []);
module.controller("IndexController", ["$scope", "IndexService",


    function ($scope, IndexService) {

        $scope.productDto = {
            productId: null,
            productName: null

        };
        // IndexService.getProductList().then(function (value) {
        //     console.log("works");
        //     $scope.allProduct = value.data;
        //
        //
        // }, function (reason) {
        //     console.log("error occured");
        // }, function (value) {
        //     console.log("no callback");
        // });
        //
        // IndexService.getProductList().then(function ($scope, $http) {
        //     console.log($http);
        //     console.log($scope);
        //     $http({
        //         url: CONSTANTS.BASE + CONSTANTS.PRODUCT_ALL,
        //         method: "GET",
        //         // data: postData,
        //         headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        //     }).success(function (data, status, headers, config) {
        //         $scope.productDto = data; // assign  $scope.persons here as promise is resolved here
        //     }).error(function (data, status, headers, config) {
        //         $scope.status = status;
        //     });
        //
        // });

        IndexService.getProductList().then(function ($http) {
            $http({
                method: 'GET',
                // url: 'api/url-api'
                // url: CONSTANTS.BASE + CONSTANTS.PRODUCT_ALL,
                url: 'http://localhost:8085/product/all/',
                headers: {
                    'Accept': 'application/json',
                    // 'Accept': '*/*',
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
            }).then(function (response) {
                console.log(response);
            }, function (error) {
                console.log(error);
            });
        });

    }]);

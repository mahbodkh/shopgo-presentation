package app.store.service;

import app.store.persistance.SearchRepository;
import org.bson.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SearchService {

    private SearchRepository searchRepository;

    @Autowired
    public SearchService(SearchRepository searchRepository) {
        this.searchRepository = searchRepository;
    }


    public void doSearch(String keyword, Integer limit) {
        searchRepository.doSearch(keyword, limit);
    }

    public List<Document> doSearch(String keyword) {
        return searchRepository.doSearch(keyword);
    }


}

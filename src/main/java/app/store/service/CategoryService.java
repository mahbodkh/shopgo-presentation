package app.store.service;

import app.store.controller.rest.error.NotFoundException;
import app.store.controller.rest.error.UpdateException;
import app.store.persistance.CategoryRepository;
import app.store.persistance.domain.Category;
import com.mongodb.client.result.UpdateResult;
import org.bson.Document;
import org.bson.types.ObjectId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CategoryService {

    private final Logger log = LoggerFactory.getLogger(CategoryService.class);
    private long now = System.currentTimeMillis();
    private CategoryRepository categoryRepository;

    @Autowired
    public CategoryService(CategoryRepository categoryRepository) {
        this.categoryRepository = categoryRepository;
    }


    public Category add(Category category) {
        log.debug("This is from add Category Service : {} ", category);
        List<Document> categoryRepositoryList = categoryRepository.getDataByNameOrCode(category);
        if (categoryRepositoryList.size() != 0)
            throw new NotFoundException("the category object exist in database, please change name or code of your product!", "CategoryService add", category.toString());
        Document categoryDocument = categoryRepository.getCategoryDocument(category);
        Document insert = categoryRepository.insert(categoryDocument);
        return categoryRepository.getCategoryEntity(insert);
    }


    public Category update(Category category) {
        log.debug("This is from update Category Service : {} ", category);
        Document categoryEditDocument = categoryRepository.getCategoryEditDocument(category);
        UpdateResult update = categoryRepository.update(categoryEditDocument);
        if (!update.wasAcknowledged())
            throw new UpdateException("Cannot update to product in mongo");
        return categoryRepository.getCategoryEntity(categoryEditDocument);
    }


//    public DeleteResult remove(String id) {
//        log.debug("This is from remove Category Service : {} ", id);
//        return null;
//    }
//
//    public CategoryDTO addSubCategory(String categoryId, CategoryDTO subCategoryDTO) {
//        Document category = null;
//        ObjectId objectId = new ObjectId(categoryId);
//        List<Document> categoryList = categoryRepository.getDataById(objectId);
//        if (categoryList == null) {
//            throw new NotFoundException("the category id not found ", "addSubCategory Service", category.toString());
//        }
//
//        Category categoryEntity = categoryRepository.getCategoryEntity(subCategoryDTO);
//        Document categoryDoc = categoryRepository.getCategoryDocument(categoryEntity);
//        categoryRepository.addSubCategory(objectId, categoryDoc);
//
//        return null;
//    }
//
//
//    public List<CategoryDTO> findAllSubCategory(String categoryId) {
//        log.debug("This is from find All Sub Categories Service : {} ", categoryId);
//
//        List<Document> categoryList = categoryRepository.getDataById(new ObjectId(categoryId));
//        if (categoryList == null)
//            throw new NotFoundException("", "", "");
//        Category categoryEntity = null;
//        CategoryDTO categoryDTO = null;
//        List<CategoryDTO> list = new ArrayList<>();
//        for (Document document : categoryList) {
//            categoryEntity = categoryRepository.getCategoryEntity(document);
//            categoryDTO = categoryRepository.getCategoryDTO(categoryEntity);
//            list.add(categoryDTO);
//        }
//        return list;
//    }
//
//
//    public CategoryDTO updateSubCategory(String cat, String sub, CategoryDTO subCategoryDto) {
//        ObjectId catId = new ObjectId(cat);
//        ObjectId subId = new ObjectId(sub);
//        Category subCategoryEntity = categoryRepository.getCategoryEntity(subCategoryDto);
//        Document subCategoryDocument = categoryRepository.getSubCategoryDocument(subCategoryEntity);
//        Document subcategory = categoryRepository.getDataBySubIdCatId(catId, subId, subCategoryDocument);
//        //todo under this line is note tested;
//        Category categoryEntity = categoryRepository.getCategoryEntity(subcategory);
//        return categoryRepository.getCategoryDTO(categoryEntity);
//    }

    public UpdateResult removeSubCategory(String cat, String sub) {
        log.debug("This is from remove Category Service : {} {}", cat, sub);
        if (cat != null && sub != null) {
            return categoryRepository.removeSubCategory(new ObjectId(cat), new ObjectId(sub));
        }
        throw new NotFoundException("the category id or sub category id is null", "addSubCategory Service", cat);
    }
}

package app.store.service;

import app.store.controller.rest.error.NotFoundException;
import app.store.controller.rest.error.UpdateException;
import app.store.persistance.CartRepository;
import app.store.persistance.ProductRepository;
import app.store.persistance.UserRepository;
import app.store.persistance.domain.Cart;
import com.mongodb.client.result.DeleteResult;
import com.mongodb.client.result.UpdateResult;
import org.bson.Document;
import org.bson.types.ObjectId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class CartService {

    private final Logger log = LoggerFactory.getLogger(CartService.class);


    private CartRepository cartRepository;
    private ProductRepository productRepository;
    private UserRepository userRepository;
    private long now = System.currentTimeMillis();

    @Autowired
    public CartService(CartRepository cartRepository, ProductRepository productRepository, UserRepository userRepository) {
        this.cartRepository = cartRepository;
        this.productRepository = productRepository;
        this.userRepository = userRepository;
    }

    public Cart add(Cart cart) {
        Document user_doc = null;
        List<Document> userList = userRepository.getDataById(new ObjectId(cart.getUserId()));
        if (userList.size() == 0)
            throw new NotFoundException("you are not true user: --> " + cart.getUserId(), "CardService add", cart.getUserId());
        user_doc = userList.get(0);

        ObjectId userObjectId = new ObjectId(cart.getUserId());
        Document product = null;
        List<Document> productListInsert = new ArrayList<>();
        Integer totalPrice = 0;
        for (String productId : cart.getProducts()) {
            if (!productRepository.exists(ProductRepository.Fields.ID.getValue(), new ObjectId(productId)))
                throw new NotFoundException("the product is not in your store: --> " + productId, "CardService add", productId);
            List<Document> productList = productRepository.getDataById(new ObjectId(productId));
            product = productList.get(0);

            Document pro_doc = new Document();
            pro_doc.put(ProductRepository.Fields.ID.getValue(), product.getObjectId(ProductRepository.Fields.ID.getValue()));
            pro_doc.put(ProductRepository.Fields.NAME.getValue(), product.getString(ProductRepository.Fields.NAME.getValue()));
            pro_doc.put(ProductRepository.Fields.PRICE.getValue(), product.getString(ProductRepository.Fields.PRICE.getValue()));
            productListInsert.add(pro_doc);
            //calculate all prices
            totalPrice += Integer.valueOf(product.getString(ProductRepository.Fields.PRICE.getValue()));
        }
        Document card_doc = new Document();
        ObjectId orderId = new ObjectId();
        card_doc.put(CartRepository.Fields.ID.getValue(), orderId);
        card_doc.put(CartRepository.Fields.USER_ID.getValue(), userObjectId);
        card_doc.put(CartRepository.Fields.START_ORDER_TIME.getValue(), now);
        card_doc.put(CartRepository.Fields.TOTAL_PRICE.getValue(), String.valueOf(totalPrice));
        card_doc.put(CartRepository.Fields.TOTAL_SHOP_CARD.getValue(), productListInsert.size());
        card_doc.put(CartRepository.Fields.PRODUCTS.getValue(), productListInsert);
        Document insertedOrder = cartRepository.insert(card_doc);

        log.debug("Finish The Add Product to Cart: ", totalPrice);
        return cartRepository.getOrderEntity(insertedOrder);
    }

    public Cart get(String cardId) {
        ObjectId cardObjectId = new ObjectId(cardId);
        if (userRepository.exists(CartRepository.Fields.ID.getValue(), cardObjectId))
            throw new NotFoundException("your cardId is not true : --> " + cardId, "CardService get", cardId);

        List<Document> cardList = cartRepository.getDataById(cardObjectId);
        if (cardList == null && cardList.size() == 0)
            throw new NotFoundException("your cardId is invalid : --> " + cardId, "CardService getProduct", cardId);

        Document card_doc = cardList.get(0);
        Cart orderEntity = cartRepository.getOrderEntity(card_doc);
        return orderEntity;
    }


    public void remove(String cardId) {
        if (!userRepository.exists(CartRepository.Fields.ID.getValue(), new ObjectId(cardId)))
            throw new NotFoundException("your cardId is not true : --> " + cardId, "CardService remove", cardId);
        DeleteResult remove = cartRepository.remove(new ObjectId(cardId));
        if (!remove.wasAcknowledged())
            throw new UpdateException("Cannot remove your card from database");
    }


    public void edit(Cart cart) {
        if (!userRepository.exists(UserRepository.Fields.ID.getValue(), new ObjectId(cart.getUserId())))
            throw new NotFoundException("you are not true user: --> " + cart.getUserId(), "CardService add", cart.getUserId());

        ObjectId cardObjectId = new ObjectId(cart.getId());
        if (!cartRepository.exists(CartRepository.Fields.ID.getValue(), cardObjectId))
            throw new NotFoundException("your order list id is invalid: --> " + cart.getId(), "CardService edit", cart.toString());

        for (String productId : cart.getProducts()) {
            if (!productRepository.exists(ProductRepository.Fields.ID.getValue(), new ObjectId(productId))) {
                throw new NotFoundException("your product is invalid: --> " + productId, "CardService edit", productId);
            }
        }
        Document editDocument = cartRepository.getEditDocument(cart);
        UpdateResult update = cartRepository.update(cardObjectId, editDocument);
        if (!update.wasAcknowledged())
            throw new UpdateException("Cannot update to cart in database");
    }

    public void addProduct(String cardId, String productId) {
        ObjectId cardObjectId = new ObjectId(cardId);
        ObjectId productObjectId = new ObjectId(productId);

        List<Document> cadList = cartRepository.findByCardIdAndProductId(cardObjectId, productObjectId);
        Document card_doc = null;
        if (cadList != null && cadList.size() != 0)
            throw new NotFoundException("your card id or product id is before persist in database  : --> " + cardId + " " + " " + productId
                    , "CardService addProduct"
                    , cardId + " " + productId);

        List<Document> productList = productRepository.getDataById(productObjectId);
        if (productList == null)
            throw new NotFoundException("your productId is invalid : --> " + productId, "CardService addProduct", productId);
        Document product_doc = productList.get(0);
        UpdateResult updateResult = cartRepository.updatePushArrayAndIncrementCount(cardObjectId
                , CartRepository.Fields.PRODUCTS.getValue()
                , CartRepository.Fields.TOTAL_SHOP_CARD.getValue()
                , product_doc);

        if (!updateResult.wasAcknowledged())
            throw new UpdateException("Cannot update your product to the card");
    }

    public void removeProduct(String cardId, String productId) {
        ObjectId cardObjectId = new ObjectId(cardId);
        ObjectId productObjectId = new ObjectId(productId);

        Document card_doc = null;
        List<Document> cadList = cartRepository.findByCardIdAndProductId(cardObjectId, productObjectId);
        if (cadList == null || cadList.size() == 0)
            throw new NotFoundException("your card id or product id is null : --> " + cardId + " " + " " + productId
                    , "CardService remove"
                    , cardId + " " + productId);
        card_doc = cadList.get(0);

        UpdateResult remove = cartRepository.remove(cardObjectId, productObjectId);
        if (!remove.wasAcknowledged())
            throw new UpdateException("Cannot remove product in your card from database");

    }


    public List<Cart> list() {
        return null;
    }


}

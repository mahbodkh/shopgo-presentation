package app.store.service;

import app.store.controller.rest.error.NotFoundException;
import app.store.controller.rest.error.UpdateException;
import app.store.persistance.CategoryRepository;
import app.store.persistance.ProductRepository;
import app.store.persistance.domain.Product;
import app.store.util.General;
import com.mongodb.client.result.DeleteResult;
import com.mongodb.client.result.UpdateResult;
import org.bson.Document;
import org.bson.types.ObjectId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ProductService {

    private final Logger log = LoggerFactory.getLogger(ProductService.class);
    private ProductRepository productRepository;
    private CategoryRepository categoryRepository;
    private long now = System.currentTimeMillis();

    @Autowired
    public ProductService(ProductRepository productRepository, CategoryRepository categoryRepository) {
        this.productRepository = productRepository;
        this.categoryRepository = categoryRepository;
    }

    public Product getById(String id) {
        List<Document> productList = productRepository.getDataById(new ObjectId(id));
        Document productResultDocument = null;
        if (productList.size() == 0)
            throw new NotFoundException("the id is wrong or deleted or not found product", "ProductService getById", productList.toString());
        else
            productResultDocument = productList.get(0);
        return productRepository.getProductEntity(productResultDocument);
    }

    public List<Product> findAll() {
        List<Document> productList = productRepository.findAll();
        if (productList.size() == 0)
            throw new NotFoundException("cant find all products", "ProductService findAll", productList.toString());
        return productRepository.getProductList(productList);
    }

    public List<Product> findAll(int limit) {
        List<Document> productList = productRepository.findAll(limit);
        if (productList.size() == 0)
            throw new NotFoundException("cant find all products", "ProductService findAllLimit", productList.toString());
        return productRepository.getProductList(productList);
    }


    public Product add(Product product) {
        Document category = null;
        Document insertResult = null;
        List<Document> categoryListById = categoryRepository.getDataById(new ObjectId(product.getCategory()));
        if (categoryListById.size() == 0)
            throw new NotFoundException("the category is not exist", "", "");
        else {
            category = categoryListById.get(0);
            Document productDocument = productRepository.getProductDocument(product);
            productDocument.put(ProductRepository.Fields.INSERTTIME.getValue(), now);
            productDocument.put(ProductRepository.Fields.LASTCHANGETIME.getValue(), now);

            // categories
            Document categories = new Document();
            categories.put(ProductRepository.Fields.ID.getValue(), category.getObjectId(ProductRepository.Fields.ID.getValue()));
            categories.put(ProductRepository.Fields.NAME.getValue(), category.getString(ProductRepository.Fields.NAME.getValue()));
            productDocument.put(ProductRepository.Fields.CATEGORIES.getValue(), categories);

            // keywords
            List<String> keywordList = new ArrayList<>();
            for (String s : product.getKeywords()) {
                s.split(" ");
                keywordList.add(General.parseString(s));
            }
            productDocument.put(ProductRepository.Fields.KEYWORDS.getValue(), keywordList);
            //
            insertResult = productRepository.insert(productDocument);
        }
        return productRepository.getProductEntity(insertResult);
    }

    public Product update(Product product) {
        Document productDoc = productRepository.getProductEditDocument(product);
        productDoc.put(ProductRepository.Fields.LASTCHANGETIME.getValue(), now);
        UpdateResult update = productRepository.update(new ObjectId(product.getId()), productDoc);
        if (!update.wasAcknowledged())
            throw new UpdateException("Cannot update to product in mongo");
        return productRepository.getProductUpdateEntity(productDoc);
    }

    public DeleteResult remove(String id) {
        getById(id);
        return productRepository.remove(new ObjectId(id));
    }


    // ------------------------------->>
    //          keyword services functions
    // ------------------------------->>

    public Product addKeywordProduct(String productId, String keyword) {
        return null;
    }


}
package app.store.service;

import app.store.persistance.UserRepository;
import app.store.persistance.domain.User;
import org.bson.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class UserService {

    private final Logger log = LoggerFactory.getLogger(CategoryService.class);
    private UserRepository userRepository;

    @Autowired
    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public User add(User user) {
        Document user_doc = userRepository.getUserDocument(user);
        Document inserted = userRepository.insert(user_doc);
        return userRepository.getUserEntity(inserted);
    }
}

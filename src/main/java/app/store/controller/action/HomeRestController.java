package app.store.controller.action;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;


@Controller
public class HomeRestController {

    public class WelcomeController {
        @RequestMapping("/welcome")
        public String loginMessage() {
            return "welcome";
        }
    }

}

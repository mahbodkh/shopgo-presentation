package app.store.controller.rest;

import app.store.controller.rest.error.BadRequestAlertException;
import app.store.controller.rest.error.NotFoundException;
import app.store.controller.rest.error.ReturnAlertException;
import app.store.persistance.domain.Product;
import app.store.service.ProductService;
import app.store.util.WriteToJson;
import app.store.util.enums.ReturnStatusCode;
import com.mongodb.client.result.DeleteResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping(value = "/product")
public class ProductRestController {

    private final Logger log = LoggerFactory.getLogger(ProductRestController.class);
    private ProductService productService;


    @Autowired
    public ProductRestController(ProductService productService) {
        this.productService = productService;
    }


    @RequestMapping(
            value = {"/add", "/add/"},
            method = RequestMethod.POST,
            produces = {"application/json"}
    )
    public @ResponseBody
    ResponseEntity add(Product product) {
        log.debug("REST request to save Product : {}", product);
        if (product == null)
            throw new NotFoundException("product object is null", "ProductRestController add", product.toString());
        Product productResult = productService.add(product);
        return ResponseEntity.ok().body(WriteToJson.jsonProduct(productResult));
    }


    @RequestMapping(
            value = {"/all", "/all/"},
            method = RequestMethod.GET,
            produces = {"application/json"}
    )
    public @ResponseBody
    ResponseEntity getAll() {
        log.debug("REST request to get all products : {} ");
        List<Product> productList = productService.findAll();
        return ResponseEntity.ok().body(WriteToJson.jsonProductList(productList));
    }

    @RequestMapping(
            value = {"/all/{limit}", "/all/{limit}/"},
            method = RequestMethod.GET,
            produces = {"application/json"}
    )
    public @ResponseBody
    ResponseEntity getAll(@PathVariable Integer limit) {
        log.debug("REST request to get all products : {} ");
        List<Product> productList = null;
        if (limit == 0)
            throw new NotFoundException("the limitation is null ", "", "");
        productList = productService.findAll(limit);
        return ResponseEntity.ok().body(WriteToJson.jsonProductList(productList));
    }

    @RequestMapping(
            value = {"/get/{id}", "/get/{id}/"},
            method = RequestMethod.GET,
            produces = {"application/json"}
    )
    public @ResponseBody
    ResponseEntity get(@PathVariable("id") String id) {
        log.debug("REST request for Get the product by id : {}", id);
        if (id == null)
            throw new NotFoundException("the product id is null", "ProductRestController get", id);
        Product product = productService.getById(id);
        return ResponseEntity.ok().body(WriteToJson.jsonProduct(product));
    }


    @RequestMapping(
            value = {"/update", "/update/"},
            method = RequestMethod.POST,
            produces = {"application/json"}
    )
    public @ResponseBody
    ResponseEntity update(Product product) {
        log.debug("REST request for update the product : {}", product);
        if (product == null)
            throw new NotFoundException("the product object is null", "ProductRestController update", product.toString());
        Product productReturn = productService.update(product);
        return ResponseEntity.ok().body(WriteToJson.jsonProduct(productReturn));
    }


    @RequestMapping(
            value = {"/remove/{id}", "/remove/{id}/"},
            method = RequestMethod.DELETE,
            produces = {"application/json"}
    )
    public @ResponseBody
    ResponseEntity remove(@PathVariable("id") String id) {
        log.debug("REST request to remove by id product : {}", id);
        if (id == null)
            throw new NotFoundException("the product id is null", "ProductRestController remove", id);
        DeleteResult remove = productService.remove(id);
        if (!remove.wasAcknowledged()) {
            throw new BadRequestAlertException("the result of remove : cant delete document", "/product", "remove acknowledge");
        }
        return ResponseEntity.ok(HttpStatus.ACCEPTED);
    }


    // --------------->
    //                  category services for product
    // --------------->
//    @RequestMapping(
//            value = {"/to", "/to/"},
//            method = RequestMethod.POST,
//            produces = {"application/json"}
//    )
//    public @ResponseBody
//    ResponseEntity addCategory(String product, String category) {
//        log.debug("REST request for update the Category : {} : {} ", product, category);
//        if (product == null && category == null) {
//            throw new ReturnAlertException(ReturnStatusCode.NULL_INPUT_PARAMETER_ENTRY.getMessage_key(), "addCategory");
//        }
//        Product productResult = productService.addCategory(product, category);
//        return ResponseEntity.ok().body(WriteToJson.jsonProduct(productResult));
//    }


    // --------------->
    //                  keyword services for product
    // --------------->
    @RequestMapping(
            value = {"/keyword/add", "/keyword/add/"},
            method = RequestMethod.POST,
            produces = {"application/json"}
    )
    public @ResponseBody
    ResponseEntity addKeyword(String productId, String keyword) {
        log.debug("REST request for add the keyword to product : {} : {} ", productId, keyword);
        if (productId == null && keyword == null) {
            throw new ReturnAlertException(ReturnStatusCode.NULL_INPUT_PARAMETER_ENTRY.getMessage_key(), "addKeyword");
        }
        Product productResult = productService.addKeywordProduct(productId, keyword);
        return ResponseEntity.ok().body(WriteToJson.jsonProduct(productResult));
    }


}

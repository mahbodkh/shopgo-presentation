package app.store.controller.rest;

import app.store.controller.rest.error.NotFoundException;
import app.store.persistance.domain.Cart;
import app.store.service.CartService;
import app.store.util.WriteToJson;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping(value = "/card")
public class CartRestController {

    private final Logger log = LoggerFactory.getLogger(CartRestController.class);
    private CartService cartService;

    @Autowired
    public CartRestController(CartService cartService) {
        this.cartService = cartService;
    }

    @RequestMapping(
            value = {"/add", "/add/"},
            method = RequestMethod.POST,
            produces = {"application/json"}
    )
    public @ResponseBody
    ResponseEntity add(Cart cart) {
        log.debug("REST request to save Product : {}", cart);
        if (cart == null)
            throw new NotFoundException("cart object is null", "CardRestController add", cart.toString());
        Cart cartResult = cartService.add(cart);
        return ResponseEntity.ok().body(WriteToJson.jsonOrder(cartResult));
    }

    @RequestMapping(
            value = {"/get", "/get/"},
            method = RequestMethod.POST,
            produces = {"application/json"}
    )
    public @ResponseBody
    ResponseEntity get(String cardId) {
        log.debug("REST request to save Product : {}", cardId);
        if (cardId == null)
            throw new NotFoundException("card id is null", "CardRestController get", cardId);
        Cart cartResult = cartService.get(cardId);
        return ResponseEntity.ok().body(WriteToJson.jsonOrder(cartResult));
    }

    @RequestMapping(
            value = {"/remove", "/remove/"},
            method = RequestMethod.POST,
            produces = {"application/json"}
    )
    public @ResponseBody
    ResponseEntity remove(String cardId) {
        log.debug("REST request to save Product : {}", cardId);
        if (cardId == null)
            throw new NotFoundException("card object is null", "CardRestController edit", cardId);
        cartService.remove(cardId);
        return ResponseEntity.ok(HttpStatus.ACCEPTED);
    }


    /////////////// CARD-PRODUCT /////////////////
    //////////////////////////////////////////////

    @RequestMapping(
            value = {"/add/product", "/add/product/"},
            method = RequestMethod.POST,
            produces = {"application/json"}
    )
    public @ResponseBody
    ResponseEntity addProduct(String cardId, String productId) {
        log.debug("REST request to save Product : {} : {} ", cardId, productId);
        if (cardId == null && productId == null)
            throw new NotFoundException("cardId or productId is null", "CardRestController removeProduct", cardId + " " + productId);
        cartService.addProduct(cardId, productId);
        return ResponseEntity.ok(HttpStatus.ACCEPTED);
    }


    @RequestMapping(
            value = {"/remove/product", "/remove/product/"},
            method = RequestMethod.POST,
            produces = {"application/json"}
    )
    public @ResponseBody
    ResponseEntity removeProduct(String cardId, String productId) {
        log.debug("REST request to save Product : {} : {} ", cardId, productId);
        if (cardId == null && productId == null)
            throw new NotFoundException("cardId or productId is null", "CardRestController removeProduct", cardId + " " + productId);
        cartService.removeProduct(cardId, productId);
        return ResponseEntity.ok(HttpStatus.ACCEPTED);
    }


}

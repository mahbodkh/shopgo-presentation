package app.store.controller.rest;

import app.store.persistance.domain.Payment;
import app.store.service.PaymentService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/payment")
public class PaymentRestController {
    private final static Logger log = LoggerFactory.getLogger(PaymentRestController.class);
    private PaymentService paymentService;

    @Autowired
    public PaymentRestController(PaymentService paymentService) {
        this.paymentService = paymentService;
    }

    @RequestMapping(
            value = {"/pay", "/pay/"},
            method = RequestMethod.POST,
            produces = {"application/json"}
    )
    public @ResponseBody
    ResponseEntity pay(Payment payment) {
        log.debug("REST request for Pay by payment : {}", payment);
        return null;
    }

    @RequestMapping(
            value = {"/pay/confirm", "/pay/confirm/"},
            method = RequestMethod.POST,
            produces = {"application/json"}
    )
    public @ResponseBody
    ResponseEntity confirm(Payment payment) {
        log.debug("REST request for Pay by payment : {}", payment);
        return null;
    }


    @RequestMapping(
            value = {"/callback", "/callback/"},
            method = {RequestMethod.POST, RequestMethod.GET},
            produces = {"application/json"}
    )
    public @ResponseBody
    ResponseEntity callback() {
        log.debug("REST request for  CallBack from bank : {}");
        return null;
    }


    @RequestMapping(
            value = {"/rollback", "/rollback/"},
            method = {RequestMethod.POST, RequestMethod.GET},
            produces = {"application/json"}
    )
    public @ResponseBody
    ResponseEntity rollback() {
        log.debug("REST request for rollback the payment to bank : {}");
        return null;
    }

}

package app.store.controller.rest;

import app.store.controller.rest.error.NotFoundException;
import app.store.service.SearchService;
import app.store.util.General;
import app.store.util.WriteToJson;
import org.bson.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = {"/search", "/search/"})
public class SearchRestController {

    private final Logger log = LoggerFactory.getLogger(ProductRestController.class);

    private SearchService searchService;

    @Autowired
    public SearchRestController(SearchService searchService) {
        this.searchService = searchService;
    }


    @RequestMapping(
            value = {"/", ""},
            method = RequestMethod.POST,
            produces = {"application/json"}
    )
    public @ResponseBody
    ResponseEntity search(@Validated String search, Integer limit) {
        log.debug("REST request for Search the rest  : {}", search);
        String keyword = null;

        if (search == null)
            throw new NotFoundException("", "", "");

        List<Document> result = null;
        if (!General.isEmpty(search)) {
            keyword = General.parseString(search);
            if (limit != null) {
                 searchService.doSearch(keyword, limit);
            } else {
                result = searchService.doSearch(keyword);
            }
        }

        return ResponseEntity.ok().body(WriteToJson.jsonProductOrCategory(result));
    }

}

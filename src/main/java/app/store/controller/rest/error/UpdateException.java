package app.store.controller.rest.error;

import org.zalando.problem.AbstractThrowableProblem;
import org.zalando.problem.Status;

import java.net.URI;
import java.util.HashMap;
import java.util.Map;

public class UpdateException extends AbstractThrowableProblem {
    public UpdateException(String message) {
        this(ErrorConstants.UPDATE_EXCEPTION, message, null, null);
    }

    public UpdateException(String defaultMessage, String entityName, String errorKey) {
        this(ErrorConstants.NOT_FOUND, defaultMessage, entityName, errorKey);
    }

    public UpdateException(URI type, String defaultMessage, String entityName, String errorKey) {
        super(type, defaultMessage, Status.NOT_FOUND, null, null, null, getAlertParameters(entityName, errorKey));
    }

    private static Map<String, Object> getAlertParameters(String entityName, String errorKey) {
        Map<String, Object> parameters = new HashMap<>();
        parameters.put("message", "error." + errorKey);
        parameters.put("params", entityName);
        return parameters;
    }
}

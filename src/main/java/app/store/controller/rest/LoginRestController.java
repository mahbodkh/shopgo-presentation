package app.store.controller.rest;

import app.store.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/login")
public class LoginRestController {

    private final Logger log = LoggerFactory.getLogger(LoginRestController.class);
    private UserService userService;

    @Autowired
    public LoginRestController(UserService userService) {
        this.userService = userService;
    }


    @RequestMapping(
            value = {"/do", "/do/"},
            method = RequestMethod.POST,
            produces = {"application/json"}
    )
    public @ResponseBody
    ResponseEntity get(@Validated String username) {
//        log.debug("REST request for Get the user by id : {}", id);
//        UserDTO userDTO = userService.getByIdOld(id);
//        return ResponseEntity.ok().body(WriteToJson.jsonUser(userDTO));
        return null;
    }


}

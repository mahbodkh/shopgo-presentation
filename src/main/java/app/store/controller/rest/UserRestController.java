package app.store.controller.rest;


import app.store.controller.rest.error.NotFoundException;
import app.store.persistance.domain.User;
import app.store.service.UserService;
import app.store.util.WriteToJson;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/user")
public class UserRestController {
    private final Logger log = LoggerFactory.getLogger(UserRestController.class);
    private UserService userService;

    @Autowired
    public UserRestController(UserService userService) {
        this.userService = userService;
    }

    @RequestMapping(
            value = {"/add", "/add/"},
            method = RequestMethod.POST,
            produces = {"application/json"}
    )
    public @ResponseBody
    ResponseEntity addProduct(User user) {
        log.debug("REST request to save User : {}", user);
        if (user == null)
            throw new NotFoundException("product object is null", "ProductRestController add", user.toString());
        User userResult = userService.add(user);
        return ResponseEntity.ok().body(WriteToJson.jsonUser(userResult));
    }
//
//    @RequestMapping(
//            value = {"/all", "/all/"},
//            method = RequestMethod.GET,
//            produces = {"application/json"}
//    )
//    public @ResponseBody
//    ResponseEntity getAll() {
//        log.debug("REST request to get all users : {}");
//        List<UserDTO> userDTOList = userService.findAll();
//        return ResponseEntity.ok().body(WriteToJson.jsonUserList(userDTOList));
//    }
//
//    @RequestMapping(
//            value = {"/get/{id}", "/get/{id}/"},
//            method = RequestMethod.GET,
//            produces = {"application/json"}
//    )
//    public @ResponseBody
//    ResponseEntity get(@PathVariable("id") String id) {
//        log.debug("REST request for Get the user by id : {}", id);
//        UserDTO userDTO = userService.getById(id);
//        return ResponseEntity.ok().body(WriteToJson.jsonUser(userDTO));
//    }
//
//
//    @RequestMapping(
//            value = {"/update", "/update/"},
//            method = RequestMethod.POST,
//            produces = {"application/json"}
//    )
//    public @ResponseBody
//    ResponseEntity update(UserDTO userDTO) {
//        log.debug("REST request for update the user : {}", userDTO);
//        UserDTO userDtoReturn = userService.update(userDTO);
//        return ResponseEntity.ok().body(WriteToJson.jsonUser(userDtoReturn));
//    }
//
//
//    @RequestMapping(
//            value = {"/remove/{id}", "/remove/{id}/"},
//            method = RequestMethod.DELETE,
//            produces = {"application/json"}
//    )
//    public @ResponseBody
//    ResponseEntity remove(@PathVariable("id") String id) {
//        log.debug("REST request to remove by id user : {}", id);
//        DeleteResult remove = userService.remove(id);
//        if (!remove.wasAcknowledged()) {
//            throw new BadRequestAlertException("the result of remove : cant user document", "/user", "remove acknowledge");
//        }
//        return ResponseEntity.ok(HttpStatus.ACCEPTED);
//    }
}

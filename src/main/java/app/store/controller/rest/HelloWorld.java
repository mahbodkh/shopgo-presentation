package app.store.controller.rest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class HelloWorld {

    private final Logger log = LoggerFactory.getLogger(HelloWorld.class);


//    @RequestMapping(value = "/", method = RequestMethod.GET)
//    public String getRoot() {
//        log.debug("this is hello world for GET method {}");
//        return "index.html";
//    }

//    @RequestMapping(value = "/valid", method = RequestMethod.GET)
//    @ResponseBody
//    public String getValidate(@Validated PojoModel pojoModel) {
//        log.debug("this is hello world for GET method {}");
//        return "hey dude";
//    }

    @RequestMapping(value = "/home")
    public String getRoot() {
        log.debug("this is hello world for root '/' method {} : ");
//        model.put("message", "hi");
        return "index";
    }


}

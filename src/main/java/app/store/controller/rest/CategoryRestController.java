package app.store.controller.rest;

import app.store.controller.rest.error.NotFoundException;
import app.store.persistance.domain.Category;
import app.store.service.CategoryService;
import app.store.util.WriteToJson;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/category")
public class CategoryRestController {

    private final Logger log = LoggerFactory.getLogger(CategoryRestController.class);
    private CategoryService categoryService;


    @Autowired
    public CategoryRestController(CategoryService categoryService) {
        this.categoryService = categoryService;
    }

    @RequestMapping(
            value = {"/add", "/add/"},
            method = RequestMethod.POST,
            produces = {"application/json"}
    )
    public @ResponseBody
    ResponseEntity add(@Validated Category category) {
        log.debug("REST request to save Category : {}", category);
        if (category == null)
            throw new NotFoundException("Category object is null", "CategoryRestController add", category.toString());
        Category categoryResult = categoryService.add(category);
        return ResponseEntity.ok().body(WriteToJson.jsonAddCategory(categoryResult));
    }

    @RequestMapping(
            value = {"/update", "/update/"},
            method = RequestMethod.POST,
            produces = {"application/json"}
    )
    public @ResponseBody
    ResponseEntity update(Category category) {
        log.debug("REST request for update the Category : {}", category);
        if (category == null)
            throw new NotFoundException("Category object is null", "CategoryRestController update", category.toString());
        Category categoryResult = categoryService.update(category);
        return ResponseEntity.ok().body(WriteToJson.jsonAddCategory(categoryResult));
    }


    //    @RequestMapping(
//            value = {"/all", "/all/"},
//            method = RequestMethod.GET,
//            produces = {"application/json"}
//    )
//    public @ResponseBody
//    ResponseEntity getAll() {
//        log.debug("REST request to get all Category : {}");
//        List<CategoryDTO> CategoryDTOList = categoryService.findAll();
//        return ResponseEntity.ok().body(WriteToJson.jsonCategoryList(CategoryDTOList));
//    }
//
//    @RequestMapping(
//            value = {"/get/{id}", "/get/{id}/"},
//            method = RequestMethod.GET,
//            produces = {"application/json"}
//    )
//    public @ResponseBody
//    ResponseEntity get(@PathVariable("id") String id) {
//        log.debug("REST request for Get the Category by id : {}", id);
//        CategoryDTO categoryDTOReturn = categoryService.getByIdOld(id);
//        return ResponseEntity.ok().body(WriteToJson.jsonCategory(categoryDTOReturn));
//    }
//


//    @RequestMapping(
//            value = {"/remove/{id}", "/remove/{id}/"},
//            method = RequestMethod.DELETE,
//            produces = {"application/json"}
//    )
//    public @ResponseBody
//    ResponseEntity remove(@PathVariable("id") String id) {
//        log.debug("REST request to remove by id Category : {}", id);
//        DeleteResult remove = categoryService.remove(id);
//        if (!remove.wasAcknowledged()) {
//            throw new BadRequestAlertException("the result of remove : cant delete document", "/category", "remove acknowledge");
//        }
//        return ResponseEntity.ok(HttpStatus.ACCEPTED);
//    }
//
//    @RequestMapping(
//            value = {"/add-sub", "/add-sub/"},
//            method = RequestMethod.POST,
//            produces = {"application/json"}
//    )
//    public @ResponseBody
//    ResponseEntity addSubCategory(@Validated String categoryId, @Validated CategoryDTO subCategoryDTO) {
//        log.debug("REST request to save Category : {} {}", categoryId, subCategoryDTO);
//        if (categoryId == null) {
//            throw new NotFoundException("the category id is null ", "addSubCategory Method", categoryId);
//        }
//        CategoryDTO categoryDTOResult = categoryService.addSubCategory(categoryId, subCategoryDTO);
//        return ResponseEntity.ok().body(WriteToJson.jsonCategory(categoryDTOResult));
//    }
//
//    @RequestMapping(
//            value = {"/all-sub/{id}", "/all-sub/{id}/"},
//            method = RequestMethod.GET,
//            produces = {"application/json"}
//    )
//    public @ResponseBody
//    ResponseEntity getAllSubCategory(@Validated @PathVariable("id") String categoryId) {
//        log.debug("REST request to get all SubCategory : {}");
//        if (categoryId != null) {
//            throw new NotFoundException("the category id id is null ", "getAllSubCategory Method", categoryId);
//        }
//        List<CategoryDTO> CategoryDTOList = categoryService.findAll();
//        return ResponseEntity.ok().body(WriteToJson.jsonCategoryList(CategoryDTOList));
//    }
//
//    @RequestMapping(
//            value = {"/update-sub", "/update-sub/"},
//            method = RequestMethod.POST,
//            produces = {"application/json"}
//    )
//    public @ResponseBody
//    ResponseEntity updateSubCategory(@Validated String categoryId, @Validated String subcategoryId, @Validated CategoryDTO subCategoryDTO) {
//        log.debug("REST request to save Category : {} {}", categoryId, subcategoryId);
//        if (categoryId == null && subcategoryId == null) {
//            throw new NotFoundException("the category id or sub-category id is null ", "addSubCategory Method", categoryId);
//        }
//        CategoryDTO categoryDTOResult = categoryService.updateSubCategory(categoryId, subcategoryId, subCategoryDTO);
//        return ResponseEntity.ok().body(WriteToJson.jsonCategory(categoryDTOResult));
//    }

    @RequestMapping(
            value = {"/remove-sub", "/remove-sub/"},
            method = RequestMethod.POST,
            produces = {"application/json"}
    )
    public @ResponseBody
    ResponseEntity removeSubCategory(@Validated String categoryId, String subcategoryId) {
        log.debug("REST request to save Category : {} {}", "");
        if (categoryId == null && subcategoryId == null) {
            throw new NotFoundException("the id of sub category or category for remove, is null", "RemoveSubCategory service", categoryId + " --- " + subcategoryId);
        }

        categoryService.removeSubCategory(categoryId, subcategoryId);

        return ResponseEntity.ok(HttpStatus.ACCEPTED);
    }


}

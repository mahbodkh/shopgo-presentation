//package app.store.util;
//
//import app.store.util.enums.ApiMethod;
//import com.google.gson.JsonObject;
//import com.google.gson.JsonParser;
//import org.springframework.http.MediaType;
//
//import javax.net.ssl.*;
//import java.io.InputStream;
//import java.io.OutputStreamWriter;
//import java.net.*;
//import org.apache.commons.io.IOUtils;
//import java.security.KeyManagementException;
//import java.security.NoSuchAlgorithmException;
//import java.security.cert.CertificateException;
//import java.security.cert.X509Certificate;
//import java.util.Map;
//import java.util.zip.GZIPInputStream;
//
//
//public class HttpCaller {
//    static boolean inited = false;
//
//    public static HttpCallerResult postUrl(String u, Map<String, String> bodyMap, Map<String, String> headers, ApiMethod method, MediaType contentType, int timeout) {
//        String body = "";
//        JsonObject jsonObject = new JsonObject();
//        String urlencodeBody = "";
//        if (bodyMap != null)
//            for (Map.Entry<String, String> entry : bodyMap.entrySet()) {
//                if (contentType.equals(MediaType.APPLICATION_JSON) || contentType.equals(MediaType.APPLICATION_JSON_UTF8))
//                    General.addJsonParseValue(jsonObject, entry.getKey(), entry.getValue());
//                else
//                    urlencodeBody += (urlencodeBody.isEmpty() ? "" : "&") + entry.getKey() + "=" + General.urlencode(entry.getValue().toString());
//            }
//        if (contentType.equals(MediaType.APPLICATION_JSON))
//            body = jsonObject.toString();
//        else //MediaType.APPLICATION_FORM_URLENCODED
//            body = urlencodeBody;
//
//
//        return postUrlWithBody(u, body, headers, method, timeout);
//    }
//
//    public static HttpCallerResult postUrlWithBody(String u, String body, Map<String, String> headers, ApiMethod method, int timeout) {
//        HttpCallerResult result = null;
//        try {
//            URL url = new URL(u);
//            if (url.getProtocol().toLowerCase().equals("https"))
//                return postUrlWithBodyHttps(u, body, headers, method, timeout);
//            else
//                return postUrlWithBodyHttp(u, body, headers, method, timeout);
//        } catch (Exception e) {
//            result = new HttpCallerResult();
//            result.setHasError(true);
//            String message = e.getMessage();
//            if (message != null) {
//                if (message.toLowerCase().contains("timed")) {
////                    result.setStatus(408);
//                    result.setReturnStatusCode(Return_Status_Codes_Common.REQUEST_TIMEDOUT);
//                } else {
////                    result.setStatus(999);
//                    result.setReturnStatusCode(Return_Status_Codes_Common.REQUEST_CALL_FAILED);
//
//                }
//            }
//            result.setError(message);
//            e.printStackTrace();
//        }
//        return result;
//    }
//
//    public static HttpCallerResult postUrlWithBodyHttp(String u, String body, Map<String, String> headers, ApiMethod method, int timeout) throws Exception {
//        Object result = null;
//        HttpCallerResult httpCallerResult = new HttpCallerResult();
//
//        CookieManager cookieManager = new CookieManager();
//        CookieHandler.setDefault(cookieManager);
//        System.setProperty("sun.net.http.allowRestrictedHeaders", "true");
//        if (method == ApiMethod.GET) {
//            if (!General.isEmpty(body)) {
//                if (u.contains("?"))
//                    u += "&" + body;
//                else
//                    u += "?" + body;
//
//            }
//        }
//        URL url = new URL(u);
//        HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
//
//        if (method == ApiMethod.undefined)
//            method = ApiMethod.POST;
//
//        urlConnection.setRequestMethod(method.getValue());
//        if (method == ApiMethod.POST || method == ApiMethod.PUT)
//            urlConnection.setDoOutput(true);
//
//        if (headers != null) {
//            for (Map.Entry<String, String> map : headers.entrySet()) {
//                urlConnection.setRequestProperty(map.getKey(), map.getValue());
//            }
//        }
//
//        urlConnection.setConnectTimeout(timeout);
//        urlConnection.setReadTimeout(timeout);
//        if (method == ApiMethod.POST || method == ApiMethod.PUT) {
//            OutputStreamWriter out = new OutputStreamWriter(urlConnection.getOutputStream(), "utf-8");
//            out.write(body);
//            out.close();
//        }
//        int returnCode = urlConnection.getResponseCode();
//        InputStream is = null;
//        boolean isError = false;
//
//        httpCallerResult.setStatus(returnCode);
//
//        if (returnCode == 200 || returnCode == 202 || returnCode == 301) {
//            is = urlConnection.getInputStream();
//        } else {
//            is = urlConnection.getErrorStream();
//            isError = true;
//        }
//        httpCallerResult.setHasError(isError);
//
//        if (is != null) {
//            if ("gzip".equals(urlConnection.getContentEncoding())) {
//                is = new GZIPInputStream(is);
//            }
//            byte[] bytes = IOUtils.toByteArray(is);
//            String contentType = urlConnection.getHeaderField("CONTENT-TYPE");
//            if (contentType != null && contentType.toLowerCase().contains("application/octet-stream".toLowerCase())) {
//                result = bytes;
//                httpCallerResult.setResultRaw(bytes);
//
//            }
//            try {
//                String strresult = new String(bytes, "UTF-8");
//                result = strresult;
//                if (isError)
//                    httpCallerResult.setError(strresult);
//                else
//                    httpCallerResult.setResult(strresult);
//
//            } catch (Exception ignore) {
//            }
//        }
//        httpCallerResult.getCustomHeaders().put("Location", urlConnection.getHeaderField("Location")); //
//        httpCallerResult.getCustomHeaders().put("Expires", String.valueOf(urlConnection.getHeaderFieldDate("Expires", 0))); //
//        httpCallerResult.setHeaders(urlConnection.getHeaderFields());
//        urlConnection.disconnect();
//        return httpCallerResult;
//    }
//
//    public static HttpCallerResult postUrlWithBodyHttps(String u, String body, Map<String, String> headers, ApiMethod method, int timeout) throws Exception {
//        HttpCallerResult httpCallerResult = new HttpCallerResult();
//        Object result = null;
//        System.setProperty("sun.net.http.allowRestrictedHeaders", "true");
//        trustAllCertsInit();
//        if (method == ApiMethod.GET) {
//            if (!General.isEmpty(body)) {
//                if (u.contains("?"))
//                    u += "&" + body;
//                else
//                    u += "?" + body;
//
//            }
//        }
//        URL url = new URL(u);
//        HttpsURLConnection urlConnection = (HttpsURLConnection) url.openConnection();
//        urlConnection.setSSLSocketFactory(new SSLSocketFactoryFacade());
//
//        if (method == ApiMethod.undefined)
//            method = ApiMethod.POST;
//
//        urlConnection.setRequestMethod(method.getValue());
//        if (method == ApiMethod.POST || method == ApiMethod.PUT)
//            urlConnection.setDoOutput(true);
//
//        if (headers != null) {
//            for (Map.Entry<String, String> map : headers.entrySet()) {
//                urlConnection.setRequestProperty(map.getKey(), map.getValue());
//            }
//        }
//
//        urlConnection.setConnectTimeout(timeout);
//        urlConnection.setReadTimeout(timeout);
//        if (method == ApiMethod.POST || method == ApiMethod.PUT) {
//            OutputStreamWriter out = new OutputStreamWriter(urlConnection.getOutputStream());
//            out.write(body);
//            out.close();
//        }
//        int returnCode = urlConnection.getResponseCode();
//        InputStream is = null;
//        boolean isError = false;
//        httpCallerResult.setStatus(returnCode);
//
//        if (returnCode == 200 || returnCode == 201 || returnCode == 202) {
//            is = urlConnection.getInputStream();
//        } else {
//            is = urlConnection.getErrorStream();
//            isError = true;
//        }
//        httpCallerResult.setHasError(isError);
//        if ("gzip".equals(urlConnection.getContentEncoding())) {
//            is = new GZIPInputStream(is);
//        }
//
//        byte[] bytes = IOUtils.toByteArray(is);
//        String contentType = urlConnection.getHeaderField("CONTENT-TYPE");
//
//        if (contentType != null && contentType.toLowerCase().contains("application/octet-stream".toLowerCase())) {
//            result = bytes;
//            httpCallerResult.setResultRaw(bytes);
//
//        }
//        try {
//            String strresult = new String(bytes, "UTF-8");
//            result = strresult;
//            if (isError)
//                httpCallerResult.setError(strresult);
//            else
//                httpCallerResult.setResult(strresult);
//
//        } catch (Exception ignore) {
//        }
//
//        httpCallerResult.getCustomHeaders().put("Location", urlConnection.getHeaderField("Location"));
//        httpCallerResult.getCustomHeaders().put("Expires", String.valueOf(urlConnection.getHeaderFieldDate("Expires", 0)));
//        httpCallerResult.setHeaders(urlConnection.getHeaderFields());
//
//        urlConnection.disconnect();
//
//        return httpCallerResult;
//    }
//
//    public static void trustAllCertsInit() {
//
//        if (!inited) {
//
//            try {
//                //Do not remove This line.... instantiate URL Handler Correctly
//                URL url = new URL("https://");
//                trustAllCerts();
//                inited = true;
//            } catch (NoSuchAlgorithmException e) {
//                e.printStackTrace();
//            } catch (KeyManagementException e) {
//                e.printStackTrace();
//            } catch (MalformedURLException e) {
//                e.printStackTrace();
//            }
//        }
//    }
//
//    public static synchronized void trustAllCerts() throws NoSuchAlgorithmException, KeyManagementException {
//        TrustManager[] trustAllCerts = new TrustManager[]{
//                new X509ExtendedTrustManager() {
//                    @Override
//                    public X509Certificate[] getAcceptedIssuers() {
//                        return null;
//                    }
//
//                    @Override
//                    public void checkClientTrusted(X509Certificate[] certs, String authType) {
//                    }
//
//                    @Override
//                    public void checkServerTrusted(X509Certificate[] certs, String authType) {
//                    }
//
//                    @Override
//                    public void checkClientTrusted(X509Certificate[] xcs, String string, Socket socket) throws CertificateException {
//
//                    }
//
//                    @Override
//                    public void checkServerTrusted(X509Certificate[] xcs, String string, Socket socket) throws CertificateException {
//
//                    }
//
//                    @Override
//                    public void checkClientTrusted(X509Certificate[] xcs, String string, SSLEngine ssle) throws CertificateException {
//
//                    }
//
//                    @Override
//                    public void checkServerTrusted(X509Certificate[] xcs, String string, SSLEngine ssle) throws CertificateException {
//
//                    }
//
//                }
//        };
//        SSLContext sc = SSLContext.getInstance("SSL");
//
//        sc.init(null, trustAllCerts, new java.security.SecureRandom());
//        HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
//
//        // Create all-trusting host name verifier
//        HostnameVerifier allHostsValid = new HostnameVerifier() {
//            @Override
//            public boolean verify(String hostname, SSLSession session) {
//                return true;
//            }
//        };
//        // Install the all-trusting host verifier
//        HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);
//    }
//
//    public static JsonObject jsonFromString(String jsonObjectStr) {
//
//        JsonParser parser = new JsonParser();
//        JsonObject o = parser.parse(jsonObjectStr).getAsJsonObject();
//
//        return o;
//    }
//}

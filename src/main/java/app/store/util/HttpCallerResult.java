package app.store.util;


import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class HttpCallerResult {
    int status;
    String result;
    byte[] resultRaw;
    String error;
    Map<String, List<String>> headers;
    HashMap<String, String> customHeaders = new HashMap<>();
    boolean hasError;
//    IReturn_Status_Codes returnStatusCode = null;

    public HttpCallerResult() {
        hasError = false;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public Map<String, List<String>> getHeaders() {
        return headers;
    }

    public void setHeaders(Map<String, List<String>> headers) {
        this.headers = headers;
    }

    public byte[] getResultRaw() {
        return resultRaw;
    }

    public void setResultRaw(byte[] resultRaw) {
        this.resultRaw = resultRaw;
    }

    public boolean isHasError() {
        return hasError;
    }

    public void setHasError(boolean hasError) {
        this.hasError = hasError;
    }

    public boolean isHasAuthenticationError() {
        return status == 401 || status == 403 || status == 405 || status == 406;
    }

    public String getResultAll() {
        if (hasError)
            return error;
        return result;
    }

    public HashMap<String, String> getCustomHeaders() {
        return customHeaders;
    }

    public void setCustomHeaders(HashMap<String, String> customHeaders) {
        this.customHeaders = customHeaders;
    }

//    public IReturn_Status_Codes getReturnStatusCode() {
//        return returnStatusCode;
//    }
//
//    public void setReturnStatusCode(IReturn_Status_Codes returnStatusCode) {
//        this.returnStatusCode = returnStatusCode;
//        this.status=returnStatusCode.getCode();
//}

}

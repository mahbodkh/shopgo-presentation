package app.store.util.enums;

public enum ApiMethod {

    undefined(""),
    POST("POST"),
    GET("GET"),
    DELETE("DELETE"),
    PUT("PUT"),
    HEAD("HEAD"),
    OPTIONS("OPTIONS"),
    MOVE("MOVE"),
    COPY("COPY"),
    CONNECT("CONNECT"),
    TRACK("TRACK"),;

    private String value;

    ApiMethod(String value) {
        this.value = value;
    }

    public static ApiMethod get(String value) {

        if (value == null) {
            return undefined;
        }

        ApiMethod[] arr$ = values();
        for (ApiMethod val : arr$) {
            if (val.value.equalsIgnoreCase(value.trim())) {
                return val;
            }
        }

        System.out.println("No enum const " + ApiMethod.class.getSimpleName() + " with value " + value);
        return undefined;
    }

    public String getValue() {
        return value;
    }
}

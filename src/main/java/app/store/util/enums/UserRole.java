package app.store.util.enums;

public enum UserRole {

    GUEST("guest"),
    ADMIN("admin"),

    ;

    private String value;

    UserRole(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }


}

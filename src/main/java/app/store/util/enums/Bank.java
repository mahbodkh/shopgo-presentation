package app.store.util.enums;


import app.store.util.General;

public enum Bank {

    UNDEFINED("UNDEFINED", 0),

    ZARINPAL("zarinpal", 1),
    TEJARAT("tejarat", 2),
    MELLAT("mellat", 3),
    PASSARGAD("passargad", 4),
    EQTESDNOVIN("eqtesadnovin", 5),
    PARSIAN("parsian", 6),;


    private String value;
    private int code;

    Bank(String value, int code) {
        this.value = value;
        this.code = code;
    }

    public static Bank get(String value, int code) {
        if (General.isEmpty(value)) {
            return UNDEFINED;
        }

        Bank[] arr$ = values();
        for (Bank val : arr$) {
            if (val.value.equalsIgnoreCase(value)) {
                return val;
            }
        }
        return UNDEFINED;
    }

    public String getValue() {
        return value;
    }

    public int getCode() {
        return code;
    }

}

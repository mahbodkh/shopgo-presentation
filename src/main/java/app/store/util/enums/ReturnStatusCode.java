package app.store.util.enums;

public enum ReturnStatusCode {

    undefined(0, 0, "undefined"),
    ok_validform(1, 100, "ok_validform"),
    SC_OK(1, 200, "SC_OK"),
    SC_BAD_REQUEST(0, 400, "SC_BAD_REQUEST"),
    SC_UNAUTHORIZED(0, 401, "SC_UNAUTHORIZED"),
    SC_UNAUTHORIZED_AUTHENTICATION(0, 401, "SC_UNAUTHORIZED_AUTHENTICATION"),
    SC_PAYMENT_REQUIRED(0, 402, "SC_PAYMENT_REQUIRED"),
    SC_FORBIDDEN(0, 403, "SC_FORBIDDEN"),
    NO_PERMISSION(0, 8403, "nopermission"),
    SC_NOT_FOUND(0, 404, "SC_NOT_FOUND"),
    SC_METHOD_NOT_ALLOWED(0, 405, "SC_METHOD_NOT_ALLOWED"),
    SC_INTERNAL_SERVER_ERROR(0, 500, "SC_INTERNAL_SERVER_ERROR"),
    SC_SERVICE_UNAVAILABLE(0, 503, "SC_SERVICE_UNAVAILABLE"),
    SC_SERVICE_VERSION_NOT_SUPPORTED(0, 506, "SC_SERVICE_VERSION_NOT_SUPPORTED"),
    SC_TOO_MANY_REQUESTS(0, 429, "SC_Too_Many_Requests"),


    NULL_INPUT_PARAMETER_ENTRY(0, 3400, "null_your_input_parameter"),


    //
    NOT_FOUND_CATEGORY(0,3404,"NOT_FOUND_CATEGORY"),


    ;



    private int status;
    private int code;
    private String message_key;

    private ReturnStatusCode(int status, int code, String message_key) {
        this.status = status;
        this.code = code;
        this.message_key = message_key;
    }

    public static ReturnStatusCode get(int value) {
        if (value == 0) {
            return undefined;
        } else {
            ReturnStatusCode[] arr$ = values();
            ReturnStatusCode[] var2 = arr$;
            int var3 = arr$.length;

            for(int var4 = 0; var4 < var3; ++var4) {
                ReturnStatusCode val = var2[var4];
                if (val.code == value) {
                    return val;
                }
            }

            System.out.println("No enum const " + ReturnStatusCode.class.getSimpleName() + " with code " + value);
            return undefined;
        }
    }

    public String getMessage_key() {
        return this.message_key;
    }

    public int getStatus() {
        return this.status;
    }

    public int getCode() {
        return this.code;
    }

}

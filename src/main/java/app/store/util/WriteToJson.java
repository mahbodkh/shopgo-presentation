package app.store.util;


import app.store.persistance.CartRepository;
import app.store.persistance.CategoryRepository;
import app.store.persistance.ProductRepository;
import app.store.persistance.domain.Cart;
import app.store.persistance.domain.Category;
import app.store.persistance.domain.Product;
import app.store.persistance.domain.User;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.bson.Document;

import java.util.List;
import java.util.Map;

public class WriteToJson {

    private static ObjectMapper MAPPER = new ObjectMapper();


    public static ObjectNode jsonUser(User user) {
        ObjectNode result = MAPPER.createObjectNode();
        if (user != null) {
            result.put("id", user.getId());
            result.put("role", user.getRole());
            result.put("firstName", user.getFirstName() != null ? user.getFirstName() : "");
            result.put("lastName", user.getLastName() != null ? user.getLastName() : "");
            result.put("email", user.getEmail());
            result.put("mobile", user.getMobile());
            result.put("address", user.getAddress() != null ? user.getAddress() : "");
            result.put("gender", user.getGender());
        }
        return result;
    }

    public static ArrayNode jsonUserList(List<User> userDTOList) {
        ArrayNode array = MAPPER.createArrayNode();
        for (User user : userDTOList) {
            array.add(jsonUser(user));
        }
        return array;
    }

    //////////////////////////
    //////////////////////////

    public static ObjectNode jsonProduct(Product product) {
        ObjectNode result = MAPPER.createObjectNode();
        if (product != null) {
            result.put(ProductRepository.Fields.ID.getValue(), product.getId());
            result.put(ProductRepository.Fields.CATEGORY.getValue(), product.getCategory());
            result.put(ProductRepository.Fields.SUBCATEGORY.getValue(), product.getSubCategory());
            result.put(ProductRepository.Fields.NAME.getValue(), product.getName());
            result.put(ProductRepository.Fields.DESCRIPTION.getValue(), product.getDescription());
            result.put(ProductRepository.Fields.PRICE.getValue(), product.getPrice());
            result.put(ProductRepository.Fields.WEIGHT.getValue(), product.getWeight());
            result.put(ProductRepository.Fields.HEIGHT.getValue(), product.getHeight());
            result.put(ProductRepository.Fields.WIDTH.getValue(), product.getWidth());
            result.put(ProductRepository.Fields.COUNT.getValue(), product.getCount());
            result.put(ProductRepository.Fields.EXIST.getValue(), product.getExist());
            result.put(ProductRepository.Fields.INSERTTIME.getValue(), product.getInsertTime());
            result.put(ProductRepository.Fields.LASTCHANGETIME.getValue(), product.getLastChangeTime());
            result.put(ProductRepository.Fields.CATEGORIES.getValue(), jsonCategoryList(product.getCategories()));
            if (product.getKeywords() != null)
                result.put(ProductRepository.Fields.KEYWORDS.getValue(), jsonKeywordList(product.getKeywords()));
        }
        return result;
    }

    public static ArrayNode jsonProductList(List<Product> products) {
        ArrayNode array = MAPPER.createArrayNode();
        for (Product product : products) {
            array.add(jsonProduct(product));
        }
        return array;
    }

    //////////////////////////
    //////////////////////////

    public static ObjectNode jsonOrder(Cart cart) {
        ObjectNode result = MAPPER.createObjectNode();
        if (cart != null) {
            result.put(CartRepository.Fields.ID.getValue(), cart.getId());
            result.put(CartRepository.Fields.START_ORDER_TIME.getValue(), cart.getStartOrderTime());
            result.put(CartRepository.Fields.TOTAL_PRICE.getValue(), cart.getCalculatePrice());
            result.put(CartRepository.Fields.PRODUCTS.getValue(), jsonProductListForOrder(cart.getOutputProducts()));
        }
        return result;
    }

    public static ObjectNode jsonProductForOrder(Map<String, String> product) {
        ObjectNode result = MAPPER.createObjectNode();
        if (product != null) {
            result.put(ProductRepository.Fields.ID.getValue(), product.get(ProductRepository.Fields.ID.getValue()));
            result.put(ProductRepository.Fields.NAME.getValue(), product.get(ProductRepository.Fields.NAME.getValue()));
            result.put(ProductRepository.Fields.PRICE.getValue(), product.get(ProductRepository.Fields.PRICE.getValue()));
        }
        return result;
    }

    public static ArrayNode jsonProductListForOrder(List<Map<String, String>> products) {
        ArrayNode array = MAPPER.createArrayNode();
        for (Map<String, String> product : products) {
            array.add(jsonProductForOrder(product));
        }
        return array;
    }

    //////////////////////////
    //////////////////////////

    private static ArrayNode jsonKeywordList(List<String> keywords) {
        ObjectNode objectNode = MAPPER.createObjectNode();
        ArrayNode array = objectNode.putArray(String.valueOf(keywords));
        for (String word : keywords) {
            array.add(word);
        }
        return array;
    }

    private static ObjectNode jsonKeyword(String word) {
        ObjectNode result = MAPPER.createObjectNode();
        result.put(ProductRepository.Fields.KEYWORDS.getValue(), word);
        return result;
    }

    //////////////////////////
    //////////////////////////

    private static ObjectNode jsonProductImageUrl(String url) {
        ObjectNode result = MAPPER.createObjectNode();
        result.put(ProductRepository.Fields.IMAGE_URL.getValue(), url);
        return result;
    }

    public static ArrayNode jsonProductImageUrlList(List<String> products) {
        ArrayNode array = MAPPER.createArrayNode();

        for (String url : products) {
            array.add(jsonProductImageUrl(url));
        }
        return array;
    }


    //////////////////////////
    //////////////////////////

    public static ObjectNode jsonCategory(Category category) {
        ObjectNode result = MAPPER.createObjectNode();
        if (category != null) {
            result.put(CategoryRepository.Fields.ID.getValue(), category.getId());
//            result.put(CategoryRepository.Fields.CODE.getValue(), category.getCode());
            result.put(CategoryRepository.Fields.NAME.getValue(), category.getName());
//            result.put(CategoryRepository.Fields.DESCRIPTION.getValue(), category.getDescription());
//            result.put(CategoryRepository.Fields.URL.getValue(), category.getUrl());
//            result.put(CategoryRepository.Fields.SUBCATEGORIES.getValue(), jsonCategoryList(category.getSubCategories()));
        }
        return result;
    }

    public static ObjectNode jsonAddCategory(Category category) {
        ObjectNode result = MAPPER.createObjectNode();
        if (category != null) {
            result.put(CategoryRepository.Fields.ID.getValue(), category.getId());
            result.put(CategoryRepository.Fields.CODE.getValue(), category.getCode());
            result.put(CategoryRepository.Fields.NAME.getValue(), category.getName());
            result.put(CategoryRepository.Fields.DESCRIPTION.getValue(), category.getDescription());
            result.put(CategoryRepository.Fields.URL.getValue(), category.getUrl());
        }
        return result;
    }


    public static ArrayNode jsonCategoryList(List<Category> categoryList) {
        ArrayNode array = MAPPER.createArrayNode();
        if (categoryList != null) {
            for (Category category : categoryList) {
                array.add(jsonCategory(category));
            }
        }
        return array;
    }

    public static ArrayNode jsonProductOrCategory(List<Document> result) {
        ArrayNode array = MAPPER.createArrayNode();
        if (result != null) {
            for (Document res : result) {
//                array.add(res);
            }

        }

        return array;
    }

}

package app.store.persistance;

import app.store.persistance.domain.Cart;
import com.mongodb.client.model.Filters;
import com.mongodb.client.result.DeleteResult;
import com.mongodb.client.result.UpdateResult;
import org.bson.Document;
import org.bson.conversions.Bson;
import org.bson.types.ObjectId;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;


@Component
public class CartRepository extends MongoBasic {

    private static final String collectionName = "Cart";
    public static MongoDB mongo = MongoDB.getInstance();
    private long now = System.currentTimeMillis();


    public CartRepository() {
        super(mongo, collectionName);
    }

    public Document insert(Document document) {
        insertOne(document);
        return document;
    }

    public void insertMeny(List<Document> documentList) {
        insertMany(documentList);
    }

    public Document getOrderDocument(Cart cart) {
        Document document = new Document();
        if (cart != null) {
            if (cart.getId() == null)
                document.put(CartRepository.Fields.ID.getValue(), new ObjectId());
            else document.put(CartRepository.Fields.ID.getValue(), new ObjectId(cart.getId()));
            document.put(CartRepository.Fields.START_ORDER_TIME.getValue(), now);
            document.put(CartRepository.Fields.TOTAL_PRICE.getValue(), cart.getCalculatePrice());
        }
        return document;
    }

    public Document getEditDocument(Cart cart) {
        Document doc = new Document();
        if (cart != null) {
            if (cart.getId() != null)
                doc.put(Fields.ID.getValue(), cart.getId());
            if (cart.getTotalShopCard() != null)
                doc.put(Fields.TOTAL_PRICE.getValue(), cart.getTotalShopCard());
            if (cart.getProducts() != null) {
                doc.put(Fields.PRODUCTS.getValue(), cart.getProducts());
            }

        }
        return doc;
    }

    public Cart getOrderEntity(Document doc) {
        Cart cart = new Cart();
        if (doc != null) {
            cart.setId(String.valueOf(doc.getObjectId(Fields.ID.getValue())));
            cart.setUserId(String.valueOf(doc.getObjectId(Fields.USER_ID.getValue())));
            cart.setCalculatePrice(doc.getString(Fields.TOTAL_PRICE.getValue()));
            cart.setStartOrderTime(doc.getLong(Fields.START_ORDER_TIME.getValue()));
            cart.setTotalShopCard(doc.getInteger(Fields.TOTAL_SHOP_CARD.getValue()));

            // Products
            if (doc.get(Fields.PRODUCTS.getValue()) != null) {
                List<Map<String, String>> productMap = new ArrayList<>();
                List<Document> productList = (List<Document>) doc.get(Fields.PRODUCTS.getValue());
                for (Document document : productList) {
                    Map<String, String> outputProducts = new ConcurrentHashMap<>();
                    outputProducts.put(Fields.ID.getValue(), String.valueOf(document.getObjectId(Fields.ID.getValue())));
                    outputProducts.put(ProductRepository.Fields.NAME.getValue(), String.valueOf(document.getString(ProductRepository.Fields.NAME.getValue())));
                    outputProducts.put(ProductRepository.Fields.PRICE.getValue(), String.valueOf(document.getString(ProductRepository.Fields.PRICE.getValue())));
                    productMap.add(outputProducts);
                }
                cart.setOutputProducts(productMap);
            }
        }
        return cart;
    }

    public List<Document> getDataById(ObjectId id) {
        Bson find_doc = Filters.and(Filters.eq(ProductRepository.Fields.ID.getValue(), id));
        return getList(find_doc);
    }


    public boolean exists(String key, ObjectId value) {
        Document query = new Document(key, new Document("$exists", true)).append(Fields.ID.getValue(), value);
        return getMongoCollection().count(query) == 1;
    }

    public UpdateResult update(ObjectId id, Document updateDoc) {
        Document find_doc = new Document();
        find_doc.put(ProductRepository.Fields.ID.getValue(), id);
        return updateOne(find_doc, new Document("$set", updateDoc));
    }


    // add product
    public UpdateResult updatePushArrayAndIncrementCount(ObjectId cardId, String array_name, String increment_name, Object new_doc) {
        List<Bson> findList = new ArrayList<>();
        findList.add(Filters.eq(Fields.ID.getValue(), cardId));
        Bson find_doc = Filters.and(findList);

        Document push = new Document();
        push.put(array_name, new_doc);

        Document updatedoc = new Document();
        updatedoc.put("$push", push);
        updatedoc.put("$inc", new Document(increment_name, (long) 1));

        return getMongoCollection().updateOne(find_doc, updatedoc);
    }

    // remove product
    public UpdateResult remove(ObjectId cardId, ObjectId productId) {
        Document find_doc = new Document();
        find_doc.put(Fields.ID.getValue(), cardId);

        Document inner_doc = new Document();
        inner_doc.put(Fields.ID.getValue(), productId);

        Document remove_doc = new Document();
        remove_doc.put(Fields.PRODUCTS.getValue(), inner_doc);

        return updateOne(find_doc, new Document("$pull", remove_doc));
    }

    public DeleteResult remove(ObjectId cardId) {
        Document find_doc = new Document();
        find_doc.put(ProductRepository.Fields.ID.getValue(), cardId);
        return deleteOne(find_doc);
    }


    public List<Document> findByCardIdAndProductId(ObjectId cardId, ObjectId productId) {
        List<Bson> findList = new ArrayList<>();
        findList.add(Filters.eq(Fields.ID.getValue(), cardId));
        findList.add(Filters.eq(Fields.PRODUCTS.getValue() + "." + Fields.ID, productId));
        Bson find_doc = Filters.and(findList);
        return getList(find_doc);
    }

    public enum Fields {

        ID("_id"),
        USER_ID("user_id"),
        START_ORDER_TIME("start_order_time"),
        TOTAL_SHOP_CARD("count"),
        TOTAL_PRICE("total_price"),
        PRODUCTS("products"),;


        private String value;

        Fields(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }

    }
}

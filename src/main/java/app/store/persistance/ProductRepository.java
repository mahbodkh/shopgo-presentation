package app.store.persistance;

import app.store.persistance.domain.Category;
import app.store.persistance.domain.Product;
import app.store.util.General;
import com.mongodb.client.model.Filters;
import com.mongodb.client.model.Projections;
import com.mongodb.client.model.Sorts;
import com.mongodb.client.result.DeleteResult;
import com.mongodb.client.result.UpdateResult;
import org.bson.Document;
import org.bson.conversions.Bson;
import org.bson.types.ObjectId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import static com.mongodb.client.model.Filters.eq;


@Component
public class ProductRepository extends MongoBasic {

    private static final String productCollection = "Product";
    public static MongoDB mongo = MongoDB.getInstance();
    private final Logger log = LoggerFactory.getLogger(ProductRepository.class);

    @Autowired
    private CategoryRepository categoryRepository;

    public ProductRepository() {
        super(mongo, productCollection);
    }

    public Document getProductDocument(Product product) {
        Document document = new Document();
        if (product != null) {
            if (product.getId() != null)
                document.put(Fields.ID.getValue(), new ObjectId(product.getId()));
            else document.put(Fields.ID.getValue(), new ObjectId());
            if (product.getCategory() != null)
                document.put(Fields.CATEGORY.getValue(), new ObjectId(product.getCategory()));
            document.put(Fields.NAME.getValue(), General.parseString(product.getName()));
            document.put(Fields.DESCRIPTION.getValue(), General.parseString(product.getDescription()));
            document.put(Fields.PRICE.getValue(), product.getPrice());
            document.put(Fields.WEIGHT.getValue(), product.getWeight());
            document.put(Fields.HEIGHT.getValue(), product.getHeight());
            document.put(Fields.WIDTH.getValue(), product.getWidth());
            document.put(Fields.COUNT.getValue(), product.getCount());
            document.put(Fields.EXIST.getValue(), product.getExist());
            document.put(Fields.IMAGEURL.getValue(), product.getUrls());
            document.put(Fields.INSERTTIME.getValue(), product.getInsertTime());
            document.put(Fields.LASTCHANGETIME.getValue(), product.getLastChangeTime());
            document.put(Fields.CATEGORIES.getValue(), product.getCategories());
            if (product.getKeywords() != null)
                document.put(Fields.KEYWORDS.getValue(), product.getKeywords());
            else
                document.put(Fields.KEYWORDS.getValue(), new ArrayList<>());

        }
        return document;
    }

    public Document getProductEditDocument(Product product) {
        Document document = new Document();
        try {
            if (product.getCategory() != null)
                document.put(Fields.CATEGORY.getValue(), new ObjectId(product.getCategory()));
            if (!General.isEmpty(product.getName()))
                document.put(Fields.NAME.getValue(), General.parseString(product.getName()));
            if (!General.isEmpty(product.getDescription()))
                document.put(Fields.DESCRIPTION.getValue(), General.parseString(product.getDescription()));
            if (!General.isEmpty(product.getPrice()))
                document.put(Fields.PRICE.getValue(), product.getPrice());
            if (!General.isEmpty(product.getWeight()))
                document.put(Fields.WEIGHT.getValue(), product.getWeight());
            if (!General.isEmpty(product.getHeight()))
                document.put(Fields.HEIGHT.getValue(), product.getHeight());
            if (!General.isEmpty(product.getWidth()))
                document.put(Fields.WIDTH.getValue(), product.getWidth());
            if (product.getCount() != null)
                document.put(Fields.COUNT.getValue(), product.getCount());
            if (product.getExist() != null)
                document.put(Fields.EXIST.getValue(), product.getExist());
            if (product.getUrls() != null)
                document.put(Fields.IMAGEURL.getValue(), product.getUrls());
            if (product.getInsertTime() != null)
                document.put(Fields.INSERTTIME.getValue(), product.getInsertTime());
            if (product.getLastChangeTime() != null)
                document.put(Fields.LASTCHANGETIME.getValue(), product.getLastChangeTime());
            if (product.getCategories() != null)
                document.put(Fields.CATEGORIES.getValue(), product.getCategories());
            if (product.getKeywords() != null)
                document.put(Fields.KEYWORDS.getValue(), product.getKeywords());
            else
                document.put(Fields.KEYWORDS.getValue(), new ArrayList<>());

        } catch (Exception e) {
            log.debug("cant convert product object to document in ProductRepository : {} ", e);
            e.printStackTrace();
        }

        return document;
    }

    public Product getProductEntity(Document document) {
        Product product = new Product();
        if (document != null) {
            product.setId(String.valueOf(document.getObjectId(Fields.ID.getValue())));
            product.setCategory(String.valueOf(document.getObjectId(Fields.CATEGORY.getValue())));
            product.setSubCategory(String.valueOf(document.getInteger(Fields.SUBCATEGORY.getValue())));
            product.setName(General.parseString(document.getString(Fields.NAME.getValue())));
            product.setDescription(General.parseString(document.getString(Fields.DESCRIPTION.getValue())));
            product.setPrice(General.parseString(document.getString(Fields.PRICE.getValue())));
            product.setWeight(General.parseString(document.getString(Fields.WEIGHT.getValue())));
            product.setHeight(General.parseString(document.getString(Fields.HEIGHT.getValue())));
            product.setWidth(General.parseString(document.getString(Fields.WIDTH.getValue())));
            product.setCount(document.getInteger(Fields.COUNT.getValue()));
            product.setExist(document.getBoolean(Fields.EXIST.getValue()));
            product.setInsertTime(document.getLong(Fields.INSERTTIME.getValue()));
            product.setLastChangeTime(document.getLong(Fields.LASTCHANGETIME.getValue()));

            // categories
            if (document.get(Fields.CATEGORIES.getValue()) != null) {
                Category category = new Category();
                List<Category> categoryList = new ArrayList<>();
                Document categories = (Document) document.get(Fields.CATEGORIES.getValue());
                category.setId(String.valueOf(categories.getObjectId(Fields.ID.getValue())));
                category.setName(categories.getString(Fields.NAME.getValue()));
                categoryList.add(category);
                product.setCategories(categoryList);
            }

            // keywords
            if (document.get(Fields.KEYWORDS.getValue()) != null) {
                List<String> keywords = (List<String>) document.get(Fields.KEYWORDS.getValue());
                List<String> words = new ArrayList<>();
                for (String s : keywords) {
                    if (s != null) {
                        s.split(" ");
                        words.add(s);
                    } else words.add("");
                }
                product.setKeywords(words);
            } else
                product.setKeywords(new ArrayList<>());
        }
        return product;
    }


    public Product getProductUpdateEntity(Document document) {
        Product product = new Product();
        if (document != null) {
            product.setId(String.valueOf(document.getObjectId(Fields.ID.getValue())));
            product.setCategory(String.valueOf(document.getObjectId(Fields.CATEGORY.getValue())));
            product.setSubCategory(String.valueOf(document.getInteger(Fields.SUBCATEGORY.getValue())));
            product.setName(General.parseString(document.getString(Fields.NAME.getValue())));
            product.setDescription(General.parseString(document.getString(Fields.DESCRIPTION.getValue())));
            product.setPrice(General.parseString(document.getString(Fields.PRICE.getValue())));
            product.setWeight(General.parseString(document.getString(Fields.WEIGHT.getValue())));
            product.setHeight(General.parseString(document.getString(Fields.HEIGHT.getValue())));
            product.setWidth(General.parseString(document.getString(Fields.WIDTH.getValue())));
            product.setCount(document.getInteger(Fields.COUNT.getValue()));
            product.setExist(document.getBoolean(Fields.EXIST.getValue()));
            product.setInsertTime(document.getLong(Fields.INSERTTIME.getValue()));
            product.setLastChangeTime(document.getLong(Fields.LASTCHANGETIME.getValue()));

            // categories
            if (document.get(Fields.CATEGORIES.getValue()) != null) {
                Category category = new Category();
                List<Category> categoryList = new ArrayList<>();
                Document categories = (Document) document.get(Fields.CATEGORIES.getValue());
                category.setId(String.valueOf(categories.getObjectId(Fields.ID.getValue())));
                category.setName(categories.getString(Fields.NAME.getValue()));
                categoryList.add(category);
                product.setCategories(categoryList);
            }

            // keywords
            List<String> keywords = (List<String>) document.get(Fields.KEYWORDS.getValue());
            product.setKeywords(keywords);
        }
        return product;
    }

    public Document getProductForOrder(Product product) {
        Document document = new Document();
        if (product != null) {
            if (product.getId() != null)
                document.put(Fields.ID.getValue(), new ObjectId(product.getId()));
            else
                document.put(Fields.ID.getValue(), new ObjectId());
            if (!General.isEmpty(product.getName()))
                document.put(Fields.NAME.getValue(), General.parseString(product.getName()));
            if (!General.isEmpty(product.getPrice()))
                document.put(Fields.PRICE.getValue(), product.getPrice());
            if (product.getInsertTime() != null)
                document.put(Fields.INSERTTIME.getValue(), product.getInsertTime());
        }
        return document;
    }


    public List<Product> getProductList(List<Document> products) {
        Product productEntity = null;
        List<Product> productList = new ArrayList<>();
        for (Document document : products) {
            productEntity = getProductEntity(document);
            productList.add(productEntity);
        }
        return productList;
    }


    public Document insert(Document document) {
        insertOne(document);
        return document;
    }

    public List<Document> getDataById(ObjectId id) {
        Bson find_doc = Filters.and(Filters.eq(Fields.ID.getValue(), id));
        return getList(find_doc);
    }

    public List<Document> getDataById(ObjectId objectId, ObjectId category, ObjectId subcategory) {
        List<Bson> findList = new ArrayList<>();
        findList.add(Filters.eq(Fields.ID.getValue(), objectId));
//        findList.add(Filters.eq(Fields.CATEGORY.getValue(), Optional.ofNullable(category).orElse(null)));
//        findList.add(Filters.eq(Fields.SUBCATEGORY.getValue(), Optional.ofNullable(subcategory).orElse(null)));
        Bson find_doc = Filters.and(findList);
        return getList(find_doc);
    }

    public List<Document> findAll() {
        Document find_doc = new Document();
        return getList(find_doc);
    }

    public List<Document> findAll(Integer limit) {
        Document find_doc = new Document();
        return getList(find_doc, limit);
    }

    public DeleteResult deleteAll() {
        Document find_doc = new Document();
        return remove(find_doc);
    }

//    public UpdateResult update(ObjectId id, Document updateDoc) {
//        Document find_doc = new Document();
//        find_doc.put(Fields.ID.getValue(), id);
//        return updateOne(find_doc, new Document("$set", updateDoc));
//    }

    public UpdateResult update(ObjectId id, Document updateDoc) {
        Document find_doc = new Document();
        find_doc.put(Fields.ID.getValue(), id);
        return updateOne(find_doc, new Document("$set", updateDoc));
    }


    public DeleteResult remove(ObjectId objectId) {
        Document find_doc = new Document();
        find_doc.put(Fields.ID.getValue(), objectId);
        return deleteOne(find_doc);
    }

    public UpdateResult addCategoryToProductUpdate(Document product, Document category, String array_name) {
        Document find_doc = new Document();
        find_doc.put(Fields.ID.getValue(), product.getObjectId(Fields.ID.getValue()));
        find_doc.put(Fields.CATEGORY.getValue(), category.getObjectId(Fields.ID.getValue()));


        Document custom = new Document();
        custom.put(Fields.ID.getValue(), category.getObjectId(Fields.ID.getValue()));
        custom.put(CategoryRepository.Fields.NAME.getValue(), category.getString(Fields.NAME.getValue()));

        Document push = new Document();
        push.put(array_name, custom);

        Document updatedoc = new Document();
        updatedoc.put("$push", push);
//        updatedoc.put("$inc", new Document(increment_name, (long) 1));

        return getMongoCollection().updateOne(find_doc, updatedoc);
    }


    public List<Document> doSearchRegex(String keyword, Integer limit) {
        keyword = keyword.replaceAll("[\\p{C}.*%$\\\\/()\\[\\]()\\r\\n]", "");

        Bson filterProduct = eq(Fields.KEYWORDS.getValue(), Pattern.compile(keyword));
        Bson filterRegex = Filters.or(filterProduct);
        Bson findBson = filterRegex;

        return getList(findBson, limit);
    }

    public List<Document> doSearchRegex(String keyword) {
        keyword = keyword.replaceAll("[\\p{C}.*%$\\\\/()\\[\\]()\\r\\n]", "");

        Bson filterProduct = eq(Fields.KEYWORDS.getValue(), Pattern.compile(keyword));
        Bson filterRegex = Filters.or(filterProduct);
        Bson findBson = filterRegex;

        return getList(findBson);
    }

    public List<Document> doSearchText(String keyword) {

        String allkeywords = "";
        String[] split = keyword.split(" ");
        if (split != null)
            for (String s : split) {
                if (s != null && !s.trim().isEmpty())
                    allkeywords += "  \"" + s.trim() + "\"";
            }
        Bson filterTextSearch = Filters.text(allkeywords);
        Bson findBson = filterTextSearch;
        return getList(findBson);
    }

    public List<Document> doSearchText(String keyword, Integer limit) {
        String allkeywords = "";
        String[] split = keyword.split(" ");
        if (split != null)
            for (String s : split) {
                if (s != null && !s.trim().isEmpty())
                    allkeywords += "  \"" + s.trim() + "\"";
            }
        Bson filterTextSearch = Filters.text(allkeywords);
        Bson findBson = filterTextSearch;
        return getList(findBson, limit);
    }

    public List<Document> doSearchText(String keyword, Integer skip, Integer limit) {

        String allkeywords = "";
        String[] split = keyword.split(" ");

        if (split != null)
            for (String s : split) {
                if (s != null && !s.trim().isEmpty())
                    allkeywords += "  \"" + s.trim() + "\"";
            }
        Bson filterTextSearch = Filters.text(allkeywords);
        Bson findBson = filterTextSearch;
        Bson sort = Sorts.metaTextScore("score");
        Bson projection = Projections.metaTextScore("score");
        return getList(findBson, projection, skip, limit, sort);

    }

    public boolean exists(String key, ObjectId value) {
//        Document query = new Document("$exist", true).append(Fields.ID.getValue(), key);
        Document query = new Document(key, new Document("$exists", true)).append("_id", value);
//        Bson query = new Document(key.toString(), new Document("$exists", true));
        return getMongoCollection().count(query) == 1;
    }


//    public boolean exists(String name, ObjectId key) {
//        Document query = new Document(name, new Document("$exists", true)).append("_id", key);
//        return getMongoCollection().count(query) == 1;
//    }

    public enum Fields {

        ID("_id"),
        CATEGORY("category"),
        SUBCATEGORY("subcategory"),
        NAME("name"),
        DESCRIPTION("description"),
        PRICE("price"),
        WEIGHT("weight"),
        HEIGHT("height"),
        WIDTH("width"),
        COUNT("count"),
        EXIST("exist"),
        IMAGEURL("imageUrl"),
        INSERTTIME("insertTime"),
        LASTCHANGETIME("lastChangeTime"),
        IMAGEONE("imageOne"),
        CATEGORIES("categories"),

        IMAGE_URL("url"),
        KEYWORDS("keywords");


        private String value;

        Fields(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }

    }

}

package app.store.persistance;

import app.store.persistance.domain.User;
import app.store.util.General;
import com.mongodb.client.model.Filters;
import com.mongodb.client.result.DeleteResult;
import com.mongodb.client.result.UpdateResult;
import org.bson.Document;
import org.bson.conversions.Bson;
import org.bson.types.ObjectId;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class UserRepository extends MongoBasic {

    private static final String userCollectionName = "User";
    private static MongoDB mongo = MongoDB.getInstance();
    private long now = System.currentTimeMillis();

    protected UserRepository() {
        super(mongo, userCollectionName);
    }

    public Document getUserDocument(User user) {
        Document document = new Document();
        if (user != null) {
            if (user.getId() != null)
                document.put(UserRepository.Fields.ID.getValue(), new ObjectId(user.getId()));
            else document.put(UserRepository.Fields.ID.getValue(), new ObjectId());
            document.put(Fields.FIRSTNAME.getValue(), General.parseString(user.getFirstName()));
            document.put(Fields.LASTNAME.getValue(), General.parseString(user.getLastName()));
            document.put(Fields.CREATEDATE.getValue(), now);
            document.put(Fields.EMAIL.getValue(), user.getEmail());
            document.put(Fields.MOBILE.getValue(), user.getMobile());
            document.put(Fields.CITY.getValue(), user.getCity());
            document.put(Fields.ADDRESS.getValue(), user.getAddress());
        }
        return document;
    }

    public User getUserEntity(Document document) {
        User user = new User();
        if (document != null) {
            user.setId(String.valueOf(document.getObjectId(UserRepository.Fields.ID.getValue())));
            user.setFirstName(document.getString(Fields.FIRSTNAME.getValue()));
            user.setLastName(document.getString(Fields.LASTNAME.getValue()));
            user.setCreationDate(document.getLong(Fields.CREATEDATE.getValue()));
            user.setEmail(document.getString(Fields.EMAIL.getValue()));
            user.setMobile(document.getString(Fields.MOBILE.getValue()));
            user.setCity(document.getString(Fields.CITY.getValue()));
            user.setAddress(document.getString(Fields.ADDRESS.getValue()));
        }
        return user;
    }


    public Document insert(Document document) {
        insertOne(document);
        return document;
    }

    public List<Document> getDataById(ObjectId objectId) {
        Bson find_doc = Filters.and(Filters.eq(Fields.ID.getValue(), objectId));
        return getList(find_doc);
    }

    public List<Document> findAll() {
        Document find_doc = new Document();
        return getList(find_doc);
    }

    public List<Document> findAll(Integer limit) {
        Document find_doc = new Document();
        return getList(find_doc, limit);
    }

    public UpdateResult update(ObjectId id, Document updateDoc) {
        Document find_doc = new Document();
        find_doc.put(Fields.ID.getValue(), id);
        return updateOne(find_doc, new Document("$set", updateDoc));
    }

    public DeleteResult remove(ObjectId objectId) {
        Document find_doc = new Document();
        find_doc.put(Fields.ID.getValue(), objectId);
        return deleteOne(find_doc);
    }

    public boolean exists(String key, ObjectId value) {
        Document query = new Document(key, new Document("$exists", true)).append(Fields.ID.getValue(), value);
        return getMongoCollection().count(query) == 1;
    }


    public enum Fields {
        ID("_id"),
        ROLE("role"),
        FIRSTNAME("firstname"),
        LASTNAME("lastname"),
        GENDER("gender"),
        MOBILE("mobile"),
        EMAIL("email"),
        ADDRESS("address"),
        ZIPCODE("zipcode"),
        CITY("city"),
        LOCATION("location"),

        //
        CREATEDATE("createdate"),
        LASTLOGIN(""),
        VERIFY(""),;

        private String value;

        Fields(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }

    }
}

package app.store.persistance;

import com.mongodb.client.model.Filters;
import com.mongodb.client.result.UpdateResult;
import org.bson.Document;
import org.bson.conversions.Bson;
import org.bson.types.ObjectId;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class PaymentRepository extends MongoBasic {

    private static final String collectionName = "Payment";
    private static MongoDB mongoDB = MongoDB.getInstance();

    public PaymentRepository() {
        super(mongoDB, collectionName);
    }

    public List<Document> getDataByObjectId(ObjectId objectId) {
        List<Bson> findList = new ArrayList<>();
        findList.add(Filters.eq(BankRepository.Fields.ID.getValue(), objectId));
        Bson find_doc = Filters.and(findList);
        return getList(find_doc);
    }

    public List<Document> getDataByObjectId(String objectId) {
        Document find_doc = new Document();
        find_doc.put(Fields.ID.getValue(), objectId);
        return getList(find_doc);
    }

    public UpdateResult update(Document old, Document doc) {
        return updateOne(old, doc);
    }


    public List<Document> getById(ObjectId id) {
        Bson find_doc = Filters.and(Filters.eq(Fields.ID.getValue(), id));
        return getList(find_doc);
    }

    public List<Document> getByConfirmCode(String orderId) {
        Bson find_doc = Filters.and(Filters.eq(Fields.User_ORDERID.getValue(), orderId));
        return getList(find_doc);
    }


    public UpdateResult updatePushArrayAndIncrementCount(ObjectId leagueId, String array_name, String increment_name, Object new_doc) {
        List<Bson> findList = new ArrayList<>();
        findList.add(Filters.eq(Fields.ID.getValue(), leagueId));
        Bson find_doc = Filters.and(findList);

        Document push = new Document();
        push.put(array_name, new_doc);

        Document updatedoc = new Document();
        updatedoc.put("$push", push);
        updatedoc.put("$inc", new Document(increment_name, (long) 1));

        return getMongoCollection().updateOne(find_doc, updatedoc);
    }

    public UpdateResult updateWithObjectId(ObjectId objectId, Document document) {
        List<Bson> findList = new ArrayList<>();
        findList.add(Filters.eq(Fields.ID.getValue(), objectId));
        Bson find_doc = Filters.and(findList);

        return updateOne(find_doc, new Document("$set", document));
    }


    public enum Fields {
        ID("_id"),
        BANK_ID("bank_id"),
        AMOUNT("amount"),
        PAYMENT_TIME("paymentTime"),
        VERIFY_TIME("verifyTime"),
        AUTHORITY("authority"),
        STATUSHOLDER("statusHolder"),
        REFIDHOLDER("refIdHolder"),
        TRANSACTIONSTATUS("transactionStatus"),
        CALLBACKURL_BANK("callBackUrl_bank"),
        CALLBACKURL_USER("callBackUrl_user"),
        User_ORDERID("userOrderId"),
        GATEWAYURL("gatewayurl"),
        ERROR("error"),
        //SUBSCRIBER_ID("subscriberId"),
        PAY_ORDERID("paykOrderId"),
        SALE_REFRENCEID("saleReferenceId"),//refrence from bank to user
        MESSAGE("message"),

//        CALLBACKRETURNAUTHORITY("callbackreturnauthority"),
//        CALLBACKRETURNSTATUS("callbackreturnstatus"),
//        CALLBACKRETURNAPPID("callbackreturnappid"),
//        CALLBACKRETURNPHONENUMBER("callbackreturnphonenumber"),

        ;

        private String value;

        Fields(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }

    }


}

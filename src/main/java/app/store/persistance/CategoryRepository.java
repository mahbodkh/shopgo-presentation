package app.store.persistance;

import app.store.persistance.domain.Category;
import app.store.util.General;
import com.mongodb.client.model.*;
import com.mongodb.client.result.DeleteResult;
import com.mongodb.client.result.UpdateResult;
import org.bson.Document;
import org.bson.conversions.Bson;
import org.bson.types.ObjectId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import static com.mongodb.client.model.Filters.eq;

@Component
public class CategoryRepository extends MongoBasic {


    private final static String collectionName = "Category";
    private static MongoDB mongo = MongoDB.getInstance();
    private final Logger log = LoggerFactory.getLogger(CategoryRepository.class);


    protected CategoryRepository() {
        super(mongo, collectionName);
    }

    public Document getCategoryDocument(Category category) {
        ObjectId objectId = new ObjectId();
        Document document = new Document();
        if (category != null) {
            if (category.getId() == null)
                document.put(CategoryRepository.Fields.ID.getValue(), objectId);
            else
                document.put(CategoryRepository.Fields.ID.getValue(), category.getId());
            document.put(Fields.CODE.getValue(), category.getCode());
            document.put(Fields.NAME.getValue(), category.getName());
            document.put(Fields.DESCRIPTION.getValue(), category.getDescription());
            document.put(Fields.URL.getValue(), category.getUrl());
        }
        return document;
    }

    public Category getCategoryEntity(Document document) {
        Category category = new Category();
        try {
            category.setId(String.valueOf(document.getObjectId(Fields.ID.getValue())));
            category.setCode(document.getInteger(Fields.CODE.getValue()));
            category.setName(General.parseString(document.getString(Fields.NAME.getValue())));
            category.setDescription(General.parseString(document.getString(Fields.DESCRIPTION.getValue())));
            category.setUrl(document.getString(Fields.URL.getValue()));
            category.setMemberCount(document.getLong(Fields.MEMBERCOUNT.getValue()));

        } catch (Exception e) {
            log.warn("Document convert to Category Object : {} ][ {} ", document, e);
            e.printStackTrace();
        }
        return category;
    }

    public Document getCategoryEditDocument(Category category) {
        Document document = new Document();
        if (category != null) {
            if (category.getId() != null)
                document.put(Fields.ID.getValue(), category.getId());
            if (category.getCode() != null)
                document.put(Fields.CODE.getValue(), category.getCode());
            if (category.getName() != null)
                document.put(Fields.NAME.getValue(), category.getName());
            if (category.getDescription() != null)
                document.put(Fields.DESCRIPTION.getValue(), category.getDescription());
            if (category.getUrl() != null)
                document.put(Fields.URL.getValue(), category.getUrl());
        }
        return document;
    }


    public Document getSubCategoryDocument(Category Category) {
        Document document = new Document();
        if (Category != null) {
            document.put(Fields.SUBCATEGORIES.getValue() + ".$." + Fields.ID.getValue(), Category.getId());
            document.put(Fields.SUBCATEGORIES.getValue() + ".$." + Fields.CODE.getValue(), Category.getCode());
            document.put(Fields.SUBCATEGORIES.getValue() + ".$." + Fields.NAME.getValue(), Category.getName());
            document.put(Fields.SUBCATEGORIES.getValue() + ".$." + Fields.DESCRIPTION.getValue(), Category.getDescription());
            document.put(Fields.SUBCATEGORIES.getValue() + ".$." + Fields.URL.getValue(), Category.getUrl());
        }
        return document;
    }


    public Document insert(Document document) {
        insertOne(document);
        return document;
    }

    public List<Document> getDataById(ObjectId id) {
        Bson find_doc = Filters.and(Filters.eq(Fields.ID.getValue(), id));
        return getList(find_doc);
    }

    public List<Document> findAll() {
        Document find_doc = new Document();
        return getList(find_doc);
    }

    public List<Document> findAll(Integer limit) {
        Document find_doc = new Document();
        return getList(find_doc, limit);
    }

    public UpdateResult update(ObjectId id, Document updateDoc) {
        Document find_doc = new Document();
        find_doc.put(Fields.ID.getValue(), id);
        return updateOne(find_doc, new Document("$set", updateDoc));
    }

    public UpdateResult update(Document updateDoc) {
        Document find_doc = new Document();
        find_doc.put(Fields.ID.getValue(), updateDoc.getObjectId(Fields.ID.getValue()));
        return updateOne(find_doc, new Document("$set", updateDoc));
    }

    public DeleteResult remove(ObjectId objectId) {
        Document find_doc = new Document();
        find_doc.put(Fields.ID.getValue(), objectId);
        return deleteOne(find_doc);
    }

    public DeleteResult deleteAll() {
        Document find_doc = new Document();
        return remove(find_doc);
    }

    public List<Document> getDataByNameOrCode(Category category) {
        Bson find_doc = Filters.or(
                Filters.eq(Fields.CODE.getValue(), category.getCode())
                , Filters.eq(Fields.NAME.getValue(), category.getName())
        );


//        List<Bson> findList = new ArrayList<>();
//        if (category.getId() != null)
//            findList.add(Filters.eq(Fields.ID.getValue(), category.getId()));
//        findList.add(Filters.eq(Fields.ID.getValue(), category.getName()));
//        Bson find_doc = Filters.and(findList);


        return getList(find_doc);
    }

    public Document addCategoryToProduct(Document document) {
        //todo
        return null;
    }

    //sub category --
    public Document updateSubCategory(ObjectId id, Document updateDoc) {
        Document find_doc = new Document();
        find_doc.put(Fields.ID.getValue(), id);

        Document update_doc = new Document();
        update_doc.put(Fields.SUBCATEGORIES.getValue() /*+ "." + Fields.ID.getValue()*/, updateDoc);

        FindOneAndUpdateOptions findOptions = new FindOneAndUpdateOptions();
        findOptions.upsert(true);
        findOptions.returnDocument(ReturnDocument.AFTER);

        return findOneAndUpdate(find_doc, new Document("$set", update_doc), findOptions);
    }

    public UpdateResult updateSubCategory(ObjectId cat, ObjectId sub, Document document) {
        List<Bson> findList = new ArrayList<>();

        findList.add(Filters.eq(Fields.ID.getValue(), cat));
        findList.add(Filters.eq(Fields.SUBCATEGORIES.getValue() + "." + Fields.ID.getValue(), sub));
        Bson find_doc = Filters.and(findList);

        return updateOne(find_doc, new Document("$set", document));
    }

    public UpdateResult addSubCategory(ObjectId objectId, Document sub) {
        List<Bson> findList = new ArrayList<>();
        findList.add(Filters.eq(Fields.ID.getValue(), objectId));
        Bson find_doc = Filters.and(findList);

        Document push = new Document();
        push.put(Fields.SUBCATEGORIES.getValue(), sub);

        Document updatedoc = new Document();
        updatedoc.put("$push", push);


        return getMongoCollection().updateOne(find_doc, updatedoc);
    }

    public Document getDataBySubIdCatId(ObjectId catId, ObjectId subId, Document update) {
        List<Bson> findList = new ArrayList<>();
        findList.add(Filters.eq(Fields.ID.getValue(), catId));
        findList.add(Filters.eq(Fields.SUBCATEGORIES.getValue() + "." + Fields.ID.getValue(), subId));
        Bson find_doc = Filters.and(findList);

        Document update_doc = new Document();
        update_doc.put(Fields.SUBCATEGORIES.getValue() + "." + Fields.ID.getValue(), update);

        Document set = new Document();
        set.put("$set", update);

        return getMongoCollection().findOneAndUpdate(find_doc, set);
    }


    public UpdateResult removeSubCategory(ObjectId cat, ObjectId sub) {
        Document find_doc = new Document();
        find_doc.put(Fields.ID.getValue(), cat);

        Document inner_doc = new Document();
        inner_doc.put(Fields.ID.getValue(), sub);

        Document del_doc = new Document();
        del_doc.put(Fields.SUBCATEGORIES.getValue(), inner_doc);

        return updateOne(find_doc, new Document("$pull", del_doc));
    }

    public List<Document> doSearchRegex(String keyword, Integer limit) {
        keyword = keyword.replaceAll("[\\p{C}.*%$\\\\/()\\[\\]()\\r\\n]", "");

        Bson filterCategory = eq(Fields.NAME.getValue(), Pattern.compile(keyword));
        Bson filterRegex = Filters.or(filterCategory);
        Bson findBson = filterRegex;

        return getList(findBson, limit);
    }

    public List<Document> doSearchRegex(String keyword) {
        keyword = keyword.replaceAll("[\\p{C}.*%$\\\\/()\\[\\]()\\r\\n]", "");

        Bson filterCategory = eq(Fields.NAME.getValue(), Pattern.compile(keyword));
        Bson filterRegex = Filters.or(filterCategory);
        Bson findBson = filterRegex;

        return getList(findBson);
    }

    public List<Document> doSearchText(String keyword) {
        String allkeywords = "";
        String[] split = keyword.split(" ");

        if (split != null)
            for (String s : split) {
                if (s != null && !s.trim().isEmpty())
                    allkeywords += "  \"" + s.trim() + "\"";
            }
        Bson filterTextSearch = Filters.text(allkeywords);

        Bson findBson = filterTextSearch;
        return getList(findBson);
    }

    public List<Document> doSearchText(String keyword, Integer limit) {
        String allkeywords = "";
        String[] split = keyword.split(" ");

        if (split != null)
            for (String s : split) {
                if (s != null && !s.trim().isEmpty())
                    allkeywords += "  \"" + s.trim() + "\"";
            }
        Bson filterTextSearch = Filters.text(allkeywords);

        Bson findBson = filterTextSearch;
        return getList(findBson, limit);
    }

    public List<Document> doSearchText(String keyword, Integer skip, Integer limit) {

        String allkeywords = "";
        String[] split = keyword.split(" ");

        if (split != null)
            for (String s : split) {
                if (s != null && !s.trim().isEmpty())
                    allkeywords += "  \"" + s.trim() + "\"";
            }
        Bson filterTextSearch = Filters.text(allkeywords);

        Bson findBson = filterTextSearch;
        Bson sort = Sorts.metaTextScore("score");
        Bson projection = Projections.metaTextScore("score");

        return getList(findBson, projection, skip, limit, sort);

    }


    public enum Fields {
        ID("_id"),
        CODE("code"),
        MEMBERCOUNT("member_count"),
        NAME("name"),
        DESCRIPTION("description"),
        URL("url"),

        SUBCATEGORIES("subcategories"),;


        private String value;

        Fields(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }

    }

}

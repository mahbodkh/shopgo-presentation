package app.store.persistance;

import com.mongodb.client.model.Filters;
import com.mongodb.client.result.UpdateResult;
import org.bson.Document;
import org.bson.conversions.Bson;
import org.bson.types.ObjectId;

import java.util.ArrayList;
import java.util.List;

public class BankRepository extends MongoBasic {

    private static final String collectionName = "BankConfig";
    private static MongoDB mongoDB = MongoDB.getInstance();

    public BankRepository() {
        super(mongoDB, collectionName);
    }

    public List<Document> getById(ObjectId id) {
        Bson find_doc = Filters.and(Filters.eq(BankRepository.Fields.ID.getValue(), id));
        return getList(find_doc);
    }

    public List<Document> getBankByCode(Integer code) {
        List<Bson> findList = new ArrayList<>();
        findList.add(Filters.eq(Fields.CODE.getValue(), code));
        Bson find_doc = Filters.and(findList);
        return getList(find_doc);
    }

    public UpdateResult updateBank(ObjectId objectId, Document document) {
        List<Bson> findList = new ArrayList<>();
        findList.add(Filters.eq(Fields.ID.getValue(), objectId));
        Bson find_doc = Filters.and(findList);
        return updateOne(find_doc, new Document("$set", document));
    }

    public List<Document> getBankByobjectId(ObjectId id) {
        List<Bson> findList = new ArrayList<>();
        findList.add(Filters.eq(Fields.ID.getValue(), id));
        Bson find_doc = Filters.and(findList);
        return getList(find_doc);
    }

//    public GateWay.BankGet.Builder getBankName(Document doc) {
//        GateWay.BankGet.Builder builder = GateWay.BankGet.newBuilder();
//        try {
//            builder.setBankCode((doc.getInteger(Fields.CODE.getValue())).toString());
//            builder.setBankName(doc.getString(Fields.NAME.getValue()));
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        return builder;
//    }

    public List<Document> getMerchant(String merchant) {
        List<Bson> findList = new ArrayList<>();
        findList.add(Filters.eq(Fields.MERCHANT.getValue(), merchant));
        Bson find_doc = Filters.and(findList);
        return getList(find_doc);
    }

    public Document insert(Document document) {
        insertOne(document);
        return document;
    }

    public List<Document> findAll() {
        Document find_doc = new Document();
        return getList(find_doc);
    }

    public enum Fields {

        ID("_id"),
        CODE("code"),
        NAME("name"),
        BASE_URL("baseUrl"),//wsdl bank url
        PAYMENT_URL("payUrl"), // for payment soap url
        GATEWAY_URL("gatewayUrl"), // for insert cart number and etc
        ISACTIVE("isactive"),

        //zarinpal
        MERCHANT("merchant"),
        GATEWAY_URL_POSTFIX("gatewayUrlPostfix"),

        //mellat
        TERMINALID("terminalId"),
        USERNAME("userName"),
        PASSWORD("userPassword"),
        CALLBACKPOTFIX("callBackPostfix"),;


        private String value;

        Fields(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }

    }


}

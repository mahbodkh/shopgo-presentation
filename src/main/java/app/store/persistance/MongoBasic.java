package app.store.persistance;

import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.model.FindOneAndUpdateOptions;
import com.mongodb.client.result.DeleteResult;
import com.mongodb.client.result.UpdateResult;
import org.bson.Document;
import org.bson.conversions.Bson;

import java.util.ArrayList;
import java.util.List;

public class MongoBasic {

    private String collectionName;
    private MongoCollection mongoCollection;
    private MongoDB mongo;

    protected MongoBasic(MongoDB mongo, String collectionName) {
        this.collectionName = collectionName;
        this.mongo = mongo;
    }

    public String getCollectionName() {
        return this.collectionName;
    }

    public MongoDB getMongo() {
        return this.mongo;
    }

    public MongoCollection<Document> getMongoCollection() {
        if (this.mongoCollection == null) {
            this.mongoCollection = this.mongo.getCollection(this.collectionName);
        }
        return this.mongoCollection;
    }

    public void createIndex(Document document) {
        this.getMongoCollection().createIndex(document);
    }

    public void insertOne(Document document) {
        this.getMongoCollection().insertOne(document);
    }

    public void insertMany(List<Document> documentList) {
        getMongoCollection().insertMany(documentList);
    }


    public DeleteResult deleteOne(Document document) {
        return this.getMongoCollection().deleteOne(document);
    }

    public DeleteResult deleteOne(Bson bson) {
        return this.getMongoCollection().deleteOne(bson);
    }

    public UpdateResult updateOne(Bson olddocument, Bson newdocument) {
        return this.getMongoCollection().updateOne(olddocument, newdocument);
    }

    public DeleteResult remove(Bson bson) {
        return this.getMongoCollection().deleteMany(bson);
    }

    public void createCollection(String collectionNameToCreate) {
        this.mongo.getMongoDatabase().createCollection(collectionNameToCreate);
    }

    public void dropCollection(String collectionNameToDrop) {
        this.mongo.getMongoDatabase().getCollection(collectionNameToDrop).drop();
    }

    public List<Document> find(Bson find, Bson projection, Integer skip, Integer limit, Bson sort) {
        FindIterable<Document> list = this.getMongoCollection().find(find);
        if (projection != null) {
            list = list.projection(projection);
        }

        if (sort != null) {
            list = list.sort(sort);
        }

        if (skip != null) {
            list = list.skip(skip);
        }

        if (limit != null) {
            list = list.limit(limit);
        }

        return (List) list.into(new ArrayList());
    }

    public List<Document> find(Document find, Document projection, Integer skip, Integer limit, Document sort) {
        FindIterable<Document> list = this.getMongoCollection().find(find);
        if (projection != null) {
            list = list.projection(projection);
        }

        if (sort != null) {
            list = list.sort(sort);
        }

        if (skip != null) {
            list = list.skip(skip);
        }

        if (limit != null) {
            list = list.limit(limit);
        }

        return (List) list.into(new ArrayList());
    }


    public List<Document> getList(Bson find) {
        return this.getList(find, (Bson) null, (Integer) null, (Integer) null, (Bson) null);
    }

    public List<Document> getList(Bson find, Integer limit) {
        return this.getList(find, (Bson) null, (Integer) null, limit, (Bson) null);
    }

    public List<Document> getList(Bson find, Bson projection, Integer skip, Integer limit, Bson sort) {

        FindIterable<Document> list = this.getMongoCollection().find(find);
        if (projection != null) {
            list = list.projection(projection);
        }

        if (sort != null) {
            list = list.sort(sort);
        }

        if (skip != null) {
            list = list.skip(skip);
        }

        if (limit != null) {
            list = list.limit(limit);
        }

        return (List) list.into(new ArrayList());
    }

    public Document findOneAndReplace(Bson finddocument, Document newdocument) {
        return this.getMongoCollection().findOneAndReplace(finddocument, newdocument);
    }

    public Document findOneAndUpdate(Bson finddocument, Bson newdocument, FindOneAndUpdateOptions options) {
        return this.getMongoCollection().findOneAndUpdate(finddocument, newdocument, options);
    }


}

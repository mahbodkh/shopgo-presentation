package app.store.persistance.domain;

import org.springframework.web.multipart.MultipartFile;

import java.util.List;

public class Product {

    private String id;                      //
    private String category;                // category objectId
    private String subCategory;             // subcategory objectId
    private String name;                    // name of the product
    private String description;             //
    private Integer stack;                  // the number of qty product
    private String price;                   //
    private String weight;                  // the weight of product
    private String width;                   // the width of product
    private String height;                  // the height of product
    private Boolean exist;                  // the product isThere in store
    private Integer count;                  // count of product
    private Long insertTime;                // the insertTime time (automatic , timestamp)
    private Long lastChangeTime;            // lastChangeTime update time the product by timestamp
    private String cover;                   // main image url
    private List<String> urls;              // Other images url
    private MultipartFile imageOne;         // upload fileOne
    private List<Category> categories;      //
    private List<String> keywords;          // tag or keyword for better search


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getSubCategory() {
        return subCategory;
    }

    public void setSubCategory(String subCategory) {
        this.subCategory = subCategory;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getStack() {
        return stack;
    }

    public void setStack(Integer stack) {
        this.stack = stack;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public String getWidth() {
        return width;
    }

    public void setWidth(String width) {
        this.width = width;
    }

    public String getHeight() {
        return height;
    }

    public void setHeight(String height) {
        this.height = height;
    }

    public Boolean getExist() {
        return exist;
    }

    public void setExist(Boolean exist) {
        this.exist = exist;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public Long getInsertTime() {
        return insertTime;
    }

    public void setInsertTime(Long insertTime) {
        this.insertTime = insertTime;
    }

    public Long getLastChangeTime() {
        return lastChangeTime;
    }

    public void setLastChangeTime(Long lastChangeTime) {
        this.lastChangeTime = lastChangeTime;
    }

    public String getCover() {
        return cover;
    }

    public void setCover(String cover) {
        this.cover = cover;
    }

    public List<String> getUrls() {
        return urls;
    }

    public void setUrls(List<String> urls) {
        this.urls = urls;
    }

    public MultipartFile getImageOne() {
        return imageOne;
    }

    public void setImageOne(MultipartFile imageOne) {
        this.imageOne = imageOne;
    }

    public List<Category> getCategories() {
        return categories;
    }

    public void setCategories(List<Category> categories) {
        this.categories = categories;
    }

    public List<String> getKeywords() {
        return keywords;
    }

    public void setKeywords(List<String> keywords) {
        this.keywords = keywords;
    }

    @Override
    public String toString() {
        return "Product{" +
                "id='" + id + '\'' +
                ", category='" + category + '\'' +
                ", subCategory='" + subCategory + '\'' +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", stack=" + stack +
                ", price='" + price + '\'' +
                ", weight='" + weight + '\'' +
                ", width='" + width + '\'' +
                ", height='" + height + '\'' +
                ", exist=" + exist +
                ", count=" + count +
                ", insertTime=" + insertTime +
                ", lastChangeTime=" + lastChangeTime +
                ", cover='" + cover + '\'' +
                ", urls=" + urls +
                ", imageOne=" + imageOne +
                ", categories=" + categories +
                ", keywords=" + keywords +
                '}';
    }
}

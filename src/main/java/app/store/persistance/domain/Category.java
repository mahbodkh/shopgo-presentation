package app.store.persistance.domain;

import org.springframework.web.multipart.MultipartFile;

public class Category {

    private String id;                  //
    private Integer code;               //
    private String Name;                // name of category
    private Long memberCount;           // count the product of members
    private String description;         //
    private String url;                 // the image address of icon*
    private MultipartFile icon;         // upload image file


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public Long getMemberCount() {
        return memberCount;
    }

    public void setMemberCount(Long memberCount) {
        this.memberCount = memberCount;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public MultipartFile getIcon() {
        return icon;
    }

    public void setIcon(MultipartFile icon) {
        this.icon = icon;
    }

    @Override
    public String toString() {
        return "Category{" +
                "id='" + id + '\'' +
                ", code=" + code +
                ", Name='" + Name + '\'' +
                ", memberCount=" + memberCount +
                ", description='" + description + '\'' +
                ", url='" + url + '\'' +
                ", icon=" + icon +
                '}';
    }
}

package app.store.persistance.domain;

import java.util.List;
import java.util.Map;

public class Cart {

    private String id;
    private String userId;                                  // the userId
    private Long startOrderTime;                            // time the costumer order any product
    private List<String> products;
    private List<Map<String, String>> outputProducts;       // only for return and show for user
    private String calculatePrice;
    private Integer totalShopCard;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public Long getStartOrderTime() {
        return startOrderTime;
    }

    public void setStartOrderTime(Long startOrderTime) {
        this.startOrderTime = startOrderTime;
    }

    public List<String> getProducts() {
        return products;
    }

    public void setProducts(List<String> products) {
        this.products = products;
    }

    public List<Map<String, String>> getOutputProducts() {
        return outputProducts;
    }

    public void setOutputProducts(List<Map<String, String>> outputProducts) {
        this.outputProducts = outputProducts;
    }

    public String getCalculatePrice() {
        return calculatePrice;
    }

    public void setCalculatePrice(String calculatePrice) {
        this.calculatePrice = calculatePrice;
    }

    public Integer getTotalShopCard() {
        return totalShopCard;
    }

    public void setTotalShopCard(Integer totalShopCard) {
        this.totalShopCard = totalShopCard;
    }

    @Override
    public String toString() {
        return "Cart{" +
                "id='" + id + '\'' +
                ", userId='" + userId + '\'' +
                ", startOrderTime=" + startOrderTime +
                ", products=" + products +
                ", outputProducts=" + outputProducts +
                ", calculatePrice='" + calculatePrice + '\'' +
                ", totalShopCard=" + totalShopCard +
                '}';
    }
}

package app.store.persistance.domain;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.validation.Errors;

public class PojoModel extends BasicModel {
    private final Logger log = LoggerFactory.getLogger(PojoModel.class);

    private int time;

    public int getTime() {
        return time;
    }

    public void setTime(int time) {
        this.time = time;
    }

    @Override
    public void validate(Errors errors) {
        log.debug("this is validation in model {}");

        System.out.println("this is validation in model ... ");
        return;
    }
}

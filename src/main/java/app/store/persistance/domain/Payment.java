package app.store.persistance.domain;

public class Payment {

    private String id;
    private String amount;
    private String paymentTime;
    private String callBack;
    private String bankId;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getPaymentTime() {
        return paymentTime;
    }

    public void setPaymentTime(String paymentTime) {
        this.paymentTime = paymentTime;
    }

    public String getCallBack() {
        return callBack;
    }

    public void setCallBack(String callBack) {
        this.callBack = callBack;
    }

    public String getBankId() {
        return bankId;
    }

    public void setBankId(String bankId) {
        this.bankId = bankId;
    }

    @Override
    public String toString() {
        return "Payment{" +
                "id='" + id + '\'' +
                ", amount='" + amount + '\'' +
                ", paymentTime='" + paymentTime + '\'' +
                ", callBack='" + callBack + '\'' +
                ", bankId='" + bankId + '\'' +
                '}';
    }
}

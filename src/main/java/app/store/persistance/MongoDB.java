package app.store.persistance;


import app.store.util.ConfigReader;
import com.mongodb.MongoClient;
import com.mongodb.MongoException;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static java.lang.String.format;

public class MongoDB {

    private final Logger log = LoggerFactory.getLogger(MongoDB.class);
    private static MongoDB instance = null;
    private MongoClient mongoClient;
    private MongoDatabase database;

//    static {
//        try {
//            instance = new MongoDB();
//        } catch (Exception e) {
//            throw new RuntimeException("Exception occured in creating singleton instance");
//        }
//    }


    private MongoDB() {
        init();
    }

    public static MongoDB getInstance() {
        if (instance == null) {
            synchronized (MongoDB.class) {
                if (instance == null) {
                    instance = new MongoDB();
                }
            }
        }
        return instance;
    }

    private void init() {
        mongoClient = null;
        try {
            mongoClient = new MongoClient
                    (
                            ConfigReader.getString("mongodb.host")
                            , Integer.parseInt(ConfigReader.getString("mongodb.port"))
                    );
            log.debug("mongo check : {} ", mongoClient);


        } catch (MongoException e) {
            System.out.println(" Mongo Exception");
            e.printStackTrace();
        }

        log.debug("Connected to MongoDB!");
        database = null;
        database = mongoClient.getDatabase(ConfigReader.getString("mongodb.database"));
        if (database == null) {
            //todo
        }
        log.debug("database and its not null");
    }

    public MongoCollection getCollection(String collectionName) {
        return mongoClient.getDatabase(database.getName()).getCollection(collectionName);
    }

    public MongoDatabase getMongoDatabase() {
        return this.database;
    }


    public void close() {
        if (mongoClient != null) {
            try {
                mongoClient.close();
                log.debug("Nulling the connection dependency objects");
                mongoClient = null;
                database = null;
            } catch (Exception e) {
                log.error(format("An error occurred when closing the MongoDB connection\n %s", e.getMessage()));
            }
        } else {
            log.warn("mongo object was null, wouldn't close connection");
        }
    }


}

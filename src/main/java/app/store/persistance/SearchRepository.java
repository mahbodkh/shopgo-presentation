package app.store.persistance;


import org.bson.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.*;

@Component
public class SearchRepository {


    private ProductRepository productRepository;
    private CategoryRepository categoryRepository;

    @Autowired
    public SearchRepository(ProductRepository productRepository, CategoryRepository categoryRepository) {
        this.productRepository = productRepository;
        this.categoryRepository = categoryRepository;
    }

    public List<List<Document>> doSearch(String keyword, Integer limit) {
        ExecutorService executor = Executors.newFixedThreadPool(2);

        List<Document> documentProductText = null;
        Callable<List<Document>> taskProduct = () -> {
            return productRepository.doSearchText(keyword, limit);
        };

        List<Document> documentCategoryText = null;
        Callable<List<Document>> taskCategory = () -> {
            return categoryRepository.doSearchText(keyword, limit);
        };


        try {
            documentProductText = taskProduct.call();
            documentCategoryText = taskCategory.call();
        } catch (Exception e) {
            e.printStackTrace();
        }

//
//        List<Document> productRegex = null;
//        Future<List<Document>> futureRegexProduct = executor.submit(() -> {
//            return productRepository.doSearchRegex(keyword, limit);
//        });
//        List<Document> categoryRegex = null;
//        Future<List<Document>> futureRegexCategory = executor.submit(() -> {
//            return categoryRepository.doSearchRegex(keyword, limit);
//        });
//
//        try {
//            productRegex = futureRegexProduct.get();
//            categoryRegex = futureRegexCategory.get();
//        } catch (InterruptedException | ExecutionException e) {
//            e.printStackTrace();
//        }

        return Arrays.asList(documentProductText, documentCategoryText/*, productRegex, categoryRegex*/);
    }

    private ExecutorService executor = Executors.newFixedThreadPool(2);

    public List<Document> doSearch(String keyword) {

        List<Document> documentProductText = null;
        Callable<List<Document>> taskProduct = () -> {
            return productRepository.doSearchText(keyword);
        };

        List<Document> documentCategoryText = null;
        Callable<List<Document>> taskCategory = () -> {
            return categoryRepository.doSearchText(keyword);
        };


        try {
            documentProductText = taskProduct.call();
            documentCategoryText = taskCategory.call();
        } catch (Exception e) {
            e.printStackTrace();
        }

        List<Document> productRegex = null;
        if (documentProductText.size() >= 5) {
            Future<List<Document>> futureRegexProduct = executor.submit(() -> {
                return productRepository.doSearchRegex(keyword);
            });


//        List<Document> categoryRegex = null;
//        Future<List<Document>> futureRegexCategory = executor.submit(() -> {
//            return categoryRepository.doSearchRegex(keyword, limit);
//        });
//
            try {
                productRegex = futureRegexProduct.get();
//            categoryRegex = futureRegexCategory.get();
            } catch (InterruptedException | ExecutionException e) {
                e.printStackTrace();
            }
        }


        Document result = new Document();
        result.put("productText", documentProductText);
        result.put("categoryText", documentCategoryText);
        result.put("productRegex", productRegex);
//        result.put("categoryRegex", categoryRegex);

        return Arrays.asList(result);
    }

}

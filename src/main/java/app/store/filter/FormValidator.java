package app.store.filter;

import app.store.persistance.domain.BasicModel;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

public class FormValidator implements Validator {

    @Override
    public boolean supports(Class<?> aClass) {
        return true;
    }

    @Override
    public void validate(Object o, Errors errors) {
//        if (o instanceof BasicModel) {
            BasicModel basic = (BasicModel) o;
            basic.validate(errors);
//        }
    }
}

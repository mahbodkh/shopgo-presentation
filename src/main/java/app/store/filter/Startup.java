package app.store.filter;

import app.store.persistance.CategoryRepository;
import org.bson.Document;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

@Component
public class Startup implements ApplicationListener<ContextRefreshedEvent> {

    private CategoryRepository categoryRepository;

    @Autowired
    public Startup(CategoryRepository categoryRepository) {
        this.categoryRepository = categoryRepository;
    }

    @Override
    public void onApplicationEvent(ContextRefreshedEvent event) {
        System.out.println("This is startup class");


//        Document category01 = new Document();
//        category01.put(CategoryRepository.Fields.ID.getValue(), new ObjectId());
//        category01.put(CategoryRepository.Fields.NAME.getValue(), "دوربین های حرفه ایی");
//        category01.put(CategoryRepository.Fields.CODE.getValue(), 101);
//        category01.put(CategoryRepository.Fields.DESCRIPTION.getValue(), "این قسمت دوربین هایی است که حرفه ایی می باشند.");
//        category01.put(CategoryRepository.Fields.URL.getValue(), "http://www.google.com/cdn/images/");
//
//        Document category02 = new Document();
//        category02.put(CategoryRepository.Fields.ID.getValue(), new ObjectId());
//        category02.put(CategoryRepository.Fields.NAME.getValue(), "دوربین های نیمه حرفه ایی");
//        category02.put(CategoryRepository.Fields.CODE.getValue(), 102);
//        category02.put(CategoryRepository.Fields.DESCRIPTION.getValue(), "این قسمت دوربین هایی است که نیمه حرفه ایی می باشند.");
//        category02.put(CategoryRepository.Fields.URL.getValue(), "http://www.google.com/cdn/images/");
//
//
//        Document category03 = new Document();
//        category03.put(CategoryRepository.Fields.ID.getValue(), new ObjectId());
//        category03.put(CategoryRepository.Fields.NAME.getValue(), "دوربین های خانگی");
//        category03.put(CategoryRepository.Fields.CODE.getValue(), 103);
//        category03.put(CategoryRepository.Fields.DESCRIPTION.getValue(), "این قسمت دوربین هایی است که غیر حرفه ایی و خانگی می باشند.");
//        category03.put(CategoryRepository.Fields.URL.getValue(), "http://www.google.com/cdn/images/");

//        categoryRepository.insert(category01);
//        categoryRepository.insert(category02);
//        categoryRepository.insert(category03);



    }
}

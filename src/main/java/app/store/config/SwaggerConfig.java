package app.store.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
class SwaggerConfig {

    @Bean
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
//                .apis(RequestHandlerSelectors.basePackage("com.kian.dorsa.market.web.com.kian.dorsa.market.api.rest"))
                .apis(RequestHandlerSelectors.any())
//                .paths(PathSelectors.any())
//                .paths(PathSelectors.regex("/api/.*"))
                .paths(PathSelectors.regex("/.*"))
                .build();
    }

}


//package app.store.payment;
//
//import com.vasl.vaslapp.common.global.general.General;
//import com.vasl.vaslapp.modules.bankgateway.global.enums.Bank;
//import com.vasl.vaslapp.modules.bankgateway.global.enums.TransactionStatus;
//import com.vasl.vaslapp.modules.bankgateway.global.model.PaymentModel;
//import com.vasl.vaslapp.modules.bankgateway.global.mongo.repositories.acm.BankRepository;
//import com.vasl.vaslapp.modules.bankgateway.global.mongo.repositories.acm.PaymentRepository;
//import com.vasl.vaslapp.modules.bankgateway.global.service.mellat.IPaymentGateway;
//import com.vasl.vaslapp.modules.bankgateway.global.service.mellat.PaymentGatewayImplServiceLocator;
//import com.vasl.vaslapp.modules.bankgateway.global.service.zarinpal.PaymentGatewayImplementationServiceLocator;
//import com.vasl.vaslapp.modules.bankgateway.global.service.zarinpal.PaymentGatewayImplementationServicePortType;
//import org.bson.Document;
//
//import javax.xml.rpc.ServiceException;
//import javax.xml.rpc.holders.IntHolder;
//import javax.xml.rpc.holders.LongHolder;
//import javax.xml.rpc.holders.StringHolder;
//import java.rmi.RemoteException;
//import java.util.Map;
//import java.util.concurrent.ConcurrentHashMap;
//
//public class GateWayImpl implements GateWay {
//    private long now = System.currentTimeMillis();
//
//
//    public Map<String, String> pay(Document bank, PaymentModel paymentModel) {
//
//        Integer bankCode = paymentModel.getBankCode();
//        if (bankCode == Bank.ZARINPAL.getCode()) {
//            return payByZarinpal(bank, paymentModel);
//        } else if (bankCode == Bank.MELLAT.getCode()) {
//            return payByMellat(bank, paymentModel);
//        } else if (bankCode == Bank.TEJARAT.getCode()) {
//            return payByTejarat(bank, paymentModel);
//        } else if (bankCode == Bank.EQTESDNOVIN.getCode()) {
//            return payByEqtesadNovin(bank, paymentModel);
//        }
//        return null;
//    }
//
//
//    /**
//     * case -1:				// Invalid Request
//     * case -2:				// Invalid IP or merchantID
//     * case -3:				// Amount should be higher than 100 $TOMAN
//     * case -4:				// Service unavailable
//     * case -11:		    // Resource not found
//     * case -21:			// Payment not found
//     * case -22:			// Result was failure
//     * case -33:			// Amount was not matched
//     * case -54:			// Result was archived
//     * case 100:			// Result was successful (first time)
//     * case 101:			// Result was successful
//     */
//    private Map<String, String> payByZarinpal(Document bank, PaymentModel paymentModel) {
//        Map<String, String> result = new ConcurrentHashMap<>();
//
//        IntHolder statusHolder = new IntHolder();
//        StringHolder authorityHolder = new StringHolder();
//        String callbackUrl = bank.getString(BankRepository.Fields.URL.getValue())
//                + paymentModel.getAppid()
//                + "/"
//                + paymentModel.getPhoneNumber()
//                + "/";
//
//        try {
//            PaymentGatewayImplementationServiceLocator payServiceLocator = new PaymentGatewayImplementationServiceLocator();
//            PaymentGatewayImplementationServicePortType payServices = payServiceLocator.getPaymentGatewayImplementationServicePort();
//            payServices.paymentRequest
//                    (
//                            General.parseString(bank.getString(BankRepository.Fields.MERCHANT.getValue()))
//                            , General.parseInt(paymentModel.getAmount())
//                            , General.parseString(bank.getString(BankRepository.Fields.DESCRIPTION.getValue()))
//                            , General.parseString(paymentModel.getEmail())
//                            , General.parseString(paymentModel.getPhoneNumber())
//                            , General.parseString(callbackUrl)
//                            , statusHolder
//                            , authorityHolder
//                    );
//
//        } catch (Exception ex) {
//            ex.printStackTrace();
//            System.out.println("Cannot Add Address ");
//            result.put(PaymentRepository.Fields.ERROR.getValue(), "Exception ... ");
//            return result;
//        }
//
//        result.put(PaymentRepository.Fields.STATUSHOLDER.getValue(), String.valueOf(statusHolder.value));
//        result.put(PaymentRepository.Fields.AUTHORITY.getValue(), authorityHolder.value);
//        result.put(PaymentRepository.Fields.TRANSACTIONSTATUS.getValue(), TransactionStatus.NOT_CHECKED.getValue());
//        result.put(PaymentRepository.Fields.GATEWAYURL.getValue(),
//                General.parseString(bank.getString(BankRepository.Fields.URL.getValue()))
//                        + "/"
//                        + authorityHolder.value
//                        + "/"
//                        + General.parseString(bank.getString(BankRepository.Fields.MOBILEPOSTFIX.getValue())));
//
//        return result;
//    }
//
//    /**
//     * 12
//     * 13
//     * 14
//     * 15
//     * 16
//     * 17
//     * 18
//     * 19
//     * 111
//     * 112
//     * 113
//     * 114
//     * 21
//     * 23
//     * 24
//     * 25
//     * 31
//     * 32
//     * 33
//     * 34
//     * 35
//     * 41
//     * 42
//     * 43
//     * 44
//     * 45
//     * 46
//     * 47
//     * 48
//     * 49
//     * 412
//     * 413
//     *///todo mrs rezaei add exception code
//    private Map<String, String> payByMellat(Document bank, PaymentModel paymentModel) {
//        Map<String, String> result = new ConcurrentHashMap<>();
//        long orderId = 0;
//        long amount = paymentModel.getAmount();
//        String localTime = null;
//        String localDate = null;
//        String additionalData = null;
//        String callbackurl = bank.getString(BankRepository.Fields.URL.getValue()) + "/" + paymentModel.getAppid() + "/";
//        long payerId = 0;   //todo uuid or objectId
//
//
//        try {
//            PaymentGatewayImplServiceLocator payServiceLocator = new PaymentGatewayImplServiceLocator();
//            IPaymentGateway payService = payServiceLocator.getPaymentGatewayImplPort();
//            payService.bpPayRequest
//                    (
//                            bank.getLong(BankRepository.Fields.TERMINALID.getValue())
//                            , bank.getString(BankRepository.Fields.USERNAME.getValue())
//                            , bank.getString(BankRepository.Fields.PASSWORD.getValue())
//                            , orderId
//                            , paymentModel.getAmount()
//                            , localDate
//                            , localTime
//                            , additionalData
//                            , callbackurl
//                            , payerId
//                    );
//
//        } catch (ServiceException | RemoteException e) {
//            e.printStackTrace();
//        }
//
//
//        return result;
//    }
//
//    private Map<String, String> payByTejarat(Document bank, PaymentModel paymentModel) {
//        return null;
//    }
//
//    private Map<String, String> payByEqtesadNovin(Document bank, PaymentModel paymentModel) {
//        return null;
//    }
//
//
//    // ------------------------------->
//    //                          verify payment functions
//    // ------------------------------->
//
//    @Override
//    public Map<String, String> payVerifying(Document config, Document transaction) {
//        Map<String, String> result = new ConcurrentHashMap<>();
//        LongHolder refIdHolder = new LongHolder();
//        IntHolder statusHolder = new IntHolder();
//
//        Integer amount = transaction.getInteger(PaymentRepository.Fields.AMOUNT.getValue());
//        String authority = transaction.getString(PaymentRepository.Fields.AUTHORITY.getValue());
//        try {
//            PaymentGatewayImplementationServiceLocator payServiceLocator = new PaymentGatewayImplementationServiceLocator();
//            PaymentGatewayImplementationServicePortType payServices = payServiceLocator.getPaymentGatewayImplementationServicePort();
//            payServices.paymentVerification
//                    (
//                            General.parseString(config.getString(BankRepository.Fields.MERCHANT.getValue()))
//                            , authority
//                            , amount
//                            , statusHolder
//                            , refIdHolder
//                    );
//
//            result.put(PaymentRepository.Fields.STATUSHOLDER.getValue(), String.valueOf(statusHolder.value));
//            if (refIdHolder != null)
//                result.put(PaymentRepository.Fields.REFIDHOLDER.getValue(), String.valueOf(refIdHolder));
//            else
//                result.put(PaymentRepository.Fields.REFIDHOLDER.getValue(), "");
//
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//
//        return result;
//    }
//
//    private Map<String, String> verifyByZarinpal(Document bank, PaymentModel paymentModel) {
//        return null;
//    }
//
//}

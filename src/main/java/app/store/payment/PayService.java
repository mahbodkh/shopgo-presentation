//package app.store.payment;
//
//import com.vasl.vaslapp.modules.bankgateway.global.model.*;
//
//public interface PayService {
//
//    GateWay.Callback.Builder getCallBank(CallbackModel callbackModel);
//
//    GateWay.BankList.Builder listBank(BankListModel bankListModel);
//
//    GateWay.BankGet.Builder getBank(BankGetModel bankGetModel);
//
//    GateWay.Bank.Builder addBank(BankAddModel bankAddModel);
//
//    GateWay.BankGet.Builder editBank(BankUpdateModel bankUpdateModel);
//
//    GateWay.Pay.Builder payment(PaymentModel paymentModel);
//
//    GateWay.Pay.Builder verifyPayment(PaymentModel paymentModel);
//
////    GateWay.Operator.Builder addOperatop(OperatorModel operatorModel);
//}

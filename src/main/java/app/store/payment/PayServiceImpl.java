//package app.store.payment;
//
//
//import com.mongodb.client.result.UpdateResult;
//import com.vasl.vaslapp.common.global.enums.Return_Status_Codes_Common;
//import com.vasl.vaslapp.common.global.interfaces.IReturn_Status_Codes;
//import com.vasl.vaslapp.modules.bankgateway.global.enums.Bank;
//import com.vasl.vaslapp.modules.bankgateway.global.enums.Return_Status_Codes_Payment;
//import com.vasl.vaslapp.modules.bankgateway.global.enums.TransactionStatus;
//import com.vasl.vaslapp.modules.bankgateway.global.general.General;
//import com.vasl.vaslapp.modules.bankgateway.global.model.*;
//import com.vasl.vaslapp.modules.bankgateway.global.mongo.repositories.acm.BankRepository;
//import com.vasl.vaslapp.modules.bankgateway.global.mongo.repositories.acm.PaymentRepository;
//import org.bson.Document;
//import org.bson.types.ObjectId;
//import org.springframework.stereotype.Component;
//
//import java.util.List;
//import java.util.Map;
//
//@Component
//public class PayServiceImpl implements PayService {
//    private long now = System.currentTimeMillis();
//
//    @Override
//    public GateWay.Callback.Builder getCallBank(CallbackModel callbackModel) {
//
//        GateWay.Callback.Builder ret = GateWay.Callback.newBuilder();
//
////        PanelSubscriberV1.BankCallback.Builder ret = PanelSubscriberV1.BankCallback.newBuilder();
////        PazhChargeTransactions pazhChargeTransactions = new PazhChargeTransactions(zarinPalCallbackModel.getOutput().getMongodb_app(zarinPalCallbackModel.getAppid()));
//
//        IReturn_Status_Codes return_status_codes = callbackModel.getReturn_status_codes();
//        if (return_status_codes.getCode() != Return_Status_Codes_Common.ok_validform.getCode()) {
//            return callbackModel.getOutput().returnResponseObject(ret, return_status_codes);
//        }
//
//
////        if (!TransactionStatus.NOT_CHECKED.getValue().equals(zarinPalCallbackModel.getTransactionObject().getString(PazhChargeTransactions.Fields.STATUS.getValue()))) {
////            return_status_codes = Return_Status_Codes_Payment.PAYMENT_SUBSCRIBER_TRANSACTION_CHECKED_BEFORE;
////            Document upDoc = new Document();
////            upDoc.put("refId", zarinPalCallbackModel.getRefIdResp());
////            upDoc.put("respStatus", zarinPalCallbackModel.getStatusResp());
////            upDoc.put(PazhChargeTransactions.Fields.STATUS.getValue(), TransactionStatus.PROCESSED_ERROR.getValue());
////
////            pazhChargeTransactions.update(zarinPalCallbackModel.getTransactionObject().getString(PazhChargeTransactions.Fields.AUTHORITY.getValue()),
////                    zarinPalCallbackModel.getTransactionObject().getLong(PazhChargeTransactions.Fields.INSERTTIME.getValue()),
////                    TransactionStatus.NOT_CHECKED.getValue(), upDoc);
////
////            return zarinPalCallbackModel.getOutput().returnResponseObject(ret, return_status_codes);
////        }
//
////        Api24Operators api24Operators = Api24Operators.get(zarinPalCallbackModel.getTransactionObject().getString(PazhChargeTransactions.Fields.OP.getValue()));
////        Api24ReturnTopup api24ReturnTopup = Api24.Topup(
////                api24Operators,
////                new PhoneNumber(zarinPalCallbackModel.getTransactionObject().getString(PazhChargeTransactions.Fields.PHONENUMBER.getValue())),
////                Api24TopupProfile.NORMAL,
////                zarinPalCallbackModel.getTransactionObject().getInteger(PazhChargeTransactions.Fields.AMOUNT.getValue()) * 10,
////                "Pazh");
//
//
////        if (api24ReturnTopup == null || api24ReturnTopup.getStatusCode() != 0) {
////            return_status_codes = Return_Status_Codes.PAZH_ERROR_INVALID_PARAMETER;
////            return zarinPalCallbackModel.getOutput().returnResponseObject(ret, return_status_codes);
////        }
////
//
//
//        System.out.println("Successfully Charge phoneNumber|" + callbackModel.getPhoneNumber() + "|");
////
////        Document upDoc = new Document();
////        upDoc.put("refId", zarinPalCallbackModel.getRefIdResp());
////        upDoc.put("respStatus", zarinPalCallbackModel.getStatusResp());
////        upDoc.put(PazhChargeTransactions.Fields.STATUS.getValue(), TransactionStatus.PROCESSED_DONE.getValue());
////        upDoc.put("InvoiceId", api24ReturnTopup.getInvoiceID());
//
//
////        pazhChargeTransactions.update(zarinPalCallbackModel.getTransactionObject().getString(PazhChargeTransactions.Fields.AUTHORITY.getValue()),
////                zarinPalCallbackModel.getTransactionObject().getLong(PazhChargeTransactions.Fields.INSERTTIME.getValue()),
////                TransactionStatus.NOT_CHECKED.getValue(), upDoc);
////
//        ret = callbackModel.getOutput().returnResponseObject(ret, Return_Status_Codes_Common.SC_OK);
//        return ret;
//    }
//
//    @Override
//    public GateWay.BankList.Builder listBank(BankListModel bankListModel) {
//        GateWay.BankList.Builder ret = GateWay.BankList.newBuilder();
//        return ret;
//    }
//
//    @Override
//    public GateWay.Bank.Builder addBank(BankAddModel bankAddModel) {
//        GateWay.Bank.Builder ret = GateWay.Bank.newBuilder();
//
//        IReturn_Status_Codes return_status_codes = bankAddModel.getReturn_status_codes();
//        if (return_status_codes.getCode() != Return_Status_Codes_Common.ok_validform.getCode()) {
//            return bankAddModel.getOutput().returnResponseObject(ret, return_status_codes);
//        }
//
//        BankRepository pay = new BankRepository(bankAddModel.getOutput().getMongodb_app());
//        Document document = new Document();
//        document.put(BankRepository.Fields.CODE.getValue(), General.parseInt(bankAddModel.getCode()));
//        document.put(BankRepository.Fields.NAME.getValue(), General.parseString(bankAddModel.getName()));
//        document.put(BankRepository.Fields.DESCRIPTION.getValue(), General.parseString(bankAddModel.getDescription()));
//        document.put(BankRepository.Fields.URL.getValue(), General.parseString(bankAddModel.getUrl()));
//        document.put(BankRepository.Fields.MOBILEPOSTFIX.getValue(), General.parseString(bankAddModel.getMobilePostfix()));
//        document.put(BankRepository.Fields.MERCHANT.getValue(), General.parseString(bankAddModel.getMerchant()));
//        pay.insert(document);
//
//
//        ret = bankAddModel.getOutput().returnResponseObject(ret, Return_Status_Codes_Common.SC_OK);
//        return ret;
//    }
//
//    @Override
//    public GateWay.BankGet.Builder getBank(BankGetModel bankGetModel) {
//        GateWay.BankGet.Builder ret = GateWay.BankGet.newBuilder();
//
//        return ret;
//    }
//
//    @Override
//    public GateWay.BankGet.Builder editBank(BankUpdateModel bankUpdateModel) {
//        GateWay.BankGet.Builder ret = GateWay.BankGet.newBuilder();
//
//        return ret;
//    }
//
//
//    @Override
//    public GateWay.Pay.Builder payment(PaymentModel paymentModel) {
//        PaymentRepository transaction = new PaymentRepository(paymentModel.getOutput().getMongodb_app());
//        GateWay.Pay.Builder ret = GateWay.Pay.newBuilder();
//
//
//        IReturn_Status_Codes return_status_codes = paymentModel.getReturn_status_codes();
//        if (return_status_codes.getCode() != Return_Status_Codes_Common.ok_validform.getCode()) {
//            return paymentModel.getOutput().returnResponseObject(ret, return_status_codes);
//        }
//
//        GateWayImpl gateWay = new GateWayImpl();
//        Document bank = paymentModel.getBankDocument();
//        Map<String, String> getPay = gateWay.pay(bank, paymentModel);
//
//
//        ret = paymentModel.getOutput().returnResponseObject(ret, Return_Status_Codes_Common.SC_OK);
//
//        //        ret.setPaymentCode(null);
//        if (paymentModel.getAmount() != null)
//            ret.setAmount(General.parseInt(paymentModel.getAmount()));
//
//        String gatewayurl = null;
//        String amount = null;
//        Integer bankCode = paymentModel.getBankCode();
//        if (bankCode == Bank.ZARINPAL.getCode()) {
//
//            String statusHolder = null;
//            String authority = null;
//            String transactionsOrdersStatus = null;
//            String error = null;
//
//            if (getPay != null) {
//                statusHolder = getPay.get(PaymentRepository.Fields.STATUSHOLDER.getValue());
//                amount = getPay.get(PaymentRepository.Fields.AMOUNT.getValue());
//                authority = getPay.get(PaymentRepository.Fields.AUTHORITY.getValue());
//                gatewayurl = getPay.get(PaymentRepository.Fields.GATEWAYURL.getValue());
//                transactionsOrdersStatus = getPay.get(PaymentRepository.Fields.TRANSACTIONSTATUS.getValue());
//                error = getPay.get(PaymentRepository.Fields.ERROR.getValue());
//            }
//
//            if (!authority.equals("100") || General.isEmpty(authority)) {
//                switch (General.parseInt(statusHolder)) {
//                    case -1:                // Invalid Request
//                        return_status_codes = Return_Status_Codes_Payment.PAYMENT_ERROR_ZARINPAL_INVALID_REQUEST_1_;
//                        return paymentModel.getOutput().returnResponseObject(ret, return_status_codes);
//                    case -2:                // Invalid IP or merchantID
//                        return_status_codes = Return_Status_Codes_Payment.PAYMENT_ERROR_ZARINPAL_INVALID_IP_MERCHANTID_2_;
//                        return paymentModel.getOutput().returnResponseObject(ret, return_status_codes);
//                    case -3:                // Amount should be higher than 100 $TOMAN
//                        return_status_codes = Return_Status_Codes_Payment.PAYMENT_ERROR_ZARINPAL_AMOUNT_UNDER_T100_3_;
//                        return paymentModel.getOutput().returnResponseObject(ret, return_status_codes);
//                    case -4:                // Service unavailable
//                        return_status_codes = Return_Status_Codes_Payment.PAYMENT_ERROR_ZARINPAL_SERVICE_UNAVAILABLE_4_;
//                        return paymentModel.getOutput().returnResponseObject(ret, return_status_codes);
//                    case -11:                // Resource not found
//                        return_status_codes = Return_Status_Codes_Payment.PAYMENT_ERROR_ZARINPAL_RESOURCE_NOT_FOUND_11_;
//                        return paymentModel.getOutput().returnResponseObject(ret, return_status_codes);
//                    case -21:                // Payment not found
//                        return_status_codes = Return_Status_Codes_Payment.PAYMENT_ERROR_ZARINPAL_PAYMENT_NOT_FOUND_21_;
//                        return paymentModel.getOutput().returnResponseObject(ret, return_status_codes);
//                    case -22:                // Result was failure
//                        return_status_codes = Return_Status_Codes_Payment.PAYMENT_ERROR_ZARINPAL_RESULT_WAS_FAILURE_22_;
//                        return paymentModel.getOutput().returnResponseObject(ret, return_status_codes);
//                    case -33:                // Amount was not matched
//                        return_status_codes = Return_Status_Codes_Payment.PAYMENT_ERROR_ZARINPAL_AMOUNT_WAS_NOT_MATCHED_33_;
//                        return paymentModel.getOutput().returnResponseObject(ret, return_status_codes);
//                    case -54:                // Result was archived
//                        return_status_codes = Return_Status_Codes_Payment.PAYMENT_ERROR_ZARINPAL_RESULT_WAS_ARCHIVED_54_;
//                        return paymentModel.getOutput().returnResponseObject(ret, return_status_codes);
//                    default:
//                        break;
//                }
//            }
//
//            Document payment = transaction.getPaymentDocument(paymentModel);
//            payment.put(PaymentRepository.Fields.BANKCODE.getValue(), bank.getInteger(BankRepository.Fields.CODE.getValue()));
//            payment.put(PaymentRepository.Fields.BANKNAME.getValue(), bank.getString(BankRepository.Fields.NAME.getValue()));
//            payment.put(PaymentRepository.Fields.STATUSHOLDER.getValue(), statusHolder);
//            payment.put(PaymentRepository.Fields.AUTHORITY.getValue(), authority);
//            payment.put(PaymentRepository.Fields.TRANSACTIONSTATUS.getValue(), transactionsOrdersStatus);
//            payment.put(PaymentRepository.Fields.PAYMENTTIME.getValue(), now);
//            payment.put(PaymentRepository.Fields.ERROR.getValue(), error);
//            transaction.insert(payment);
//
//            if (!gatewayurl.equals(""))
//                ret.setGatewayUrl(gatewayurl);
//            return ret;
//        } else if (bankCode == Bank.MELLAT.getCode()) {
//            getPay.get("error");
//
//            return ret;
//        } else if (bankCode == Bank.TEJARAT.getCode()) {
//            return ret;
//        } else
//            return null;
//    }
//
//    private Map<String, String> checkZarinpalStatusCode(Map<String, String> getPay) {
//        return null;
//    }
//
//
//    @Override
//    public GateWay.Pay.Builder verifyPayment(PaymentModel paymentModel) {
//        GateWay.Pay.Builder ret = GateWay.Pay.newBuilder();
//        PaymentRepository transaction = new PaymentRepository(paymentModel.getOutput().getMongodb_app());
//
//        IReturn_Status_Codes return_status_codes = paymentModel.getReturn_status_codes();
//        if (return_status_codes.getCode() != Return_Status_Codes_Common.ok_validform.getCode()) {
//            return paymentModel.getOutput().returnResponseObject(ret, return_status_codes);
//        }
//
//        GateWayImpl gateWay = new GateWayImpl();
//        String amount = null;
//        String gatewayurl = null;
//        String transactionStatus = null;
//        String error = null;
//
//        Document bank = paymentModel.getBankDocument();
//        Document payment = null;
//        List<Document> transactionList = null;
//        if (transactionList == null) {
//            transactionList = transaction.getById(paymentModel.getId());
//            payment = transactionList.get(0);
//        }
//
//        ObjectId objectId = null;
//        objectId = payment.getObjectId(PaymentRepository.Fields.ID.getValue());
//
//        Map<String, String> getVerify = gateWay.payVerifying(bank, payment);
//        if (getVerify == null) {
//            return_status_codes = Return_Status_Codes_Payment.PAYMENT_ERROR_PAYMENT_VERIFICATION;
//            return paymentModel.getOutput().returnResponseObject(ret, return_status_codes);
//        }
//
//        if (paymentModel.getBankCode() == Bank.ZARINPAL.getCode()) {
//
//            String statusHolder = null;
//            String authority = null;
//            String refIdHolder = null;
//            refIdHolder = getVerify.get(PaymentRepository.Fields.REFIDHOLDER.getValue());
//            statusHolder = getVerify.get(PaymentRepository.Fields.STATUSHOLDER.getValue());
//            error = getVerify.get(PaymentRepository.Fields.ERROR.getValue());
//            amount = getVerify.get(PaymentRepository.Fields.AMOUNT.getValue());
//            transactionStatus = getVerify.get(PaymentRepository.Fields.TRANSACTIONSTATUS.getValue());
//
//            switch (General.parseInt(statusHolder)) {
//                case -1:                // Invalid Request
//                    return_status_codes = Return_Status_Codes_Payment.PAYMENT_ERROR_ZARINPAL_INVALID_REQUEST_1_;
//                    return paymentModel.getOutput().returnResponseObject(ret, return_status_codes);
//                case -2:                // Invalid IP or merchantID
//                    return_status_codes = Return_Status_Codes_Payment.PAYMENT_ERROR_ZARINPAL_INVALID_IP_MERCHANTID_2_;
//                    return paymentModel.getOutput().returnResponseObject(ret, return_status_codes);
//                case -3:                // Amount should be higher than 100 $TOMAN
//                    return_status_codes = Return_Status_Codes_Payment.PAYMENT_ERROR_ZARINPAL_AMOUNT_UNDER_T100_3_;
//                    return paymentModel.getOutput().returnResponseObject(ret, return_status_codes);
//                case -4:                // Service unavailable
//                    return_status_codes = Return_Status_Codes_Payment.PAYMENT_ERROR_ZARINPAL_SERVICE_UNAVAILABLE_4_;
//                    return paymentModel.getOutput().returnResponseObject(ret, return_status_codes);
//                case -11:                // Resource not found
//                    return_status_codes = Return_Status_Codes_Payment.PAYMENT_ERROR_ZARINPAL_RESOURCE_NOT_FOUND_11_;
//                    return paymentModel.getOutput().returnResponseObject(ret, return_status_codes);
//                case -21:                // Payment not found
//                    return_status_codes = Return_Status_Codes_Payment.PAYMENT_ERROR_ZARINPAL_PAYMENT_NOT_FOUND_21_;
//                    return paymentModel.getOutput().returnResponseObject(ret, return_status_codes);
//                case -22:                // Result was failure
//                    return_status_codes = Return_Status_Codes_Payment.PAYMENT_ERROR_ZARINPAL_RESULT_WAS_FAILURE_22_;
//                    return paymentModel.getOutput().returnResponseObject(ret, return_status_codes);
//                case -33:                // Amount was not matched
//                    return_status_codes = Return_Status_Codes_Payment.PAYMENT_ERROR_ZARINPAL_AMOUNT_WAS_NOT_MATCHED_33_;
//                    return paymentModel.getOutput().returnResponseObject(ret, return_status_codes);
//                case -54:                // Result was archived
//                    return_status_codes = Return_Status_Codes_Payment.PAYMENT_ERROR_ZARINPAL_RESULT_WAS_ARCHIVED_54_;
//                    return paymentModel.getOutput().returnResponseObject(ret, return_status_codes);
//                default:
//                    break;
//            }
//
//            Document newPayment = transaction.getPaymentDocument(paymentModel);
//            newPayment.put(PaymentRepository.Fields.VERIFYTIME.getValue(), now);
//            newPayment.put(PaymentRepository.Fields.REFIDHOLDER.getValue(), refIdHolder);
//            newPayment.put(PaymentRepository.Fields.STATUSHOLDER.getValue(), statusHolder);
//            newPayment.put(PaymentRepository.Fields.ERROR.getValue(), error);
//            newPayment.put(PaymentRepository.Fields.TRANSACTIONSTATUS.getValue(), TransactionStatus.PROCESSED_DONE.getValue());
//            UpdateResult result = transaction.update(payment, newPayment);
//            if (result.wasAcknowledged()) {
//                //todo
//            }
//        } else if (paymentModel.getBankCode() == Bank.MELLAT.getCode()) {
//        } else if (paymentModel.getBankCode() == Bank.TEJARAT.getCode()) {
//        } else if (paymentModel.getBankCode() == Bank.PASSARGAD.getCode()) {
//        } else if (paymentModel.getBankCode() == Bank.EQTESDNOVIN.getCode()) {
//        }
//
//
//        return paymentModel.getOutput().returnResponseObject(ret, Return_Status_Codes_Common.SC_OK);
//    }
//
//
//}

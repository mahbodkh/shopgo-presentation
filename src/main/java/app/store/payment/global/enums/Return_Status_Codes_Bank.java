package app.store.payment.global.enums;



public enum Return_Status_Codes_Bank /*implements IReturn_Status_Codes */{

    UNDEFINED(0, 0, "undefined"),


    //error
    BANK_ERROR_INVALID_CODE(0, 16200, "bank_error_invalid_code"),
    BANK_ERROR_EXIST_BANK(0, 16201, "bank_error_exist_bank"),
    BANK_ERROR_NO_ACTIVE(0, 16202, "bank_error_no_active"),
    // success
    PAYMENT_DONE_SUBSCRIBER_TRANSACTION_SUCCESS(1, 16100, "global_subscriber_transaction_success"),

    ;


    private int status;
    private int code;
    private String message_key;

    Return_Status_Codes_Bank(int status, int code, String message_key) {
        this.status = status;
        this.code = code;
        this.message_key = message_key;
    }

    public static Return_Status_Codes_Bank get(int value) {

        if (value == 0) {
            return UNDEFINED;
        }

        Return_Status_Codes_Bank[] arr$ = values();
        for (Return_Status_Codes_Bank val : arr$) {
            if (val.code == value) {
                return val;
            }
        }

        return UNDEFINED;
    }

//    @Override
//    public String getMessage_key() {
//        return message_key;
//    }
//
//    @Override
//    public int getStatus() {
//        return status;
//    }
//
//    @Override
//    public int getCode() {
//        return code;
//    }
}

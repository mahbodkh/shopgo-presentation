//package app.store.payment.global.service;
//
//import com.vasl.vaslapp.modules.bankgateway.global.model.*;
//import com.vasl.vaslapp.modules.bankgateway.global.proto.holder.GateWay;
//
//public interface PayService {
//
//    GateWay.Pay.Builder payment(PaymentModel paymentModel) ;
//
//    GateWay.Callback.Builder getBankCallBack(CallbackModel CallbackModel);
//
//    GateWay.BankList.Builder listBank(BankListModel bankListModel);
//
//    GateWay.ConfirmTransaction.Builder confirmTransaction(ConfirmTransactionModel confirmTransactionModel);
//
//}

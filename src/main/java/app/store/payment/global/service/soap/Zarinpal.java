//package app.store.payment.global.service.soap;
//
//import com.vasl.vaslapp.common.global.interfaces.IReturn_Status_Codes;
//import com.vasl.vaslapp.modules.bankgateway.global.enums.Return_Status_Codes_Payment;
//import com.vasl.vaslapp.modules.bankgateway.global.mongo.repositories.acm_app.PaymentRepository;
//import org.bson.types.ObjectId;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.stereotype.Service;
//import org.w3c.dom.NodeList;
//
//import javax.net.ssl.HttpsURLConnection;
//import javax.xml.soap.*;
//import java.io.*;
//import java.net.URL;
//import java.net.URLConnection;
//import java.nio.charset.Charset;
//import java.util.Map;
//import java.util.concurrent.ConcurrentHashMap;
//
//@Service
//public class Zarinpal {
//
//
//    public static String logCode = "ZarinpalClass";
//    private static Logger log = LoggerFactory.getLogger(Zarinpal.class);
//
//    public static Map<String, String> PaymentRequest(String merchantID, Integer amount, String callbackURL,
//                                                     String payurl, ObjectId objectId) {
//
//        amount = amount / 10;//tomans
//        Map<String, String> res = new ConcurrentHashMap<>();
//        String SOAPAction = "#PaymentRequest";
//        String infWebSvcRequestMessage = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:zar=\"http://zarinpal.com/\">\n" +
//                "   <soapenv:Header/>\n" +
//                "   <soapenv:Body>\n" +
//                "      <zar:PaymentRequest>\n" +
//                "         <zar:MerchantID>" + merchantID + "</zar:MerchantID>\n" +
//                "         <zar:Amount>" + amount + "</zar:Amount>\n" +
//                "         <zar:Description>?</zar:Description>\n" +
//                "         <zar:CallbackURL>" + callbackURL + "</zar:CallbackURL>\n" +
//                "      </zar:PaymentRequest>\n" +
//                "   </soapenv:Body>\n" +
//                "</soapenv:Envelope>";
//
//
//        String urlString = payurl;
//        try {
//            URL urlForInfWebSvc = new URL(urlString);
//            URLConnection UrlConnInfWebSvc = urlForInfWebSvc.openConnection();
//            HttpsURLConnection httpUrlConnInfWebSvc = (HttpsURLConnection) UrlConnInfWebSvc;
//            httpUrlConnInfWebSvc.setDoOutput(true);
//            httpUrlConnInfWebSvc.setDoInput(true);
//            httpUrlConnInfWebSvc.setAllowUserInteraction(true);
//            httpUrlConnInfWebSvc.setRequestMethod("POST");
//            httpUrlConnInfWebSvc.setRequestProperty("Host", "www.zarinpal.com");
//            httpUrlConnInfWebSvc.setRequestProperty("SOAPAction", SOAPAction);
//            httpUrlConnInfWebSvc.setRequestProperty("Content-Type", "text/xml;charset=UTF-8");
//            OutputStreamWriter infWebSvcReqWriter = new OutputStreamWriter(httpUrlConnInfWebSvc.getOutputStream());
//            infWebSvcReqWriter.write(infWebSvcRequestMessage);
//            infWebSvcReqWriter.flush();
//            BufferedReader infWebSvcReplyReader = new BufferedReader(new InputStreamReader(httpUrlConnInfWebSvc.getInputStream()));
//            String line;
//            String infWebSvcReplyString = "";
//            while ((line = infWebSvcReplyReader.readLine()) != null) {
//                infWebSvcReplyString = infWebSvcReplyString.concat(line);
//            }
//            log.info(logCode + " PaymentRequest 0 : " + " ,transactionID: " + objectId.toString() + " ,response: " + infWebSvcReplyString);
//            MessageFactory factory = MessageFactory.newInstance();
//            SOAPMessage soapMessage = factory.createMessage(new MimeHeaders(), new ByteArrayInputStream(infWebSvcReplyString.getBytes(Charset.forName("UTF-8"))));
//            SOAPBody soapBody = soapMessage.getSOAPBody();
//            NodeList nodes1 = soapBody.getElementsByTagName("ns1:Status");
//            String someMsgContent = "";
//            Node node = (Node) nodes1.item(0);
//            someMsgContent = node != null ? node.getTextContent() : "";
//            res.put(PaymentRepository.Fields.STATUSHOLDER.getValue(), someMsgContent);
//            nodes1 = soapBody.getElementsByTagName("ns1:Authority");
//            String someMsgContent1 = "";
//            node = (Node) nodes1.item(0);
//            someMsgContent1 = node != null ? node.getTextContent() : "";
//            res.put(PaymentRepository.Fields.AUTHORITY.getValue(), someMsgContent1);
//
//            infWebSvcReqWriter.close();
//            infWebSvcReplyReader.close();
//            httpUrlConnInfWebSvc.disconnect();
//            log.info(logCode + " PaymentRequest 1 : " + " ,transactionID: " + objectId.toString() + " ,Status: " + someMsgContent + " , Authority: " + someMsgContent1);
//            return res;
//        } catch (SOAPException soex) {
//            String message = logCode + " PaymentRequest 2 : " + " ,transactionID: " + objectId.toString() + " , " + soex.toString();
//            log.error(message);
//            return null;
//        } catch (IOException ioex) {
//            String message = logCode + " PaymentRequest 3 : " + " ,transactionID: " + objectId.toString() + " , " + ioex.toString();
//            log.error(message);
//            return null;
//        }
//    }
//
//    public static Map<String, String> PaymentVerification(String MerchantID, Integer Amount, String Authority, String payurl, ObjectId objectId) {
//
//        Amount = Amount / 10;//tomans
//        Map<String, String> res = new ConcurrentHashMap<>();
//        String SOAPAction = "#PaymentVerification";
//        String infWebSvcRequestMessage = "" +
//                "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:zar=\"http://zarinpal.com/\">\n" +
//                "   <soapenv:Header/>\n" +
//                "   <soapenv:Body>\n" +
//                "      <zar:PaymentVerification>\n" +
//                "         <zar:MerchantID>" + MerchantID + "</zar:MerchantID>\n" +
//                "         <zar:Authority>" + Authority + "</zar:Authority>\n" +
//                "         <zar:Amount>" + Amount + "</zar:Amount>\n" +
//                "      </zar:PaymentVerification>\n" +
//                "   </soapenv:Body>\n" +
//                "</soapenv:Envelope>";
//
//
//        String urlString = payurl;
//        try {
//            URL urlForInfWebSvc = new URL(urlString);
//            URLConnection UrlConnInfWebSvc = urlForInfWebSvc.openConnection();
//            HttpsURLConnection httpUrlConnInfWebSvc = (HttpsURLConnection) UrlConnInfWebSvc;
//            httpUrlConnInfWebSvc.setDoOutput(true);
//            httpUrlConnInfWebSvc.setDoInput(true);
//            httpUrlConnInfWebSvc.setAllowUserInteraction(true);
//            httpUrlConnInfWebSvc.setRequestMethod("POST");
//            httpUrlConnInfWebSvc.setRequestProperty("Host", "www.zarinpal.com");
//            httpUrlConnInfWebSvc.setRequestProperty("SOAPAction", SOAPAction);
//            httpUrlConnInfWebSvc.setRequestProperty("Content-Type", "text/xml;charset=UTF-8");
//            OutputStreamWriter infWebSvcReqWriter = new OutputStreamWriter(httpUrlConnInfWebSvc.getOutputStream());
//            infWebSvcReqWriter.write(infWebSvcRequestMessage);
//            infWebSvcReqWriter.flush();
//            BufferedReader infWebSvcReplyReader = new BufferedReader(new InputStreamReader(httpUrlConnInfWebSvc.getInputStream()));
//            String line;
//            String infWebSvcReplyString = "";
//            while ((line = infWebSvcReplyReader.readLine()) != null) {
//                infWebSvcReplyString = infWebSvcReplyString.concat(line);
//            }
//            log.info(logCode + " PaymentVerification 0 : " + " ,transactionID: " + objectId.toString() +
//                    " ,response: " + infWebSvcReplyString);
//            MessageFactory factory = MessageFactory.newInstance();
//            SOAPMessage soapMessage = factory.createMessage(new MimeHeaders(), new ByteArrayInputStream(infWebSvcReplyString.getBytes(Charset.forName("UTF-8"))));
//            SOAPBody soapBody = soapMessage.getSOAPBody();
//            NodeList nodes1 = soapBody.getElementsByTagName("ns1:Status");
//            String someMsgContent = "";
//            Node node = (Node) nodes1.item(0);
//            someMsgContent = node != null ? node.getTextContent() : "";
//            res.put(PaymentRepository.Fields.STATUSHOLDER.getValue(), someMsgContent);
//            nodes1 = soapBody.getElementsByTagName("ns1:RefID");
//            String someMsgContent1 = "";
//            node = (Node) nodes1.item(0);
//            someMsgContent1 = node != null ? node.getTextContent() : "";
//            res.put(PaymentRepository.Fields.REFIDHOLDER.getValue(), someMsgContent1);
//            infWebSvcReqWriter.close();
//            infWebSvcReplyReader.close();
//            httpUrlConnInfWebSvc.disconnect();
//            log.info(logCode + " PaymentVerification 1 : " + " ,transactionID: " + objectId.toString() +
//                    " ,Status: " + someMsgContent + " , Authority: " + someMsgContent1);
//            return res;
//        } catch (SOAPException soex) {
//            String message = logCode + " PaymentVerification 2 : " + " ,transactionID: " + objectId.toString() + " , " + soex.toString();
//            log.error(message);
//            return null;
//        } catch (IOException ioex) {
//            String message = logCode + " PaymentVerification 3 : " + " ,transactionID: " + objectId.toString() + " , " + ioex.toString();
//            log.error(message);
//            return null;
//        }
//    }
//
//    public static IReturn_Status_Codes answerByZarinpal(String statusHolder) {
//        IReturn_Status_Codes return_status_codes = null;
//        switch (Integer.valueOf(statusHolder)) {
//            case -1:                // Invalid Request
//                return_status_codes = Return_Status_Codes_Payment.PAYMENT_ERROR_ZARINPAL_INVALID_REQUEST_1_;
//                return return_status_codes;
//            case -2:                // Invalid IP or merchantID
//                return_status_codes = Return_Status_Codes_Payment.PAYMENT_ERROR_ZARINPAL_INVALID_IP_MERCHANTID_2_;
//                return return_status_codes;
//            case -3:                // Amount should be higher than 100 $TOMAN
//                return_status_codes = Return_Status_Codes_Payment.PAYMENT_ERROR_ZARINPAL_AMOUNT_UNDER_T100_3_;
//                return return_status_codes;
//            case -4:                // Service unavailable
//                return_status_codes = Return_Status_Codes_Payment.PAYMENT_ERROR_ZARINPAL_SERVICE_UNAVAILABLE_4_;
//                return return_status_codes;
//            case -11:                // Resource not found
//                return_status_codes = Return_Status_Codes_Payment.PAYMENT_ERROR_ZARINPAL_RESOURCE_NOT_FOUND_11_;
//                return return_status_codes;
//            case -21:                // Payment not found
//                return_status_codes = Return_Status_Codes_Payment.PAYMENT_ERROR_ZARINPAL_PAYMENT_NOT_FOUND_21_;
//                return return_status_codes;
//            case -22:                // Result was failure
//                return_status_codes = Return_Status_Codes_Payment.PAYMENT_ERROR_ZARINPAL_RESULT_WAS_FAILURE_22_;
//                return return_status_codes;
//            case -33:                // Amount was not matched
//                return_status_codes = Return_Status_Codes_Payment.PAYMENT_ERROR_ZARINPAL_AMOUNT_WAS_NOT_MATCHED_33_;
//                return return_status_codes;
//            case -54:                // Result was archived
//                return_status_codes = Return_Status_Codes_Payment.PAYMENT_ERROR_ZARINPAL_RESULT_WAS_ARCHIVED_54_;
//                return return_status_codes;
//            default:
//                return_status_codes = Return_Status_Codes_Payment.PAYMENT_ERROR_PAYMENT_OTHER_ERROR;
//                break;
//        }
//        return_status_codes = Return_Status_Codes_Payment.PAYMENT_ERROR_REQUEST_FAILED;
//        return return_status_codes;
//    }
//}

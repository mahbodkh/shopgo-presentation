//package app.store.payment.global.service;
//
//import com.vasl.vaslapp.common.global.enums.Return_Status_Codes_Common;
//import com.vasl.vaslapp.common.global.interfaces.IReturn_Status_Codes;
//import com.vasl.vaslapp.modules.bankgateway.global.enums.Return_Status_Codes_Payment;
//import com.vasl.vaslapp.modules.bankgateway.global.enums.TransactionStatus;
//import com.vasl.vaslapp.modules.bankgateway.global.general.General;
//import com.vasl.vaslapp.modules.bankgateway.global.model.PaymentModel;
//import com.vasl.vaslapp.modules.bankgateway.global.mongo.repositories.acm_app.GatewayConfig;
//import com.vasl.vaslapp.modules.bankgateway.global.mongo.repositories.acm_app.GlobalTransaction;
//import com.vasl.vaslapp.modules.bankgateway.global.proto.holder.GateWay;
//import com.vasl.vaslapp.modules.bankgateway.global.service.webServices.Mellat;
//import com.vasl.vaslapp.modules.bankgateway.global.service.webServices.Tejarat;
//import com.vasl.vaslapp.modules.bankgateway.global.service.webServices.Zarinpal;
//import org.bson.Document;
//import org.bson.types.ObjectId;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Service;
//
//import java.util.Map;
//import java.util.concurrent.ConcurrentHashMap;
//
//@Service
//public class GateWayServiceImpl {
//    private static String logCode = "GateWayServiceImplClass";
//    private static Logger log = LoggerFactory.getLogger(Zarinpal.class);
//    private long now = System.currentTimeMillis();
//    private Zarinpal zar;
//    private Mellat mellat;
//    private Tejarat tejarat;
//    private String callBackUrlPath = "https://server.pazhapp.com/actions/gateway/payment/callback/";
//
//
//    @Autowired
//    public GateWayServiceImpl(Zarinpal zar, Mellat mellat, Tejarat tejarat) {
//        this.mellat = mellat;
//        this.zar = zar;
//        this.tejarat = tejarat;
//    }
//
//    public GateWay.Pay.Builder payByZarinpal(PaymentModel paymentModel) {
//        GateWay.Pay.Builder ret = GateWay.Pay.newBuilder();
//        Document newdoc = new Document();
//        Map<String, String> result = new ConcurrentHashMap<>();
//        IReturn_Status_Codes return_status_codes = paymentModel.getReturn_status_codes();
//        GlobalTransaction transaction = new GlobalTransaction(paymentModel.getOutput().getMongodb_app());
//        ObjectId objectId = new ObjectId();
//        String appid = paymentModel.getAppid();
//        String paymenturl = paymentModel.getBankDocument().getString(GatewayConfig.Fields.PAYMENT_URL.getValue());
//        String callbackUrl = callBackUrlPath + appid + "/" + objectId + "/" + paymentModel.getBankCode();
//        String statusHolder = "-1";
//        String authorityHolder = "-1";
//        String gatewayUrl = "";
//        try {
//            result = zar.PaymentRequest(
//                    paymentModel.getBankDocument().getString(GatewayConfig.Fields.MERCHANT.getValue())
//                    , paymentModel.getAmount()
//                    , callbackUrl
//                    , paymenturl
//                    , objectId);
//
//            if (result == null) {
//                return_status_codes = Return_Status_Codes_Payment.PAYMENT_ERROR_REQUEST_FAILED;
//                ret = paymentModel.getOutput().returnResponseObject(ret, return_status_codes);
//                return ret;
//            }
//            if (result != null && result.get(GlobalTransaction.Fields.STATUSHOLDER.getValue()).length() > 0)
//                statusHolder = result.get(GlobalTransaction.Fields.STATUSHOLDER.getValue());
//            if (result != null && result.get(GlobalTransaction.Fields.AUTHORITY.getValue()).length() > 0)
//                authorityHolder = result.get(GlobalTransaction.Fields.AUTHORITY.getValue());
//
//            if (!statusHolder.equalsIgnoreCase("100") || authorityHolder.length() != 36) {
//                return_status_codes = zar.answerByZarinpal(statusHolder);
//                newdoc.put(GlobalTransaction.Fields.ID.getValue(), objectId);
//                newdoc.put(GlobalTransaction.Fields.AMOUNT.getValue(), paymentModel.getAmount().toString());
//                newdoc.put(GlobalTransaction.Fields.BANKCODE.getValue(),
//                        paymentModel.getBankDocument().getInteger(GatewayConfig.Fields.CODE.getValue()).toString());
//                newdoc.put(GlobalTransaction.Fields.STATUSHOLDER.getValue(), statusHolder);
//                newdoc.put(GlobalTransaction.Fields.AUTHORITY.getValue(), authorityHolder);
//                newdoc.put(GlobalTransaction.Fields.PAYMENT_TIME.getValue(), paymentModel.getTransactionTime());
//                newdoc.put(GlobalTransaction.Fields.CALLBACKURL_USER.getValue(), paymentModel.getCallBackUserURL());
//                newdoc.put(GlobalTransaction.Fields.User_ORDERID.getValue(), paymentModel.getOrderId());
//                newdoc.put(GlobalTransaction.Fields.TRANSACTIONSTATUS.getValue(), TransactionStatus.INVALID_AUTHORITY.getValue());
//                newdoc.put(GlobalTransaction.Fields.ERROR.getValue(), logCode + " payByZarinpal 2 : " + return_status_codes);
//                transaction.insertOne(newdoc);
//                ret = paymentModel.getOutput().returnResponseObject(ret, return_status_codes);
//                String message = logCode + " payByZarinpal 2 : transactionID:" + objectId +
//                        " ," + GlobalTransaction.Fields.BANKCODE.getValue() + ":" +
//                        paymentModel.getBankDocument().getInteger(GatewayConfig.Fields.CODE.getValue()).toString() +
//                        " ," + GlobalTransaction.Fields.GATEWAYURL.getValue() + ":" + gatewayUrl +
//                        " ," + GlobalTransaction.Fields.STATUSHOLDER.getValue() + ":" + statusHolder +
//                        " ," + GlobalTransaction.Fields.AUTHORITY.getValue() + ":" + authorityHolder +
//                        " ," + GlobalTransaction.Fields.PAYMENT_TIME.getValue() + ":" + paymentModel.getTransactionTime() +
//                        " ," + GlobalTransaction.Fields.CALLBACKURL_BANK.getValue() + ":" + callbackUrl +
//                        " ," + GlobalTransaction.Fields.TRANSACTIONSTATUS.getValue() + ":" + TransactionStatus.INVALID_AUTHORITY.getValue() +
//                        " ," + GlobalTransaction.Fields.CALLBACKURL_USER.getValue() + ":" + paymentModel.getCallBackUserURL() +
//                        " ," + GlobalTransaction.Fields.User_ORDERID.getValue() + ":" + paymentModel.getOrderId();
//                log.info(message);
//                return ret;
//            } else {
//                gatewayUrl = paymentModel.getBankDocument().getString(GatewayConfig.Fields.GATEWAY_URL.getValue())
//                        + authorityHolder + paymentModel.getBankDocument().getString(GatewayConfig.Fields.GATEWAY_URL_POSTFIX.getValue());
//                newdoc.put(GlobalTransaction.Fields.ID.getValue(), objectId);
//                newdoc.put(GlobalTransaction.Fields.AMOUNT.getValue(), paymentModel.getAmount().toString());
//                newdoc.put(GlobalTransaction.Fields.BANKCODE.getValue(),
//                        paymentModel.getBankDocument().getInteger(GatewayConfig.Fields.CODE.getValue()).toString());
//                newdoc.put(GlobalTransaction.Fields.GATEWAYURL.getValue(), gatewayUrl);
//                newdoc.put(GlobalTransaction.Fields.STATUSHOLDER.getValue(), statusHolder);
//                newdoc.put(GlobalTransaction.Fields.AUTHORITY.getValue(), authorityHolder);
//                newdoc.put(GlobalTransaction.Fields.PAYMENT_TIME.getValue(), paymentModel.getTransactionTime());
//                newdoc.put(GlobalTransaction.Fields.CALLBACKURL_BANK.getValue(), callbackUrl);
//                newdoc.put(GlobalTransaction.Fields.TRANSACTIONSTATUS.getValue(), TransactionStatus.NOT_CHECKED.getValue());
//                newdoc.put(GlobalTransaction.Fields.CALLBACKURL_USER.getValue(), paymentModel.getCallBackUserURL());
//                newdoc.put(GlobalTransaction.Fields.User_ORDERID.getValue(), paymentModel.getOrderId());
//                transaction.insertOne(newdoc);
//                ret.setGatewayUrl(gatewayUrl);
//                ret = paymentModel.getOutput().returnResponseObject(ret, Return_Status_Codes_Common.SC_OK);
//                String message = logCode + " payByZarinpal 1 : transactionID:" + objectId
//                        + " ," + GlobalTransaction.Fields.BANKCODE.getValue() + ":" +
//                        paymentModel.getBankDocument().getInteger(GatewayConfig.Fields.CODE.getValue()).toString() +
//                        " ," + GlobalTransaction.Fields.GATEWAYURL.getValue() + ":" + gatewayUrl +
//                        " ," + GlobalTransaction.Fields.STATUSHOLDER.getValue() + ":" + statusHolder +
//                        " ," + GlobalTransaction.Fields.AUTHORITY.getValue() + ":" + authorityHolder +
//                        " ," + GlobalTransaction.Fields.PAYMENT_TIME.getValue() + ":" + paymentModel.getTransactionTime() +
//                        " ," + GlobalTransaction.Fields.CALLBACKURL_BANK.getValue() + ":" + callbackUrl +
//                        " ," + GlobalTransaction.Fields.TRANSACTIONSTATUS.getValue() + ":" + TransactionStatus.NOT_CHECKED.getValue() +
//                        " ," + GlobalTransaction.Fields.CALLBACKURL_USER.getValue() + ":" + paymentModel.getCallBackUserURL() +
//                        " ," + GlobalTransaction.Fields.User_ORDERID.getValue() + ":" + paymentModel.getOrderId();
//                log.info(message);
//                return ret;
//            }
//        } catch (Exception ex) {/*
//            newdoc.put(PaymentRepository.Fields.ID.getValue(), objectId);
//            newdoc.put(PaymentRepository.Fields.AMOUNT.getValue(), paymentModel.getAmount().toString());
//            newdoc.put(PaymentRepository.Fields.BANKCODE.getValue(),
//                    paymentModel.getBankDocument().getInteger(BankRepository.Fields.CODE.getValue()).toString());
//            newdoc.put(PaymentRepository.Fields.PAYMENT_TIME.getValue(), paymentModel.getTransactionTime());
//            newdoc.put(PaymentRepository.Fields.ERROR.getValue(), logCode + " payByZarinpal 3 : " + ex.toString());
//            newdoc.put(PaymentRepository.Fields.TRANSACTIONSTATUS.getValue(), TransactionStatus.PROCESSED_ERROR_REQUEST_CALL_FAILED.getValue());
//            newdoc.put(PaymentRepository.Fields.CALLBACKURL_USER.getValue(), paymentModel.getCallBackUserURL());
//            newdoc.put(PaymentRepository.Fields.User_ORDERID.getValue(), paymentModel.getOrderId());
//            transaction.insertOne(newdoc);*/
//            return_status_codes = Return_Status_Codes_Payment.PAYMENT_ERROR_REQUEST_FAILED;
//            ex.printStackTrace();
//            ret = paymentModel.getOutput().returnResponseObject(ret, return_status_codes);
//            String message = logCode + " payByZarinpal 3 : transactionID:" + objectId
//                    + " ," + GlobalTransaction.Fields.BANKCODE.getValue() + ":" +
//                    paymentModel.getBankDocument().getInteger(GatewayConfig.Fields.CODE.getValue()).toString() +
//                    " ," + GlobalTransaction.Fields.PAYMENT_TIME.getValue() + ":" + paymentModel.getTransactionTime() +
//                    " ," + GlobalTransaction.Fields.CALLBACKURL_USER.getValue() + ":" + paymentModel.getCallBackUserURL() +
//                    " ," + GlobalTransaction.Fields.User_ORDERID.getValue() + ":" + paymentModel.getOrderId() +
//                    " ," + ex.toString();
//            log.error(message);
//            return ret;
//        }
//    }
//
//    public GateWay.Pay.Builder payByTejarat(PaymentModel paymentModel) {
//        GateWay.Pay.Builder ret = GateWay.Pay.newBuilder();
//        Document newdoc = new Document();
//        Map<String, String> result = new ConcurrentHashMap<>();
//        IReturn_Status_Codes return_status_codes = paymentModel.getReturn_status_codes();
//        GlobalTransaction transaction = new GlobalTransaction(paymentModel.getOutput().getMongodb_app());
//        ObjectId objectId = new ObjectId();
//        String appid = paymentModel.getAppid();
//        String baseUrl = paymentModel.getBankDocument().getString(GatewayConfig.Fields.BASE_URL.getValue());
//        String callbackUrl = callBackUrlPath + appid + "/" + objectId + "/" + paymentModel.getBankCode();
//        String statusHolder = "-1";
//        String authorityHolder = "-1";
//        String gatewayUrl = "-1";
//        String payOrderId = General.randomNumberGenerate();
//        try {
//            result = tejarat.MakeToken(paymentModel.getAmount(),
//                    paymentModel.getBankDocument().getString(GatewayConfig.Fields.MERCHANT.getValue())
//                    , payOrderId
//                    , callbackUrl
//                    , baseUrl
//                    , objectId);
//
//
//            if (result == null) {
//                return_status_codes = Return_Status_Codes_Payment.PAYMENT_ERROR_REQUEST_FAILED;
//                ret = paymentModel.getOutput().returnResponseObject(ret, return_status_codes);
//                return ret;
//            }
//            if (result != null && result.size() == 1) {
//                statusHolder = result.get(GlobalTransaction.Fields.STATUSHOLDER.getValue());
//            }
//            if (result != null && result.size() == 2) {
//                statusHolder = result.get(GlobalTransaction.Fields.STATUSHOLDER.getValue());
//                authorityHolder = result.get(GlobalTransaction.Fields.AUTHORITY.getValue());
//            }
//
//            if (!statusHolder.equalsIgnoreCase("Success")) {
//                newdoc.put(GlobalTransaction.Fields.ID.getValue(), objectId);
//                newdoc.put(GlobalTransaction.Fields.AMOUNT.getValue(), paymentModel.getAmount().toString());
//                newdoc.put(GlobalTransaction.Fields.BANKCODE.getValue(),
//                        paymentModel.getBankDocument().getInteger(GatewayConfig.Fields.CODE.getValue()).toString());
//                newdoc.put(GlobalTransaction.Fields.STATUSHOLDER.getValue(), statusHolder);
//                newdoc.put(GlobalTransaction.Fields.PAYMENT_TIME.getValue(), paymentModel.getTransactionTime());
//                newdoc.put(GlobalTransaction.Fields.CALLBACKURL_USER.getValue(), paymentModel.getCallBackUserURL());
//                newdoc.put(GlobalTransaction.Fields.User_ORDERID.getValue(), paymentModel.getOrderId());
//                newdoc.put(GlobalTransaction.Fields.PAY_ORDERID.getValue(), payOrderId);
//                newdoc.put(GlobalTransaction.Fields.TRANSACTIONSTATUS.getValue(), TransactionStatus.INVALID_AUTHORITY.getValue());
//                transaction.insertOne(newdoc);
//                ret = paymentModel.getOutput().returnResponseObject(ret, return_status_codes);
//                String message = logCode + " payByTejarat 2 : transactionID:" + objectId
//                        + " ," + GlobalTransaction.Fields.BANKCODE.getValue() + ":" +
//                        paymentModel.getBankDocument().getInteger(GatewayConfig.Fields.CODE.getValue()).toString() +
//                        " ," + GlobalTransaction.Fields.GATEWAYURL.getValue() + ":" + gatewayUrl +
//                        " ," + GlobalTransaction.Fields.STATUSHOLDER.getValue() + ":" + statusHolder +
//                        " ," + GlobalTransaction.Fields.AUTHORITY.getValue() + ":" + authorityHolder +
//                        " ," + GlobalTransaction.Fields.PAYMENT_TIME.getValue() + ":" + paymentModel.getTransactionTime() +
//                        " ," + GlobalTransaction.Fields.CALLBACKURL_BANK.getValue() + ":" + callbackUrl +
//                        " ," + GlobalTransaction.Fields.TRANSACTIONSTATUS.getValue() + ":" + TransactionStatus.INVALID_AUTHORITY.getValue() +
//                        " ," + GlobalTransaction.Fields.CALLBACKURL_USER.getValue() + ":" + paymentModel.getCallBackUserURL() +
//                        " ," + GlobalTransaction.Fields.PAY_ORDERID.getValue() + ":" + payOrderId +
//                        " ," + GlobalTransaction.Fields.User_ORDERID.getValue() + ":" + paymentModel.getOrderId();
//                log.info(message);
//                return ret;
//            } else {
//                gatewayUrl = paymentModel.getBankDocument().getString(GatewayConfig.Fields.GATEWAY_URL.getValue());
//                newdoc.put(GlobalTransaction.Fields.ID.getValue(), objectId);
//                newdoc.put(GlobalTransaction.Fields.AMOUNT.getValue(), paymentModel.getAmount().toString());
//                newdoc.put(GlobalTransaction.Fields.BANKCODE.getValue(),
//                        paymentModel.getBankDocument().getInteger(GatewayConfig.Fields.CODE.getValue()).toString());
//                newdoc.put(GlobalTransaction.Fields.GATEWAYURL.getValue(), gatewayUrl);
//                newdoc.put(GlobalTransaction.Fields.STATUSHOLDER.getValue(), statusHolder);
//                newdoc.put(GlobalTransaction.Fields.AUTHORITY.getValue(), authorityHolder);
//                newdoc.put(GlobalTransaction.Fields.PAYMENT_TIME.getValue(), paymentModel.getTransactionTime());
//                newdoc.put(GlobalTransaction.Fields.CALLBACKURL_BANK.getValue(), callbackUrl);
//                newdoc.put(GlobalTransaction.Fields.TRANSACTIONSTATUS.getValue(), TransactionStatus.NOT_CHECKED.getValue());
//                newdoc.put(GlobalTransaction.Fields.CALLBACKURL_USER.getValue(), paymentModel.getCallBackUserURL());
//                newdoc.put(GlobalTransaction.Fields.User_ORDERID.getValue(), paymentModel.getOrderId());
//                newdoc.put(GlobalTransaction.Fields.PAY_ORDERID.getValue(), payOrderId);
//                transaction.insertOne(newdoc);
//                ret.setGatewayUrl(gatewayUrl);
//                ret.setRefid(authorityHolder);
//                ret = paymentModel.getOutput().returnResponseObject(ret, Return_Status_Codes_Payment.PAYMENT_IS_OK);
//                String message = logCode + " payByTejarat 1 : transactionID:" + objectId
//                        + " ," + GlobalTransaction.Fields.BANKCODE.getValue() + ":" +
//                        paymentModel.getBankDocument().getInteger(GatewayConfig.Fields.CODE.getValue()).toString() +
//                        " ," + GlobalTransaction.Fields.GATEWAYURL.getValue() + ":" + gatewayUrl +
//                        " ," + GlobalTransaction.Fields.STATUSHOLDER.getValue() + ":" + statusHolder +
//                        " ," + GlobalTransaction.Fields.AUTHORITY.getValue() + ":" + authorityHolder +
//                        " ," + GlobalTransaction.Fields.PAYMENT_TIME.getValue() + ":" + paymentModel.getTransactionTime() +
//                        " ," + GlobalTransaction.Fields.CALLBACKURL_BANK.getValue() + ":" + callbackUrl +
//                        " ," + GlobalTransaction.Fields.TRANSACTIONSTATUS.getValue() + ":" + TransactionStatus.NOT_CHECKED.getValue() +
//                        " ," + GlobalTransaction.Fields.CALLBACKURL_USER.getValue() + ":" + paymentModel.getCallBackUserURL() +
//                        " ," + GlobalTransaction.Fields.PAY_ORDERID.getValue() + ":" + payOrderId +
//                        " ," + GlobalTransaction.Fields.User_ORDERID.getValue() + ":" + paymentModel.getOrderId();
//                log.info(message);
//                return ret;
//            }
//        } catch (Exception ex) {/*
//            newdoc.put(PaymentRepository.Fields.ID.getValue(), objectId);
//            newdoc.put(PaymentRepository.Fields.AMOUNT.getValue(), paymentModel.getAmount().toString());
//            newdoc.put(PaymentRepository.Fields.BANKCODE.getValue(),
//                    paymentModel.getBankDocument().getInteger(BankRepository.Fields.CODE.getValue()).toString());
//            newdoc.put(PaymentRepository.Fields.PAYMENT_TIME.getValue(), paymentModel.getTransactionTime());
//            newdoc.put(PaymentRepository.Fields.ERROR.getValue(), logCode + " payByTejarat 3 : " + ex.toString());
//            newdoc.put(PaymentRepository.Fields.TRANSACTIONSTATUS.getValue(), TransactionStatus.PROCESSED_ERROR_REQUEST_CALL_FAILED.getValue());
//            newdoc.put(PaymentRepository.Fields.CALLBACKURL_USER.getValue(), paymentModel.getCallBackUserURL());
//            newdoc.put(PaymentRepository.Fields.User_ORDERID.getValue(), paymentModel.getOrderId());
//            newdoc.put(PaymentRepository.Fields.PAY_ORDERID.getValue(), payOrderId);
//            transaction.insertOne(newdoc);*/
//            return_status_codes = Return_Status_Codes_Payment.PAYMENT_ERROR_REQUEST_FAILED;
//            ex.printStackTrace();
//            ret = paymentModel.getOutput().returnResponseObject(ret, return_status_codes);
//            String message = logCode + " payByTejarat 3 : transactionID:" + objectId
//                    + " ," + GlobalTransaction.Fields.BANKCODE.getValue() + ":" +
//                    paymentModel.getBankDocument().getInteger(GatewayConfig.Fields.CODE.getValue()).toString() +
//                    " ," + GlobalTransaction.Fields.PAYMENT_TIME.getValue() + ":" + paymentModel.getTransactionTime() +
//                    " ," + GlobalTransaction.Fields.CALLBACKURL_USER.getValue() + ":" + paymentModel.getCallBackUserURL() +
//                    " ," + GlobalTransaction.Fields.PAY_ORDERID.getValue() + ":" + payOrderId +
//                    " ," + GlobalTransaction.Fields.User_ORDERID.getValue() + ":" + paymentModel.getOrderId() +
//                    " ," + ex.toString();
//            log.error(message);
//            return ret;
//        }
//
//    }
//
//    public GateWay.Pay.Builder payByMellat(PaymentModel paymentModel) {
//        GateWay.Pay.Builder ret = GateWay.Pay.newBuilder();
//        Document newdoc = new Document();
//        Map<String, String> result = new ConcurrentHashMap<>();
//        IReturn_Status_Codes return_status_codes = paymentModel.getReturn_status_codes();
//        GlobalTransaction transaction = new GlobalTransaction(paymentModel.getOutput().getMongodb_app());
//        ObjectId objectId = new ObjectId();
//        String appid = paymentModel.getAppid();
//        //String paymenturl = paymentModel.getBankDocument().getString(BankRepository.Fields.PAYMENT_URL.getValue());
//        String callbackUrl = callBackUrlPath + appid + "/" + objectId + "/" + paymentModel.getBankCode();
//        String statusHolder = "-1";
//        String authorityHolder = "-1";
//        String gatewayUrl = "";
//        String payOrderId = General.randomNumberGenerate();
//        try {
//            result = mellat.bpPayRequest(
//                    paymentModel.getBankDocument().getString(GatewayConfig.Fields.TERMINALID.getValue()),
//                    paymentModel.getBankDocument().getString(GatewayConfig.Fields.USERNAME.getValue()),
//                    paymentModel.getBankDocument().getString(GatewayConfig.Fields.PASSWORD.getValue()),
//                    payOrderId,
//                    paymentModel.getAmount(),
//                    callbackUrl,
//                    paymentModel.getBankDocument().getString(GatewayConfig.Fields.PAYMENT_URL.getValue()),
//                    objectId
//            );
//
//            if (result == null) {
//                return_status_codes = Return_Status_Codes_Payment.PAYMENT_ERROR_REQUEST_FAILED;
//                ret = paymentModel.getOutput().returnResponseObject(ret, return_status_codes);
//                return ret;
//            }
//            if (result != null && result.size() == 1) {
//                statusHolder = result.get(GlobalTransaction.Fields.STATUSHOLDER.getValue());
//            } else if (result != null && result.size() == 2) {
//                statusHolder = result.get(GlobalTransaction.Fields.STATUSHOLDER.getValue());
//                authorityHolder = result.get(GlobalTransaction.Fields.REFIDHOLDER.getValue());
//            }
//            if (!statusHolder.equalsIgnoreCase("0")) {
//                return_status_codes = mellat.answerByMellats(statusHolder);
//                newdoc.put(GlobalTransaction.Fields.ID.getValue(), objectId);
//                newdoc.put(GlobalTransaction.Fields.AMOUNT.getValue(), paymentModel.getAmount().toString());
//                newdoc.put(GlobalTransaction.Fields.BANKCODE.getValue(),
//                        paymentModel.getBankDocument().getInteger(GatewayConfig.Fields.CODE.getValue()).toString());
//                newdoc.put(GlobalTransaction.Fields.STATUSHOLDER.getValue(), statusHolder);
//                newdoc.put(GlobalTransaction.Fields.PAYMENT_TIME.getValue(), paymentModel.getTransactionTime());
//                newdoc.put(GlobalTransaction.Fields.CALLBACKURL_USER.getValue(), paymentModel.getCallBackUserURL());
//                newdoc.put(GlobalTransaction.Fields.User_ORDERID.getValue(), paymentModel.getOrderId());
//                newdoc.put(GlobalTransaction.Fields.PAY_ORDERID.getValue(), payOrderId);
//                newdoc.put(GlobalTransaction.Fields.TRANSACTIONSTATUS.getValue(), TransactionStatus.INVALID_AUTHORITY.getValue());
//                newdoc.put(GlobalTransaction.Fields.ERROR.getValue(), logCode + " payByMellat 2 : " + return_status_codes);
//                transaction.insertOne(newdoc);
//                ret = paymentModel.getOutput().returnResponseObject(ret, return_status_codes);
//                String message = logCode + " payByMellat 2 : transactionID:" + objectId
//                        + " ," + GlobalTransaction.Fields.BANKCODE.getValue() + ":" +
//                        paymentModel.getBankDocument().getInteger(GatewayConfig.Fields.CODE.getValue()).toString() +
//                        " ," + GlobalTransaction.Fields.GATEWAYURL.getValue() + ":" + gatewayUrl +
//                        " ," + GlobalTransaction.Fields.STATUSHOLDER.getValue() + ":" + statusHolder +
//                        " ," + GlobalTransaction.Fields.AUTHORITY.getValue() + ":" + authorityHolder +
//                        " ," + GlobalTransaction.Fields.PAYMENT_TIME.getValue() + ":" + paymentModel.getTransactionTime() +
//                        " ," + GlobalTransaction.Fields.CALLBACKURL_BANK.getValue() + ":" + callbackUrl +
//                        " ," + GlobalTransaction.Fields.TRANSACTIONSTATUS.getValue() + ":" + TransactionStatus.INVALID_AUTHORITY.getValue() +
//                        " ," + GlobalTransaction.Fields.CALLBACKURL_USER.getValue() + ":" + paymentModel.getCallBackUserURL() +
//                        " ," + GlobalTransaction.Fields.PAY_ORDERID.getValue() + ":" + payOrderId +
//                        " ," + GlobalTransaction.Fields.User_ORDERID.getValue() + ":" + paymentModel.getOrderId();
//                log.info(message);
//                return ret;
//            } else {
//                gatewayUrl = paymentModel.getBankDocument().getString(GatewayConfig.Fields.GATEWAY_URL.getValue());
//                newdoc.put(GlobalTransaction.Fields.ID.getValue(), objectId);
//                newdoc.put(GlobalTransaction.Fields.AMOUNT.getValue(), paymentModel.getAmount().toString());
//                newdoc.put(GlobalTransaction.Fields.BANKCODE.getValue(),
//                        paymentModel.getBankDocument().getInteger(GatewayConfig.Fields.CODE.getValue()).toString());
//                newdoc.put(GlobalTransaction.Fields.GATEWAYURL.getValue(), gatewayUrl);
//                newdoc.put(GlobalTransaction.Fields.STATUSHOLDER.getValue(), statusHolder);
//                newdoc.put(GlobalTransaction.Fields.REFIDHOLDER.getValue(), authorityHolder);
//                newdoc.put(GlobalTransaction.Fields.PAYMENT_TIME.getValue(), paymentModel.getTransactionTime());
//                newdoc.put(GlobalTransaction.Fields.CALLBACKURL_BANK.getValue(), callbackUrl);
//                newdoc.put(GlobalTransaction.Fields.TRANSACTIONSTATUS.getValue(), TransactionStatus.NOT_CHECKED.getValue());
//                newdoc.put(GlobalTransaction.Fields.CALLBACKURL_USER.getValue(), paymentModel.getCallBackUserURL());
//                newdoc.put(GlobalTransaction.Fields.User_ORDERID.getValue(), paymentModel.getOrderId());
//                newdoc.put(GlobalTransaction.Fields.PAY_ORDERID.getValue(), payOrderId);
//                transaction.insertOne(newdoc);
//                ret.setGatewayUrl(gatewayUrl);
//                ret.setRefid(authorityHolder);
//                ret = paymentModel.getOutput().returnResponseObject(ret,
//                        Return_Status_Codes_Payment.PAYMENT_OK_MELLAT_OK_TRANSACTION_0);
//                String message = logCode + " payByMellat 1 : transactionID:" + objectId
//                        + " ," + GlobalTransaction.Fields.BANKCODE.getValue() + ":" +
//                        paymentModel.getBankDocument().getInteger(GatewayConfig.Fields.CODE.getValue()).toString() +
//                        " ," + GlobalTransaction.Fields.GATEWAYURL.getValue() + ":" + gatewayUrl +
//                        " ," + GlobalTransaction.Fields.STATUSHOLDER.getValue() + ":" + statusHolder +
//                        " ," + GlobalTransaction.Fields.AUTHORITY.getValue() + ":" + authorityHolder +
//                        " ," + GlobalTransaction.Fields.PAYMENT_TIME.getValue() + ":" + paymentModel.getTransactionTime() +
//                        " ," + GlobalTransaction.Fields.CALLBACKURL_BANK.getValue() + ":" + callbackUrl +
//                        " ," + GlobalTransaction.Fields.TRANSACTIONSTATUS.getValue() + ":" + TransactionStatus.NOT_CHECKED.getValue() +
//                        " ," + GlobalTransaction.Fields.CALLBACKURL_USER.getValue() + ":" + paymentModel.getCallBackUserURL() +
//                        newdoc.put(GlobalTransaction.Fields.PAY_ORDERID.getValue(), payOrderId) +
//                        " ," + GlobalTransaction.Fields.User_ORDERID.getValue() + ":" + paymentModel.getOrderId();
//                log.info(message);
//                return ret;
//            }
//        } catch (Exception ex) {
//            newdoc.put(GlobalTransaction.Fields.ID.getValue(), objectId);
//            newdoc.put(GlobalTransaction.Fields.AMOUNT.getValue(), paymentModel.getAmount().toString());
//            newdoc.put(GlobalTransaction.Fields.BANKCODE.getValue(),
//                    paymentModel.getBankDocument().getInteger(GatewayConfig.Fields.CODE.getValue()).toString());
//            newdoc.put(GlobalTransaction.Fields.PAYMENT_TIME.getValue(), paymentModel.getTransactionTime());
//            newdoc.put(GlobalTransaction.Fields.ERROR.getValue(), logCode + " payByMellat 3 : " + ex.toString());
//            newdoc.put(GlobalTransaction.Fields.TRANSACTIONSTATUS.getValue(), TransactionStatus.PROCESSED_ERROR_REQUEST_CALL_FAILED.getValue());
//            newdoc.put(GlobalTransaction.Fields.CALLBACKURL_USER.getValue(), paymentModel.getCallBackUserURL());
//            newdoc.put(GlobalTransaction.Fields.User_ORDERID.getValue(), paymentModel.getOrderId());
//            newdoc.put(GlobalTransaction.Fields.PAY_ORDERID.getValue(), payOrderId);
//            transaction.insertOne(newdoc);
//            return_status_codes = Return_Status_Codes_Payment.PAYMENT_ERROR_REQUEST_FAILED;
//            ex.printStackTrace();
//            ret = paymentModel.getOutput().returnResponseObject(ret, return_status_codes);
//            String message = logCode + " payByMellat 3 : transactionID:" + objectId
//                    + " ," + GlobalTransaction.Fields.BANKCODE.getValue() + ":" +
//                    paymentModel.getBankDocument().getInteger(GatewayConfig.Fields.CODE.getValue()).toString() +
//                    " ," + GlobalTransaction.Fields.PAYMENT_TIME.getValue() + ":" + paymentModel.getTransactionTime() +
//                    " ," + GlobalTransaction.Fields.CALLBACKURL_USER.getValue() + ":" + paymentModel.getCallBackUserURL() +
//                    " ," + GlobalTransaction.Fields.User_ORDERID.getValue() + ":" + paymentModel.getOrderId() +
//                    " ," + ex.toString();
//            log.error(message);
//            return ret;
//        }
//
//    }
//
//
//}
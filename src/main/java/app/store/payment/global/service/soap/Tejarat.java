//package app.store.payment.global.service.soap;
//
//import com.vasl.vaslapp.common.global.interfaces.IReturn_Status_Codes;
//import com.vasl.vaslapp.modules.bankgateway.global.enums.Return_Status_Codes_Payment;
//import com.vasl.vaslapp.modules.bankgateway.global.mongo.repositories.acm_app.PaymentRepository;
//import org.bson.types.ObjectId;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.stereotype.Service;
//import org.w3c.dom.NodeList;
//
//import javax.net.ssl.HttpsURLConnection;
//import javax.xml.soap.*;
//import java.io.*;
//import java.net.URL;
//import java.net.URLConnection;
//import java.nio.charset.Charset;
//import java.util.Map;
//import java.util.concurrent.ConcurrentHashMap;
//
//@Service
//public class Tejarat {
//    public static String logCode = "TejaratClass";
//    private static Logger log = LoggerFactory.getLogger(Zarinpal.class);
//
//    public static Map<String, String> MakeToken(Integer amount, String merchantID, String orderId
//            , String callbackURL, String baseUrl, ObjectId objectId) {
//
//        //amount=amount/10;//tomans
//        Map<String, String> res = new ConcurrentHashMap<>();
//        String SOAPAction = "http://tempuri.org/ITokens/MakeToken";
//        String infWebSvcRequestMessage = "" +
//                "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:tem=\"http://tempuri.org/\">\n" +
//                "   <soapenv:Header/>\n" +
//                "   <soapenv:Body>\n" +
//                "      <tem:MakeToken>\n" +
//                "         <tem:amount>" + amount + "</tem:amount>\n" +
//                "         <tem:merchantId>" + merchantID + "</tem:merchantId>\n" +
//                "         <tem:invoiceNo>" + orderId + "</tem:invoiceNo>\n" +
//                "         <tem:revertURL>" + callbackURL + "</tem:revertURL>\n" +
//                "      </tem:MakeToken>\n" +
//                "   </soapenv:Body>\n" +
//                "</soapenv:Envelope>";
//
//
//        String urlString = baseUrl;
//        try {
//            URL urlForInfWebSvc = new URL(urlString);
//            URLConnection UrlConnInfWebSvc = urlForInfWebSvc.openConnection();
//            HttpsURLConnection httpUrlConnInfWebSvc = (HttpsURLConnection) UrlConnInfWebSvc;
//            httpUrlConnInfWebSvc.setDoOutput(true);
//            httpUrlConnInfWebSvc.setDoInput(true);
//            httpUrlConnInfWebSvc.setAllowUserInteraction(true);
//            httpUrlConnInfWebSvc.setRequestMethod("POST");
//            httpUrlConnInfWebSvc.setRequestProperty("Host", "ikc.shaparak.ir");
//            httpUrlConnInfWebSvc.setRequestProperty("SOAPAction", SOAPAction);
//            httpUrlConnInfWebSvc.setRequestProperty("Content-Type", "text/xml;charset=UTF-8");
//            OutputStreamWriter infWebSvcReqWriter = new OutputStreamWriter(httpUrlConnInfWebSvc.getOutputStream());
//            infWebSvcReqWriter.write(infWebSvcRequestMessage);
//            infWebSvcReqWriter.flush();
//            BufferedReader infWebSvcReplyReader = new BufferedReader(new InputStreamReader(httpUrlConnInfWebSvc.getInputStream()));
//            String line;
//            String infWebSvcReplyString = "";
//            while ((line = infWebSvcReplyReader.readLine()) != null) {
//                infWebSvcReplyString = infWebSvcReplyString.concat(line);
//            }
//            log.info(logCode + " MakeToken 0 : " + " ,transactionID: " + objectId.toString() + " ,response: " + infWebSvcReplyString);
//            MessageFactory factory = MessageFactory.newInstance();
//            SOAPMessage soapMessage = factory.createMessage(new MimeHeaders(), new ByteArrayInputStream(infWebSvcReplyString.getBytes(Charset.forName("UTF-8"))));
//            SOAPBody soapBody = soapMessage.getSOAPBody();
//            NodeList nodes1 = soapBody.getElementsByTagName("a:message");
//            String someMsgContent = "-1";
//            Node node = (Node) nodes1.item(0);
//            someMsgContent = node != null ? node.getTextContent() : "";
//            res.put(PaymentRepository.Fields.STATUSHOLDER.getValue(), someMsgContent);
//
//            nodes1 = soapBody.getElementsByTagName("a:result");
//            String someMsgContent1 = "-1";
//            node = (Node) nodes1.item(0);
//            someMsgContent1 = node != null ? node.getTextContent() : "";
//            String someMsgContent3 = "-1";
//            if (someMsgContent.equalsIgnoreCase("Success") &&
//                    someMsgContent1.equalsIgnoreCase("true")) {
//                nodes1 = soapBody.getElementsByTagName("a:token");
//                someMsgContent3 = "";
//                node = (Node) nodes1.item(0);
//                someMsgContent3 = node != null ? node.getTextContent() : "";
//                res.put(PaymentRepository.Fields.AUTHORITY.getValue(), someMsgContent3);
//            }
//
//            infWebSvcReqWriter.close();
//            infWebSvcReplyReader.close();
//            httpUrlConnInfWebSvc.disconnect();
//            log.info(logCode + " MakeToken 1 : " + " ,transactionID: " + objectId.toString() + " ,message: " + someMsgContent +
//                    " , result: " + someMsgContent1 + " , token: " + someMsgContent3);
//            return res;
//        } catch (SOAPException soex) {
//            String message = logCode + " MakeToken 2 : " + " ,transactionID: " + objectId.toString() + " , " + soex.toString();
//            log.error(message);
//            return null;
//        } catch (IOException ioex) {
//            String message = logCode + " MakeToken 3 : " + " ,transactionID: " + objectId.toString() + " , " + ioex.toString();
//            log.error(message);
//            return null;
//        }
//    }
//
//    public static Map<String, String> kicccPaymentsVerification(String tokenId, String merchantId,
//                                                                String referenceNumber, String payurl, ObjectId objectId) {
//        Map<String, String> res = new ConcurrentHashMap<>();
//        String SOAPAction = "http://tempuri.org/IVerify/KicccPaymentsVerification";
//        String infWebSvcRequestMessage = "" +
//                "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:tem=\"http://tempuri.org/\">\n" +
//                "   <soapenv:Header/>\n" +
//                "   <soapenv:Body>\n" +
//                "      <tem:KicccPaymentsVerification>\n" +
//                "         <tem:token>" + tokenId + "</tem:token>\n" +
//                "         <tem:merchantId>" + merchantId + "</tem:merchantId>\n" +
//                "         <tem:referenceNumber>" + referenceNumber + "</tem:referenceNumber>\n" +
//                "         <tem:sha1Key>?</tem:sha1Key>\n" +
//                "      </tem:KicccPaymentsVerification>\n" +
//                "   </soapenv:Body>\n" +
//                "</soapenv:Envelope>";
//        try {
//            String urlString = payurl;
//            URL urlForInfWebSvc = new URL(urlString);
//            URLConnection UrlConnInfWebSvc = urlForInfWebSvc.openConnection();
//            HttpsURLConnection httpUrlConnInfWebSvc = (HttpsURLConnection) UrlConnInfWebSvc;
//            httpUrlConnInfWebSvc.setDoOutput(true);
//            httpUrlConnInfWebSvc.setDoInput(true);
//            httpUrlConnInfWebSvc.setAllowUserInteraction(true);
//            httpUrlConnInfWebSvc.setRequestMethod("POST");
//            httpUrlConnInfWebSvc.setRequestProperty("Host", "ikc.shaparak.ir");
//            httpUrlConnInfWebSvc.setRequestProperty("SOAPAction", SOAPAction);
//            httpUrlConnInfWebSvc.setRequestProperty("Content-Type", "text/xml;charset=UTF-8");
//            OutputStreamWriter infWebSvcReqWriter = new OutputStreamWriter(httpUrlConnInfWebSvc.getOutputStream());
//            infWebSvcReqWriter.write(infWebSvcRequestMessage);
//            infWebSvcReqWriter.flush();
//            BufferedReader infWebSvcReplyReader = new BufferedReader
//                    (new InputStreamReader(httpUrlConnInfWebSvc.getInputStream()));
//            String line;
//            String infWebSvcReplyString = "";
//            while ((line = infWebSvcReplyReader.readLine()) != null) {
//                infWebSvcReplyString = infWebSvcReplyString.concat(line);
//            }
//            log.info(logCode + " kicccPaymentsVerification 0 : " + " ,transactionID: " + objectId.toString() + " ,response: " + infWebSvcReplyString);
//            MessageFactory factory = MessageFactory.newInstance();
//            SOAPMessage soapMessage = factory.createMessage(new MimeHeaders(), new ByteArrayInputStream
//                    (infWebSvcReplyString.getBytes(Charset.forName("UTF-8"))));
//            SOAPBody soapBody = soapMessage.getSOAPBody();
//            NodeList nodes1 = soapBody.getElementsByTagName("KicccPaymentsVerificationResult");
//            String someMsgContent = "";
//            Node node = (Node) nodes1.item(0);
//            someMsgContent = node != null ? node.getTextContent() : "";
//            res.put(PaymentRepository.Fields.STATUSHOLDER.getValue(), someMsgContent);
//
//            infWebSvcReqWriter.close();
//            infWebSvcReplyReader.close();
//            httpUrlConnInfWebSvc.disconnect();
//            log.info(logCode + " kicccPaymentsVerification 1 : " + " ,transactionID: " + objectId.toString() +
//                    " ,KicccPaymentsVerificationResult: " + someMsgContent);
//            return res;
//        } catch (SOAPException soex) {
//            String message = logCode + " kicccPaymentsVerification 2 : " + " ,transactionID: " +
//                    objectId.toString() + " , " + soex.toString();
//            log.error(message);
//            return null;
//        } catch (IOException ioex) {
//            String message = logCode + " kicccPaymentsVerification 3 : " + " ,transactionID: " +
//                    objectId.toString() + " , " + ioex.toString();
//            log.error(message);
//            return null;
//        }
//    }
//
//
//    public static IReturn_Status_Codes answerByTejarat(String statusHolder) {
//        IReturn_Status_Codes return_status_codes = null;
//        switch (Integer.valueOf(statusHolder)) {
//            case 110:
//                return_status_codes = Return_Status_Codes_Payment.PAYMENT_ERROR_TEJARAT_USER_CANCEL_110;
//                return return_status_codes;
//            case 120:
//                return_status_codes = Return_Status_Codes_Payment.PAYMENT_ERROR_TEJARAT_NO_ENOGH_MONEY_120;
//                return return_status_codes;
//            case 121:
//                return_status_codes = Return_Status_Codes_Payment.PAYMENT_ERROR_TEJARAT_LIMIT_MONEY_121;
//                return return_status_codes;
//            case 130:
//                return_status_codes = Return_Status_Codes_Payment.PAYMENT_ERROR_TEJARAT_INVALID_CARD_INFORMATION_130;
//                return return_status_codes;
//            case 131:
//                return_status_codes = Return_Status_Codes_Payment.PAYMENT_ERROR_TEJARAT_INVALID_CARD_PASSWORD_131;
//                return return_status_codes;
//            case 132:
//                return_status_codes = Return_Status_Codes_Payment.PAYMENT_ERROR_TEJARAT_CARD_BLOCKED_132;
//                return return_status_codes;
//            case 133:
//                return_status_codes = Return_Status_Codes_Payment.PAYMENT_ERROR_TEJARAT_CARD_TIMEOUT_133;
//                return return_status_codes;
//            case 140:
//                return_status_codes = Return_Status_Codes_Payment.PAYMENT_ERROR_TEJARAT_TIMEOUT_140;
//                return return_status_codes;
//            case 150:
//                return_status_codes = Return_Status_Codes_Payment.PAYMENT_ERROR_TEJARAT_BANK_INTERNAL_ERROR_150;
//                return return_status_codes;
//            case 160:
//                return_status_codes = Return_Status_Codes_Payment.PAYMENT_ERROR_TEJARAT_INVALID_CVV2_160;
//                return return_status_codes;
//            case 166:
//                return_status_codes = Return_Status_Codes_Payment.PAYMENT_ERROR_TEJARAT_NO_PERMIT_FROM_BANK_166;
//                return return_status_codes;
//            case 167:
//                return_status_codes = Return_Status_Codes_Payment.PAYMENT_ERROR_TEJARAT_INVALID_MONEY_167;
//                return return_status_codes;
//            case 200:
//                return_status_codes = Return_Status_Codes_Payment.PAYMENT_ERROR_TEJARAT_LIMIT_MONEY_200;
//                return return_status_codes;
//            case 201:
//                return_status_codes = Return_Status_Codes_Payment.PAYMENT_ERROR_TEJARAT_LIMIT_MONEY_WORKDAY_201;
//                return return_status_codes;
//            case 202:
//                return_status_codes = Return_Status_Codes_Payment.PAYMENT_ERROR_TEJARAT_LIMIT_MONEY_WORKMONTH_202;
//                return return_status_codes;
//            case 203:
//                return_status_codes = Return_Status_Codes_Payment.PAYMENT_ERROR_TEJARAT_TOO_MANY_TIMES_TRANSACTION_203;
//                return return_status_codes;
//            case 499:
//                return_status_codes = Return_Status_Codes_Payment.PAYMENT_ERROR_TEJARAT_SYSTEM_ERROR_499;
//                return return_status_codes;
//            case 500:
//                return_status_codes = Return_Status_Codes_Payment.PAYMENT_ERROR_TEJARAT_VERIFICATION_TRANSACTION_500;
//                return return_status_codes;
//            case 501:
//                return_status_codes = Return_Status_Codes_Payment.PAYMENT_ERROR_TEJARAT_AUTOMATIC_VERIFICATION_TRANSACTION_501;
//                return return_status_codes;
//            case 502:
//                return_status_codes = Return_Status_Codes_Payment.PAYMENT_ERROR_TEJARAT_INAVLID_IP_ADDRESS_502;
//                return return_status_codes;
//            case 503:
//                return_status_codes = Return_Status_Codes_Payment.PAYMENT_ERROR_TEJARAT_TESTSTATE_RECIVER_503;
//                return return_status_codes;
//            case 504:
//                return_status_codes = Return_Status_Codes_Payment.PAYMENT_ERROR_TEJARAT_PAYID_ALGORITHM_504;
//                return return_status_codes;
//            case 505:
//                return_status_codes = Return_Status_Codes_Payment.PAYMENT_ERROR_TEJARAT_VERIFICATION_TIMEOUT_505;
//                return return_status_codes;
//            case 506:
//                return_status_codes = Return_Status_Codes_Payment.PAYMENT_ERROR_TEJARAT_NOT_FOUND_RECIVER_506;
//                return return_status_codes;
//            case 507:
//                return_status_codes = Return_Status_Codes_Payment.PAYMENT_ERROR_TEJARAT_INVALID_TOKEN_507;
//                return return_status_codes;
//            case 508:
//                return_status_codes = Return_Status_Codes_Payment.PAYMENT_ERROR_TEJARAT_NOT_FOUND_TOKEN_508;
//                return return_status_codes;
//            case 509:
//                return_status_codes = Return_Status_Codes_Payment.PAYMENT_ERROR_TEJARAT_INVALID_PAY_PARAMEETR_509;
//                return return_status_codes;
//            case 510:
//                return_status_codes = Return_Status_Codes_Payment.PAYMENT_ERROR_TEJARAT_REPEAT_OR_NOT_MATCH_MONEY_510;
//                return return_status_codes;
//            case 511:
//                return_status_codes = Return_Status_Codes_Payment.PAYMENT_ERROR_TEJARAT_BLOCKED_ACCOUNT_511;
//                return return_status_codes;
//            case 512:
//                return_status_codes = Return_Status_Codes_Payment.PAYMENT_ERROR_TEJARAT_NOT_DEFINITION_ACCOUNT_512;
//                return return_status_codes;
//            case 513:
//                return_status_codes = Return_Status_Codes_Payment.PAYMENT_ERROR_TEJARAT_REPEATED_TRANSATION_ID_513;
//                return return_status_codes;
//            case -20:
//                return_status_codes = Return_Status_Codes_Payment.PAYMENT_ERROR_TEJARAT_VERIFICATION_INVALID_CHARACTER_20_;
//                return return_status_codes;
//            case -30:
//                return_status_codes = Return_Status_Codes_Payment.PAYMENT_ERROR_TEJARAT_VERIFICATION_REPEATED_BACK_TRANSACTION_30_;
//                return return_status_codes;
//            case -50:
//                return_status_codes = Return_Status_Codes_Payment.PAYMENT_ERROR_TEJARAT_VERIFICATION_INVALID_REQUEST_LENGHT_50_;
//                return return_status_codes;
//            case -51:
//                return_status_codes = Return_Status_Codes_Payment.PAYMENT_ERROR_TEJARAT_VERIFICATION_REWUEST_ERRROR_51_;
//                return return_status_codes;
//            case -80:
//                return_status_codes = Return_Status_Codes_Payment.PAYMENT_ERROR_TEJARAT_VERIFICATION_NOT_FOUND_TRANSATION_80_;
//                return return_status_codes;
//            case -81:
//                return_status_codes = Return_Status_Codes_Payment.PAYMENT_ERROR_TEJARAT_VERIFICATION_INTERNAL_ERROR_81_;
//                return return_status_codes;
//            case -90:
//                return_status_codes = Return_Status_Codes_Payment.PAYMENT_ERROR_TEJARAT_VERIFICATION_REPEATED_VERIFY_TRANSACTION_90_;
//                return return_status_codes;
//            default:
//                return_status_codes = Return_Status_Codes_Payment.PAYMENT_ERROR_PAYMENT_OTHER_ERROR;
//                break;
//        }
//        return_status_codes = Return_Status_Codes_Payment.PAYMENT_ERROR_REQUEST_FAILED;
//        return return_status_codes;
//    }
//}

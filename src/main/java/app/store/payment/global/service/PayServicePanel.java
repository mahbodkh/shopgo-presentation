//package app.store.payment.global.service;
//
//import com.vasl.vaslapp.modules.bankgateway.global.model.BankAddModel;
//import com.vasl.vaslapp.modules.bankgateway.global.model.BankListModel;
//import com.vasl.vaslapp.modules.bankgateway.global.model.BankUpdateModel;
//import com.vasl.vaslapp.modules.bankgateway.global.proto.holder.GateWay;
//import org.springframework.stereotype.Service;
//
//@Service
//public interface PayServicePanel {
//    GateWay.Bank.Builder addBank(BankAddModel bankAddModel);
//
//    GateWay.Bank.Builder editBank(BankUpdateModel bankUpdateModel);
//
//    GateWay.BankList.Builder listBank(BankListModel bankListModel);
//
//}

package app.store.payment.global.service.soap;

import app.store.persistance.PaymentRepository;
import org.bson.types.ObjectId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.w3c.dom.NodeList;

import javax.net.ssl.HttpsURLConnection;
import javax.xml.soap.*;
import java.io.*;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.Charset;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Service
public class Mellat {

    private static String logCode = "MellatClass";
    private static Logger log = LoggerFactory.getLogger(Mellat.class);

    public static Map<String, String> bpPayRequest(String terminalId, String username
            , String userPassword, String orderId, Integer amount, String callbackURL, String payurl, ObjectId objectId) {

        //amount=amount/10;       //tomans
        Map<String, String> res = new ConcurrentHashMap<>();
        String SOAPAction = "";
        String infWebSvcRequestMessage = "" +
                "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:int=\"http://interfaces.core.sw.bps.com/\">\n" +
                "   <soapenv:Header/>\n" +
                "   <soapenv:Body>\n" +
                "      <int:bpPayRequest>\n" +
                "         <terminalId>" + terminalId + "</terminalId>\n" +
                "         <userName>" + username + "</userName>\n" +
                "         <userPassword>" + userPassword + "</userPassword>\n" +
                "         <orderId>" + orderId + "</orderId>\n" +
                "         <amount>" + amount + "</amount>\n" +
                "         <localDate>" + getDateMellat() + "</localDate>\n" +
                "         <localTime>" + getTimeMellat() + "</localTime>\n" +
                "         <additionalData></additionalData>\n" +
                "         <callBackUrl>" + callbackURL + "</callBackUrl>\n" +
                "         <payerId>0</payerId>\n" +
                "      </int:bpPayRequest>\n" +
                "   </soapenv:Body>\n" +
                "</soapenv:Envelope>";


        String urlString = payurl;
        try {
            URL urlForInfWebSvc = new URL(urlString);
            URLConnection UrlConnInfWebSvc = urlForInfWebSvc.openConnection();
            HttpsURLConnection httpUrlConnInfWebSvc = (HttpsURLConnection) UrlConnInfWebSvc;
            httpUrlConnInfWebSvc.setDoOutput(true);
            httpUrlConnInfWebSvc.setDoInput(true);
            httpUrlConnInfWebSvc.setAllowUserInteraction(true);
            httpUrlConnInfWebSvc.setRequestMethod("POST");
            httpUrlConnInfWebSvc.setRequestProperty("Host", "bpm.shaparak.ir");
            httpUrlConnInfWebSvc.setRequestProperty("SOAPAction", SOAPAction);
            httpUrlConnInfWebSvc.setRequestProperty("Content-Type", "text/xml;charset=UTF-8");
            OutputStreamWriter infWebSvcReqWriter = new OutputStreamWriter(httpUrlConnInfWebSvc.getOutputStream());
            infWebSvcReqWriter.write(infWebSvcRequestMessage);
            infWebSvcReqWriter.flush();
            BufferedReader infWebSvcReplyReader = new BufferedReader(new InputStreamReader(httpUrlConnInfWebSvc.getInputStream()));
            String line;
            String infWebSvcReplyString = "";
            while ((line = infWebSvcReplyReader.readLine()) != null) {
                infWebSvcReplyString = infWebSvcReplyString.concat(line);
            }
            log.info(logCode + " bpPayRequest 0 : " + " ,transactionID: " + objectId.toString() + " ,response: " + infWebSvcReplyString);
            MessageFactory factory = MessageFactory.newInstance();
            SOAPMessage soapMessage = factory.createMessage(new MimeHeaders(), new ByteArrayInputStream(infWebSvcReplyString.getBytes(Charset.forName("UTF-8"))));
            SOAPBody soapBody = soapMessage.getSOAPBody();
            NodeList nodes1 = soapBody.getElementsByTagName("return");
            String someMsgContent = "";//0,85C19A363D73D496
            Node node = (Node) nodes1.item(0);
            someMsgContent = node != null ? node.getTextContent() : "";
            infWebSvcReqWriter.close();
            infWebSvcReplyReader.close();
            httpUrlConnInfWebSvc.disconnect();
            log.info(logCode + " bpPayRequest 1 : " + " ,transactionID: " + objectId.toString() + " ,Status AND Refid: " + someMsgContent);
            return resultMellat(someMsgContent);
        } catch (SOAPException soex) {
            String message = logCode + " bpPayRequest 2 : " + " ,transactionID: " + objectId.toString() + " , " + soex.toString();
            log.error(message);
            return null;
        } catch (IOException ioex) {
            String message = logCode + " bpPayRequest 3 : " + " ,transactionID: " + objectId.toString() + " , " + ioex.toString();
            log.error(message);
            return null;
        }
    }

    private static Map<String, String> resultMellat(String someMsgContent) {
        Map<String, String> res = new ConcurrentHashMap<>();
        String basestring = someMsgContent;
        int i = -1;
        i = basestring.indexOf(",");
        if (i == -1) {
            res.put(PaymentRepository.Fields.STATUSHOLDER.getValue(), someMsgContent);
        } else {
            String status = basestring.substring(0, i);
            basestring = basestring.substring(i + 1);
            res.put(PaymentRepository.Fields.STATUSHOLDER.getValue(), status);
            res.put(PaymentRepository.Fields.REFIDHOLDER.getValue(), basestring);
        }
        return res;
    }

    public static Map<String, String> bpVerifyRequest(String terminalId, String username, String password,
                                                      String orderId, String saleOrderId, String saleReferenceId,
                                                      String payurl, ObjectId objectId) {
        Map<String, String> res = new ConcurrentHashMap<>();
        String SOAPAction = "";
        String infWebSvcRequestMessage = "" +
                "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:int=\"http://interfaces.core.sw.bps.com/\">\n" +
                "   <soapenv:Header/>\n" +
                "   <soapenv:Body>\n" +
                "      <int:bpVerifyRequest>\n" +
                "         <terminalId>" + terminalId + "</terminalId>\n" +
                "         <userName>" + username + "</userName>\n" +
                "         <userPassword>" + password + "</userPassword>\n" +
                "         <orderId>" + orderId + "</orderId>\n" +
                "         <saleOrderId>" + saleOrderId + "</saleOrderId>\n" +
                "         <saleReferenceId>" + saleReferenceId + "</saleReferenceId>\n" +
                "      </int:bpVerifyRequest>\n" +
                "   </soapenv:Body>\n" +
                "</soapenv:Envelope>";


        String urlString = payurl;
        try {
            URL urlForInfWebSvc = new URL(urlString);
            URLConnection UrlConnInfWebSvc = urlForInfWebSvc.openConnection();
            HttpsURLConnection httpUrlConnInfWebSvc = (HttpsURLConnection) UrlConnInfWebSvc;
            httpUrlConnInfWebSvc.setDoOutput(true);
            httpUrlConnInfWebSvc.setDoInput(true);
            httpUrlConnInfWebSvc.setAllowUserInteraction(true);
            httpUrlConnInfWebSvc.setRequestMethod("POST");
            httpUrlConnInfWebSvc.setRequestProperty("Host", "bpm.shaparak.ir");
            httpUrlConnInfWebSvc.setRequestProperty("SOAPAction", SOAPAction);
            httpUrlConnInfWebSvc.setRequestProperty("Content-Type", "text/xml;charset=UTF-8");
            OutputStreamWriter infWebSvcReqWriter = new OutputStreamWriter(httpUrlConnInfWebSvc.getOutputStream());
            infWebSvcReqWriter.write(infWebSvcRequestMessage);
            infWebSvcReqWriter.flush();
            BufferedReader infWebSvcReplyReader = new BufferedReader(new InputStreamReader(httpUrlConnInfWebSvc.getInputStream()));
            String line;
            String infWebSvcReplyString = "";
            while ((line = infWebSvcReplyReader.readLine()) != null) {
                infWebSvcReplyString = infWebSvcReplyString.concat(line);
            }
            log.info(logCode + " MakeToken 0 : " + " ,transactionID: " + objectId.toString() + " ,response: " + infWebSvcReplyString);
            MessageFactory factory = MessageFactory.newInstance();
            SOAPMessage soapMessage = factory.createMessage(new MimeHeaders(), new ByteArrayInputStream(infWebSvcReplyString.getBytes(Charset.forName("UTF-8"))));
            SOAPBody soapBody = soapMessage.getSOAPBody();
            NodeList nodes1 = soapBody.getElementsByTagName("return");
            String someMsgContent = "";
            Node node = (Node) nodes1.item(0);
            someMsgContent = node != null ? node.getTextContent() : "";
            res.put(PaymentRepository.Fields.STATUSHOLDER.getValue(), someMsgContent);

            infWebSvcReqWriter.close();
            infWebSvcReplyReader.close();
            httpUrlConnInfWebSvc.disconnect();
            log.info(logCode + " bpVerifyRequest 1 : " + " ,transactionID: " + objectId.toString() + " ,status: " + someMsgContent);
            return res;
        } catch (SOAPException soex) {
            String message = logCode + " bpVerifyRequest 2 : " + " ,transactionID: " + objectId.toString() + " , " + soex.toString();
            log.error(message);
            return null;
        } catch (IOException ioex) {
            String message = logCode + " bpVerifyRequest 3 : " + " ,transactionID: " + objectId.toString() + " , " + ioex.toString();
            log.error(message);
            return null;
        }
    }

    private static String getDateMellat() {
        Date date = new Date();
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        return dateFormat.format(date).replace("-", "");
    }

    private static String getTimeMellat() {
        Date date = new Date();
        DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
        return dateFormat.format(date).replace(":", "");
    }

//    public static IReturn_Status_Codes answerByMellats(String statusHolder) {
//        IReturn_Status_Codes return_status_codes = null;
//        switch (Integer.valueOf(statusHolder)) {
//            case 11:
//                return_status_codes = Return_Status_Codes_Payment.PAYMENT_ERROR_MELLAT_CARD_NUMBER_INVALID_11;
//                return return_status_codes;
//            case 12:
//                return_status_codes = Return_Status_Codes_Payment.PAYMENT_ERROR_MELLAT_NOT_ENOGH_MONEY_12;
//                return return_status_codes;
//            case 13:
//                return_status_codes = Return_Status_Codes_Payment.PAYMENT_ERROR_MELLAT_PASSWORD_INVALID_13;
//                return return_status_codes;
//            case 14:
//                return_status_codes = Return_Status_Codes_Payment.PAYMENT_ERROR_MELLAT_PASSWORD_TOO_MANY_TIMES_14;
//                return return_status_codes;
//            case 15:
//                return_status_codes = Return_Status_Codes_Payment.PAYMENT_ERROR_MELLAT_CARD_INVALID_15;
//                return return_status_codes;
//            case 16:
//                return_status_codes = Return_Status_Codes_Payment.PAYMENT_ERROR_MELLAT_GET_MONEY_TOO_MANY_TIMES_16;
//                return return_status_codes;
//            case 17:
//                return_status_codes = Return_Status_Codes_Payment.PAYMENT_ERROR_MELLAT_USER_CANCEL_TTRANSACTION_17;
//                return return_status_codes;
//            case 18:
//                return_status_codes = Return_Status_Codes_Payment.PAYMENT_ERROR_MELLAT_EXPIRED_CARD_DATE_18;
//                return return_status_codes;
//            case 19:
//                return_status_codes = Return_Status_Codes_Payment.PAYMENT_ERROR_MELLAT_LIMIT_MONEY_19;
//                return return_status_codes;
//            case 111:
//                return_status_codes = Return_Status_Codes_Payment.PAYMENT_ERROR_MELLAT_CARD_EXPORTER_INVALID_111;
//                return return_status_codes;
//            case 112:
//                return_status_codes = Return_Status_Codes_Payment.PAYMENT_ERROR_MELLAT_SWITCH_CARD_EXPORTER_112;
//                return return_status_codes;
//            case 113:
//                return_status_codes = Return_Status_Codes_Payment.PAYMENT_ERROR_MELLAT_CARD_EXPORTER_NO_ANSWER_113;
//                return return_status_codes;
//            case 114:
//                return_status_codes = Return_Status_Codes_Payment.PAYMENT_ERROR_MELLAT_USER_ACCESS_114;
//                return return_status_codes;
//            case 21:
//                return_status_codes = Return_Status_Codes_Payment.PAYMENT_ERROR_MELLAT_RECEIVER_INVALID_21;
//                return return_status_codes;
//            case 23:
//                return_status_codes = Return_Status_Codes_Payment.PAYMENT_ERROR_MELLAT_SECURITY_23;
//                return return_status_codes;
//            case 24:
//                return_status_codes = Return_Status_Codes_Payment.PAYMENT_ERROR_MELLAT_RECEIVER_INFORMATION_INVALID_24;
//                return return_status_codes;
//            case 25:
//                return_status_codes = Return_Status_Codes_Payment.PAYMENT_ERROR_MELLAT_MONEY_INAVALID_25;
//                return return_status_codes;
//            case 31:
//                return_status_codes = Return_Status_Codes_Payment.PAYMENT_ERROR_MELLAT_ANSWER_INVALID_31;
//                return return_status_codes;
//            case 32:
//                return_status_codes = Return_Status_Codes_Payment.PAYMENT_ERROR_MELLAT_PARAMETER_FORMAT_INVALID_32;
//                return return_status_codes;
//            case 33:
//                return_status_codes = Return_Status_Codes_Payment.PAYMENT_ERROR_MELLAT_ACCOUNT_INVALID_33;
//                return return_status_codes;
//            case 34:
//                return_status_codes = Return_Status_Codes_Payment.PAYMENT_ERROR_MELLAT_SYSTEM_ERROR_34;
//                return return_status_codes;
//            case 35:
//                return_status_codes = Return_Status_Codes_Payment.PAYMENT_ERROR_MELLAT_DATE_INVALID_35;
//                return return_status_codes;
//            case 41:
//                return_status_codes = Return_Status_Codes_Payment.PAYMENT_ERROR_MELLAT_REQUEST_NUMBER_REPETED_41;
//                return return_status_codes;
//            case 42:
//                return_status_codes = Return_Status_Codes_Payment.PAYMENT_ERROR_MELLAT_SALE_TRANSACTION_NOT_EXIST_42;
//                return return_status_codes;
//            case 43:
//                return_status_codes = Return_Status_Codes_Payment.PAYMENT_ERROR_MELLAT_VERIFY_REQUEST_REPETED_43;
//                return return_status_codes;
//            case 44:
//                return_status_codes = Return_Status_Codes_Payment.PAYMENT_ERROR_MELLAT_VERIFY_REQUEST_NOT_FOUND_44;
//                return return_status_codes;
//            case 45:
//                return_status_codes = Return_Status_Codes_Payment.PAYMENT_ERROR_MELLAT_TRANSACTION_SETTLE_45;
//                return return_status_codes;
//            case 46:
//                return_status_codes = Return_Status_Codes_Payment.PAYMENT_ERROR_MELLAT_TRANSACTION_NOT_SETTLE_46;
//                return return_status_codes;
//            case 47:
//                return_status_codes = Return_Status_Codes_Payment.PAYMENT_ERROR_MELLAT_TRANSACTION_SETTLE_NOT_FOUND_47;
//                return return_status_codes;
//            case 48:
//                return_status_codes = Return_Status_Codes_Payment.PAYMENT_ERROR_MELLAT_RECEIVE_TRANSACTION_48;
//                return return_status_codes;
//            case 49:
//                return_status_codes = Return_Status_Codes_Payment.PAYMENT_ERROR_MELLAT_REFUND_TRANSACTION_NOT_FOUND_49;
//                return return_status_codes;
//            case 412:
//                return_status_codes = Return_Status_Codes_Payment.PAYMENT_ERROR_MELLAT_BILLID_INVALID_412;
//                return return_status_codes;
//            case 413:
//                return_status_codes = Return_Status_Codes_Payment.PAYMENT_ERROR_MELLAT_PAYMENTID_INVALID_413;
//                return return_status_codes;
//            case 414:
//                return_status_codes = Return_Status_Codes_Payment.PAYMENT_ERROR_MELLAT_BILL_EXPORTER_INVALID_414;
//                return return_status_codes;
//            case 415:
//                return_status_codes = Return_Status_Codes_Payment.PAYMENT_ERROR_MELLAT_SESSION_EXPIRED_415;
//                return return_status_codes;
//            case 416:
//                return_status_codes = Return_Status_Codes_Payment.PAYMENT_ERROR_MELLAT_INFORMATION_REGISTEER_ERROR_416;
//                return return_status_codes;
//            case 417:
//                return_status_codes = Return_Status_Codes_Payment.PAYMENT_ERROR_MELLAT_PAYERID_INVALID_417;
//                return return_status_codes;
//            case 418:
//                return_status_codes = Return_Status_Codes_Payment.PAYMENT_ERROR_MELLAT_USER_INFORMATION_DEFINE_ERROR_418;
//                return return_status_codes;
//            case 419:
//                return_status_codes = Return_Status_Codes_Payment.PAYMENT_ERROR_MELLAT_ENTER_INFORMATION_TOO_MANY_TIMES_419;
//                return return_status_codes;
//            case 421:
//                return_status_codes = Return_Status_Codes_Payment.PAYMENT_ERROR_MELLAT_IP_INVALID_421;
//                return return_status_codes;
//            case 51:
//                return_status_codes = Return_Status_Codes_Payment.PAYMENT_ERROR_MELLAT_TRANSACTION_REPETED_51;
//                return return_status_codes;
//            case 54:
//                return_status_codes = Return_Status_Codes_Payment.PAYMENT_ERROR_MELLAT_TRANSACTION_NOT_FOUND_54;
//                return return_status_codes;
//            case 55:
//                return_status_codes = Return_Status_Codes_Payment.PAYMENT_ERROR_MELLAT_TRANSATION_INVALID_55;
//                return return_status_codes;
//            case 61:
//                return_status_codes = Return_Status_Codes_Payment.PAYMENT_ERROR_MELLAT_SETTLEMENT_ERROR_61;
//                return return_status_codes;
//            default:
//                return_status_codes = Return_Status_Codes_Payment.PAYMENT_ERROR_PAYMENT_OTHER_ERROR;
//                break;
//        }
//        return_status_codes = Return_Status_Codes_Payment.PAYMENT_ERROR_REQUEST_FAILED;
//        return return_status_codes;
//    }


}

//package app.store.payment.global.service;
//
//
//import com.vasl.vaslapp.common.global.enums.Return_Status_Codes_Common;
//import com.vasl.vaslapp.common.global.interfaces.IReturn_Status_Codes;
//import com.vasl.vaslapp.modules.bankgateway.global.general.General;
//import com.vasl.vaslapp.modules.bankgateway.global.model.BankAddModel;
//import com.vasl.vaslapp.modules.bankgateway.global.model.BankListModel;
//import com.vasl.vaslapp.modules.bankgateway.global.model.BankUpdateModel;
//import com.vasl.vaslapp.modules.bankgateway.global.mongo.repositories.acm_app.GatewayConfig;
//import com.vasl.vaslapp.modules.bankgateway.global.proto.holder.GateWay;
//import org.bson.Document;
//import org.springframework.stereotype.Component;
//
//import java.util.List;
//
//@Component
//public class PayServicePanelImpl implements PayServicePanel {
//    private long now = System.currentTimeMillis();
//
//    @Override
//    public GateWay.Bank.Builder addBank(BankAddModel bankAddModel) {
//        GateWay.Bank.Builder ret = GateWay.Bank.newBuilder();
//        IReturn_Status_Codes return_status_codes = bankAddModel.getReturn_status_codes();
//        if (return_status_codes.getCode() != Return_Status_Codes_Common.ok_validform.getCode()) {
//            return bankAddModel.getOutput().returnResponseObject(ret, return_status_codes);
//        }
//
//        GatewayConfig pay = new GatewayConfig(bankAddModel.getOutput().getMongodb_app());
//        Document document = new Document();
//
//        if (bankAddModel.getCode() == 1)//zarinpall
//        {
//            document.put(GatewayConfig.Fields.CODE.getValue(), General.parseInt(bankAddModel.getCode()));
//            document.put(GatewayConfig.Fields.NAME.getValue(), General.parseString(bankAddModel.getName()));
//            document.put(GatewayConfig.Fields.GATEWAY_URL.getValue(), General.parseString(bankAddModel.getGatewayUrl()));
//            document.put(GatewayConfig.Fields.BASE_URL.getValue(), General.parseString(bankAddModel.getBaseUrl()));
//            document.put(GatewayConfig.Fields.PAYMENT_URL.getValue(), General.parseString(bankAddModel.getPaymentUrl()));
//            document.put(GatewayConfig.Fields.GATEWAY_URL_POSTFIX.getValue(), General.parseString(bankAddModel.getGatewayUrlPostfix()));
//            document.put(GatewayConfig.Fields.MERCHANT.getValue(), General.parseString(bankAddModel.getMerchant()));
//            document.put(GatewayConfig.Fields.ISACTIVE.getValue(), false);
//        } else if (bankAddModel.getCode() == 2) {//tejarat
//            document.put(GatewayConfig.Fields.CODE.getValue(), General.parseInt(bankAddModel.getCode()));
//            document.put(GatewayConfig.Fields.NAME.getValue(), General.parseString(bankAddModel.getName()));
//            document.put(GatewayConfig.Fields.GATEWAY_URL.getValue(), General.parseString(bankAddModel.getGatewayUrl()));
//            document.put(GatewayConfig.Fields.BASE_URL.getValue(), General.parseString(bankAddModel.getBaseUrl()));
//            document.put(GatewayConfig.Fields.PAYMENT_URL.getValue(), General.parseString(bankAddModel.getPaymentUrl()));
//            document.put(GatewayConfig.Fields.MERCHANT.getValue(), General.parseString(bankAddModel.getMerchant()));
//            document.put(GatewayConfig.Fields.ISACTIVE.getValue(), false);
//        } else if (bankAddModel.getCode() == 3)//melat
//        {
//            document.put(GatewayConfig.Fields.CODE.getValue(), General.parseInt(bankAddModel.getCode()));
//            document.put(GatewayConfig.Fields.GATEWAY_URL.getValue(), General.parseString(bankAddModel.getGatewayUrl()));
//            document.put(GatewayConfig.Fields.BASE_URL.getValue(), General.parseString(bankAddModel.getBaseUrl()));
//            document.put(GatewayConfig.Fields.PAYMENT_URL.getValue(), General.parseString(bankAddModel.getPaymentUrl()));
//            document.put(GatewayConfig.Fields.NAME.getValue(), General.parseString(bankAddModel.getName()));
//            document.put(GatewayConfig.Fields.TERMINALID.getValue(), General.parseString(bankAddModel.getTerminalId()));
//            document.put(GatewayConfig.Fields.USERNAME.getValue(), General.parseString(bankAddModel.getUserName()));
//            document.put(GatewayConfig.Fields.PASSWORD.getValue(), General.parseString(bankAddModel.getUserPassword()));
//            document.put(GatewayConfig.Fields.CALLBACKPOTFIX.getValue(), General.parseString(bankAddModel.getCallBackPostfix()));
//            document.put(GatewayConfig.Fields.ISACTIVE.getValue(), false);
//        }
//        pay.insertOne(document);
//
//        ret = bankAddModel.getOutput().returnResponseObject(ret, Return_Status_Codes_Common.SC_OK);
//
//        return ret;
//    }
//
//    @Override
//    public GateWay.Bank.Builder editBank(BankUpdateModel bankUpdateModel) {
//        GateWay.Bank.Builder ret = GateWay.Bank.newBuilder();
//        IReturn_Status_Codes return_status_codes = bankUpdateModel.getReturn_status_codes();
//        if (return_status_codes.getCode() != Return_Status_Codes_Common.ok_validform.getCode()) {
//            return bankUpdateModel.getOutput().returnResponseObject(ret, return_status_codes);
//        }
//        GatewayConfig bank = new GatewayConfig(bankUpdateModel.getOutput().getMongodb_app());
//
//        Document document = new Document();
//        if (bankUpdateModel.isIsactive()) document.put(GatewayConfig.Fields.ISACTIVE.getValue(), true);
//        else document.put(GatewayConfig.Fields.ISACTIVE.getValue(), false);
//
//        document.put(GatewayConfig.Fields.ISACTIVE.getValue(), General.parseBoolean(bankUpdateModel.isIsactive()));
//
//        if (bankUpdateModel.getBankObject().getInteger(GatewayConfig.Fields.CODE.getValue()) == 1)//zarinpall
//        {
//            if (bankUpdateModel.getBaseUrl() != null)
//                document.put(GatewayConfig.Fields.BASE_URL.getValue(), General.parseString(bankUpdateModel.getBaseUrl()));
//            if (bankUpdateModel.getGatewayUrlPostfix() != null)
//                document.put(GatewayConfig.Fields.GATEWAY_URL_POSTFIX.getValue(), General.parseString(bankUpdateModel.getGatewayUrlPostfix()));
//            if (bankUpdateModel.getGatewayUrl() != null)
//                document.put(GatewayConfig.Fields.GATEWAY_URL.getValue(), General.parseString(bankUpdateModel.getGatewayUrl()));
//            if (bankUpdateModel.getPaymentUrl() != null)
//                document.put(GatewayConfig.Fields.PAYMENT_URL.getValue(), General.parseString(bankUpdateModel.getPaymentUrl()));
//            if (bankUpdateModel.getMerchant() != null)
//                document.put(GatewayConfig.Fields.MERCHANT.getValue(), General.parseString(bankUpdateModel.getMerchant()));
//        } else if (bankUpdateModel.getBankObject().getInteger(GatewayConfig.Fields.CODE.getValue()) == 2)//tejarat
//        {
//            if (bankUpdateModel.getBaseUrl() != null)
//                document.put(GatewayConfig.Fields.BASE_URL.getValue(), General.parseString(bankUpdateModel.getBaseUrl()));
//            if (bankUpdateModel.getGatewayUrl() != null)
//                document.put(GatewayConfig.Fields.GATEWAY_URL.getValue(), General.parseString(bankUpdateModel.getGatewayUrl()));
//            if (bankUpdateModel.getPaymentUrl() != null)
//                document.put(GatewayConfig.Fields.PAYMENT_URL.getValue(), General.parseString(bankUpdateModel.getPaymentUrl()));
//            if (bankUpdateModel.getMerchant() != null)
//                document.put(GatewayConfig.Fields.MERCHANT.getValue(), General.parseString(bankUpdateModel.getMerchant()));
//        } else if (bankUpdateModel.getBankObject().getInteger(GatewayConfig.Fields.CODE.getValue()) == 3)//melat
//        {
//            if (bankUpdateModel.getBaseUrl() != null)
//                document.put(GatewayConfig.Fields.BASE_URL.getValue(), General.parseString(bankUpdateModel.getBaseUrl()));
//            if (bankUpdateModel.getGatewayUrl() != null)
//                document.put(GatewayConfig.Fields.GATEWAY_URL.getValue(), General.parseString(bankUpdateModel.getGatewayUrl()));
//            if (bankUpdateModel.getPaymentUrl() != null)
//                document.put(GatewayConfig.Fields.PAYMENT_URL.getValue(), General.parseString(bankUpdateModel.getPaymentUrl()));
//            if (bankUpdateModel.getTerminalId() != null)
//                document.put(GatewayConfig.Fields.TERMINALID.getValue(), General.parseString(bankUpdateModel.getTerminalId()));
//            if (bankUpdateModel.getUserName() != null)
//                document.put(GatewayConfig.Fields.USERNAME.getValue(), General.parseString(bankUpdateModel.getUserName()));
//            if (bankUpdateModel.getUserPassword() != null)
//                document.put(GatewayConfig.Fields.PASSWORD.getValue(), General.parseString(bankUpdateModel.getUserPassword()));
//            if (bankUpdateModel.getCallBackPostfix() != null)
//                document.put(GatewayConfig.Fields.CALLBACKPOTFIX.getValue(), General.parseString(bankUpdateModel.getCallBackPostfix()));
//        }
//        bank.updateBank(bankUpdateModel.getObjectId(), document);
//        ret = bankUpdateModel.getOutput().returnResponseObject(ret, Return_Status_Codes_Common.SC_OK);
//        return ret;
//    }
//
//    @Override
//    public GateWay.BankList.Builder listBank(BankListModel bankListModel) {
//        GateWay.BankList.Builder ret = GateWay.BankList.newBuilder();
//
//        IReturn_Status_Codes return_status_codes = bankListModel.getReturn_status_codes();
//        if (return_status_codes.getCode() != Return_Status_Codes_Common.ok_validform.getCode()) {
//            return bankListModel.getOutput().returnResponseObject(ret, return_status_codes);
//        }
//        GatewayConfig gatewayConfig = new GatewayConfig(bankListModel.getOutput().getMongodb_app());
//        List<Document> banks = gatewayConfig.findAll();
//        for (Document document : banks) {
//                GateWay.BankGet.Builder builder = gatewayConfig.getBankName(document);
//                ret.addData(builder);
//
//        }
//        return bankListModel.getOutput().returnResponseObject(ret, Return_Status_Codes_Common.SC_OK);
//    }
//
//}

//package app.store.payment.global.service;
//
//
//import com.vasl.vaslapp.common.global.enums.Return_Status_Codes_Common;
//import com.vasl.vaslapp.common.global.interfaces.IReturn_Status_Codes;
//import com.vasl.vaslapp.modules.bankgateway.global.enums.Bank;
//import com.vasl.vaslapp.modules.bankgateway.global.enums.Return_Status_Codes_Payment;
//import com.vasl.vaslapp.modules.bankgateway.global.enums.TransactionStatus;
//import com.vasl.vaslapp.modules.bankgateway.global.model.BankListModel;
//import com.vasl.vaslapp.modules.bankgateway.global.model.CallbackModel;
//import com.vasl.vaslapp.modules.bankgateway.global.model.ConfirmTransactionModel;
//import com.vasl.vaslapp.modules.bankgateway.global.model.PaymentModel;
//import com.vasl.vaslapp.modules.bankgateway.global.mongo.repositories.acm_app.GatewayConfig;
//import com.vasl.vaslapp.modules.bankgateway.global.mongo.repositories.acm_app.GlobalTransaction;
//import com.vasl.vaslapp.modules.bankgateway.global.proto.holder.GateWay;
//import com.vasl.vaslapp.modules.bankgateway.global.service.callBackUser.Api24;
//import com.vasl.vaslapp.modules.bankgateway.global.service.webServices.Zarinpal;
//import org.bson.Document;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Component;
//
//import java.util.List;
//import java.util.Map;
//import java.util.concurrent.ConcurrentHashMap;
//
//@Component
//public class PayServiceImpl implements PayService {
//
//    //log
//    public static String logCode = "PayServiceImplClass";
//    private static Logger log = LoggerFactory.getLogger(Zarinpal.class);
//    private long now = System.currentTimeMillis();
//    private GateWayServiceImpl gateWayService;
//
//    @Autowired
//    public PayServiceImpl(GateWayServiceImpl gateWayService) {
//        this.gateWayService = gateWayService;
//    }
//
//    public GateWay.Pay.Builder payment(PaymentModel paymentModel) {
//        GateWay.Pay.Builder ret = GateWay.Pay.newBuilder();
//
//        IReturn_Status_Codes return_status_codes = paymentModel.getReturn_status_codes();
//        if (return_status_codes.getCode() != Return_Status_Codes_Common.ok_validform.getCode()) {
//            return paymentModel.getOutput().returnResponseObject(ret, return_status_codes);
//        }
//        Map<String, String> getPay = new ConcurrentHashMap<>();
//        if (paymentModel.getBankCode() == Bank.ZARINPAL.getCode()) {
//            return gateWayService.payByZarinpal(paymentModel);
//        } else if (paymentModel.getBankCode() == Bank.TEJARAT.getCode()) {
//            return gateWayService.payByTejarat(paymentModel);
//        } else if (paymentModel.getBankCode() == Bank.MELLAT.getCode()) {
//            return gateWayService.payByMellat(paymentModel);
//        } else {
//        }
//        ret = paymentModel.getOutput().returnResponseObject(ret, return_status_codes);
//        return ret;
//    }
//
//    @Override
//    public GateWay.Callback.Builder getBankCallBack(CallbackModel callbackModel) {
//        GateWay.Callback.Builder ret = GateWay.Callback.newBuilder();
//        Document upDoc = new Document();
//        GlobalTransaction globalTransaction = new GlobalTransaction(callbackModel.getOutput().getMongodb_app(callbackModel.getAppid()));
//        IReturn_Status_Codes return_status_codes = callbackModel.getReturn_status_codes();
//        if ((TransactionStatus.PROCESSED_DONE.getValue().equalsIgnoreCase(callbackModel.getTransactionObject().getString
//                (GlobalTransaction.Fields.TRANSACTIONSTATUS.getValue())))
//                || (TransactionStatus.CHECKING.getValue().equalsIgnoreCase(callbackModel.getTransactionObject().getString
//                (GlobalTransaction.Fields.TRANSACTIONSTATUS.getValue())))) {
//            return_status_codes = Return_Status_Codes_Payment.PAYMENT_TRANSACTION_CHECKED_BEFORE;
//            log.info(logCode + " ,getBankCallBack 2 ,transactionId:" + callbackModel.getTransactionId() + " ,bankCode:" +
//                    callbackModel.getBankCode() + " ,appid:" + callbackModel.getAppid() +
//                    GlobalTransaction.Fields.TRANSACTIONSTATUS.getValue() + " ," +
//                    callbackModel.getTransactionObject().getString(GlobalTransaction.Fields.TRANSACTIONSTATUS.getValue()));
//            return callbackModel.getOutput().returnResponseObject(ret, return_status_codes);
//        }
//        if (TransactionStatus.CALLBACK_VALIDITY_OK.getValue().equalsIgnoreCase(callbackModel.getTransactionObject().getString
//                (GlobalTransaction.Fields.TRANSACTIONSTATUS.getValue()))) {
//
//            log.info(logCode + " ,getBankCallBack 5 ,transactionId:" + callbackModel.getTransactionId() + " ,bankCode:" +
//                    callbackModel.getBankCode() + " ,appid:" + callbackModel.getAppid() +
//                    GlobalTransaction.Fields.TRANSACTIONSTATUS.getValue() + ":" + return_status_codes);
//
//            //call user callbach
//            String userCallBackUrl = callbackModel.getTransactionObject().getString(GlobalTransaction.Fields.CALLBACKURL_USER.getValue());
//            String orderId = callbackModel.getTransactionObject().getString(GlobalTransaction.Fields.User_ORDERID.getValue());
//            String amount = callbackModel.getTransactionObject().getString(GlobalTransaction.Fields.AMOUNT.getValue());
//            String status = TransactionStatus.NOTCONFIRM.getValue();
//            String transactionId = callbackModel.getTransactionId();
//            String bankCode = callbackModel.getBankCode();
//            try {
//                boolean res = Api24.userCallBackCreator(userCallBackUrl
//                        , orderId
//                        , amount
//                        , bankCode
//                        , status);
//
//                if (!res) {
//                    upDoc.put(GlobalTransaction.Fields.TRANSACTIONSTATUS.getValue(), TransactionStatus.CALLBACK_ERROR_USEER.getValue());
//                    globalTransaction.updateWithObjectId(callbackModel.getObjectId(), upDoc);
//                    return_status_codes = Return_Status_Codes_Payment.PAYMENT_USER_CALLBACK_FAILED;
//                    log.info(logCode + " ,getBankCallBack 3 ,transactionId:" + callbackModel.getTransactionId() + " ,bankCode:" +
//                            callbackModel.getBankCode() + " ,appid:" + callbackModel.getAppid() +
//                            " ," + GlobalTransaction.Fields.TRANSACTIONSTATUS.getValue() + ":" +
//                            TransactionStatus.CALLBACK_ERROR_USEER.getValue());
//                } else {
//                    log.info(logCode + " ,getBankCallBack 1 ,transactionId:" + callbackModel.getTransactionId() + " ,bankCode:" +
//                            callbackModel.getBankCode() + " ,appid:" + callbackModel.getAppid() +
//                            " ," + GlobalTransaction.Fields.TRANSACTIONSTATUS.getValue() + ":" +
//                            TransactionStatus.NOTCONFIRM.getValue());
//                    upDoc.put(GlobalTransaction.Fields.TRANSACTIONSTATUS.getValue(), TransactionStatus.NOTCONFIRM.getValue());
//                    globalTransaction.updateWithObjectId(callbackModel.getObjectId(), upDoc);
//                    return_status_codes = Return_Status_Codes_Payment.PAYMENT_IS_OK;
//                }
//            } catch (Exception e) {
//                e.printStackTrace();
//                upDoc.put(GlobalTransaction.Fields.TRANSACTIONSTATUS.getValue(), TransactionStatus.CALLBACK_ERROR_USEER.getValue());
//                upDoc.put(GlobalTransaction.Fields.ERROR.getValue(), logCode + " ,getBankCallBack 4 ," + e.toString());
//                globalTransaction.updateWithObjectId(callbackModel.getObjectId(), upDoc);
//                return_status_codes = Return_Status_Codes_Payment.PAYMENT_USER_CALLBACK_FAILED;
//                log.info(logCode + " ,getBankCallBack 4 ,transactionId:" + callbackModel.getTransactionId() + " ,bankCode:" +
//                        callbackModel.getBankCode() + " ,appid:" + callbackModel.getAppid() +
//                        GlobalTransaction.Fields.TRANSACTIONSTATUS.getValue() + ":" +
//                        TransactionStatus.CALLBACK_ERROR_USEER.getValue()
//                        + " ," + GlobalTransaction.Fields.ERROR.getValue() + ":" + e.toString());
//            }
//        }
//        ret = callbackModel.getOutput().returnResponseObject(ret, return_status_codes);
//        return ret;
//    }
//
//    @Override
//    public GateWay.BankList.Builder listBank(BankListModel bankListModel) {
//        GateWay.BankList.Builder ret = GateWay.BankList.newBuilder();
//
//        IReturn_Status_Codes return_status_codes = bankListModel.getReturn_status_codes();
//        if (return_status_codes.getCode() != Return_Status_Codes_Common.ok_validform.getCode()) {
//            return bankListModel.getOutput().returnResponseObject(ret, return_status_codes);
//        }
//        GatewayConfig gatewayConfig = new GatewayConfig(bankListModel.getOutput().getMongodb_app());
//        List<Document> banks = gatewayConfig.findAll();
//        for (Document document : banks) {
//            if (document.getBoolean(GatewayConfig.Fields.ISACTIVE.getValue()) == true) {
//                GateWay.BankGet.Builder builder = gatewayConfig.getBankName(document);
//                ret.addData(builder);
//            }
//        }
//        return bankListModel.getOutput().returnResponseObject(ret, Return_Status_Codes_Common.SC_OK);
//    }
//
//    @Override
//    public GateWay.ConfirmTransaction.Builder confirmTransaction(ConfirmTransactionModel confirmTransactionModel) {
//        GateWay.ConfirmTransaction.Builder ret = GateWay.ConfirmTransaction.newBuilder();
//        IReturn_Status_Codes return_status_codes = confirmTransactionModel.getReturn_status_codes();
//        if (return_status_codes.getCode() != Return_Status_Codes_Common.ok_validform.getCode()) {
//            return confirmTransactionModel.getOutput().returnResponseObject(ret, return_status_codes);
//        }
//        return_status_codes = Return_Status_Codes_Payment.VALIDATE_USER_IS_OK;
//        return confirmTransactionModel.getOutput().returnResponseObject(ret, return_status_codes);
//    }
//}

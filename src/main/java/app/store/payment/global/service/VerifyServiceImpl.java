//package app.store.payment.global.service;
//
//import com.vasl.vaslapp.common.global.interfaces.IReturn_Status_Codes;
//import com.vasl.vaslapp.modules.bankgateway.global.enums.Return_Status_Codes_Payment;
//import com.vasl.vaslapp.modules.bankgateway.global.enums.TransactionStatus;
//import com.vasl.vaslapp.modules.bankgateway.global.mongo.repositories.acm_app.GatewayConfig;
//import com.vasl.vaslapp.modules.bankgateway.global.mongo.repositories.acm_app.GlobalTransaction;
//import com.vasl.vaslapp.modules.bankgateway.global.service.webServices.Mellat;
//import com.vasl.vaslapp.modules.bankgateway.global.service.webServices.Tejarat;
//import com.vasl.vaslapp.modules.bankgateway.global.service.webServices.Zarinpal;
//import org.bson.Document;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//
//import java.util.Map;
//import java.util.concurrent.ConcurrentHashMap;
//
//
//public class VerifyServiceImpl {
//
//    public static String logCode = "VerifyServiceImplClass";
//    private static Logger log = LoggerFactory.getLogger(Zarinpal.class);
//    private Zarinpal zar;
//    private Mellat mellat;
//    private Tejarat tejarat;
//
//    public VerifyServiceImpl(Zarinpal zar, Mellat mellat, Tejarat tejarat) {
//        this.zar = zar;
//        this.mellat = mellat;
//        this.tejarat = tejarat;
//    }
//
//    public VerifyServiceImpl() {
//    }
//
//    public Document zarinpalVerify(Document transaction, Document bank) {
//        Map<String, String> result = new ConcurrentHashMap<>();
//        Document upDoc = new Document();
//        String statusRespHolder = "-1";
//        String refIdRespHolder = "-1";
//        try {
//            String merchantid = bank.getString(GatewayConfig.Fields.MERCHANT.getValue());
//            String authority = transaction.getString(GlobalTransaction.Fields.AUTHORITY.getValue());
//            String amountString = transaction.getString(GlobalTransaction.Fields.AMOUNT.getValue());
//            Integer amount = Integer.valueOf(amountString);
//            result = zar.PaymentVerification(
//                    merchantid,
//                    amount,
//                    authority,
//                    bank.getString(GatewayConfig.Fields.PAYMENT_URL.getValue()),
//                    transaction.getObjectId(GlobalTransaction.Fields.ID.getValue()));
//
//        } catch (Exception ex) {
//            upDoc.put("message", Return_Status_Codes_Payment.PAYMENT_ERROR_VERIFICATION_REQUEST_FAILED.getCode());
//            upDoc.put(GlobalTransaction.Fields.TRANSACTIONSTATUS.getValue(), TransactionStatus.CALLBACK_VALIDITY_EXCEPTION_BANK.getValue());
//            upDoc.put(GlobalTransaction.Fields.ERROR.getValue(), logCode + " zarinpalVerify 2 : " + ex.toString());
//            String message = logCode + " zarinpalVerify 2 : " + " ,transactionID: " +
//                    (transaction.getObjectId(GlobalTransaction.Fields.ID.getValue())).toString() +
//                    " ," + GlobalTransaction.Fields.ERROR.getValue() + " : " + ex.toString() +
//                    " ," + GlobalTransaction.Fields.TRANSACTIONSTATUS.getValue() +
//                    " : " + TransactionStatus.CALLBACK_VALIDITY_EXCEPTION_BANK.getValue();
//            log.error(message);
//            return upDoc;
//        }
//        try {
//            if (result == null) {
//                upDoc.put("message", Return_Status_Codes_Payment.PAYMENT_ERROR_VERIFICATION_REQUEST_FAILED.getCode());
//                upDoc.put(GlobalTransaction.Fields.TRANSACTIONSTATUS.getValue(), TransactionStatus.CALLBACK_VALIDITY_EXCEPTION.getValue());
//                upDoc.put(GlobalTransaction.Fields.ERROR.getValue(), logCode
//                        + " zarinpalVerify 3 : verification result is null");
//                String message = logCode + " zarinpalVerify 3 : " + " ,transactionID: " +
//                        (transaction.getObjectId(GlobalTransaction.Fields.ID.getValue())).toString() +
//                        " ," + GlobalTransaction.Fields.ERROR.getValue() + " : verification result is null" +
//                        " ," + GlobalTransaction.Fields.TRANSACTIONSTATUS.getValue() +
//                        " : " + TransactionStatus.CALLBACK_VALIDITY_EXCEPTION.getValue();
//                log.error(message);
//                return upDoc;
//            }
//            if (result != null && result.get(GlobalTransaction.Fields.STATUSHOLDER.getValue()).length() > 1)
//                statusRespHolder = result.get(GlobalTransaction.Fields.STATUSHOLDER.getValue());
//            if (result != null && result.get(GlobalTransaction.Fields.REFIDHOLDER.getValue()).length() > 1)
//                refIdRespHolder = result.get(GlobalTransaction.Fields.REFIDHOLDER.getValue());
//
//            if (statusRespHolder.equalsIgnoreCase("101") || statusRespHolder.equalsIgnoreCase("100")) {
//                upDoc.put(GlobalTransaction.Fields.STATUSHOLDER.getValue(), statusRespHolder);
//                upDoc.put(GlobalTransaction.Fields.REFIDHOLDER.getValue(), refIdRespHolder);
//                upDoc.put(GlobalTransaction.Fields.TRANSACTIONSTATUS.getValue(), TransactionStatus.CALLBACK_VALIDITY_OK.getValue());
//                String message = logCode + " zarinpalVerify 1 : " + " ,transactionID: " +
//                        (transaction.getObjectId(GlobalTransaction.Fields.ID.getValue())).toString() +
//                        " ," + GlobalTransaction.Fields.TRANSACTIONSTATUS.getValue() +
//                        " : " + TransactionStatus.CALLBACK_VALIDITY_OK.getValue() +
//                        " ," + GlobalTransaction.Fields.STATUSHOLDER.getValue() + " : " + statusRespHolder +
//                        " ," + GlobalTransaction.Fields.REFIDHOLDER.getValue() + " : " + refIdRespHolder;
//                log.info(message);
//                return upDoc;
//            } else {
//                IReturn_Status_Codes return_status_codes = zar.answerByZarinpal(statusRespHolder);
//                upDoc.put("message", return_status_codes.getCode());
//                upDoc.put(GlobalTransaction.Fields.ERROR.getValue(), logCode + " zarinpalVerify 4 : "
//                        + return_status_codes.getMessage_key());
//                upDoc.put(GlobalTransaction.Fields.STATUSHOLDER.getValue(), statusRespHolder);
//                upDoc.put(GlobalTransaction.Fields.REFIDHOLDER.getValue(), refIdRespHolder);
//                upDoc.put(GlobalTransaction.Fields.TRANSACTIONSTATUS.getValue(), TransactionStatus.CALLBACK_VALIDITY_ERROR.getValue());
//
//                String message = logCode + " zarinpalVerify 4 : " + " ,transactionID: " +
//                        (transaction.getObjectId(GlobalTransaction.Fields.ID.getValue())).toString() +
//                        " ," + GlobalTransaction.Fields.ERROR.getValue() + " : " + return_status_codes.toString() +
//                        " ," + GlobalTransaction.Fields.TRANSACTIONSTATUS.getValue() +
//                        " : " + TransactionStatus.CALLBACK_VALIDITY_ERROR.getValue();
//                log.info(message);
//                return upDoc;
//            }
//        } catch (Exception ex) {
//            upDoc.put("message", Return_Status_Codes_Payment.PAYMENT_ERROR_INTERNAL_ERROR.getCode());
//            upDoc.put(GlobalTransaction.Fields.TRANSACTIONSTATUS.getValue(), TransactionStatus.CALLBACK_VALIDITY_EXCEPTION.getValue());
//            upDoc.put(GlobalTransaction.Fields.ERROR.getValue(), logCode + " zarinpalVerify 5 : " + ex.toString());
//            String message = logCode + " zarinpalVerify 5 : " + " ,transactionID: " +
//                    (transaction.getObjectId(GlobalTransaction.Fields.ID.getValue())).toString() +
//                    " ," + GlobalTransaction.Fields.ERROR.getValue() + " : " + ex.toString() +
//                    " ," + GlobalTransaction.Fields.TRANSACTIONSTATUS.getValue() +
//                    " : " + TransactionStatus.CALLBACK_VALIDITY_EXCEPTION.getValue();
//            log.error(message);
//            return upDoc;
//        }
//    }
//
//    public Document mellatVerify(Document transaction, Document bank) {
//        Map<String, String> result = new ConcurrentHashMap<>();
//        Document upDoc = new Document();
//        String statusRespHolder = "-1";
//        try {
//            result = mellat.bpVerifyRequest(
//                    bank.getString(GatewayConfig.Fields.TERMINALID.getValue()),
//                    bank.getString(GatewayConfig.Fields.USERNAME.getValue()),
//                    bank.getString(GatewayConfig.Fields.PASSWORD.getValue()),
//                    transaction.getString(GlobalTransaction.Fields.PAY_ORDERID.getValue()),
//                    transaction.getString(GlobalTransaction.Fields.PAY_ORDERID.getValue()),
//                    transaction.getString(GlobalTransaction.Fields.SALE_REFRENCEID.getValue()),
//                    bank.getString(GatewayConfig.Fields.PAYMENT_URL.getValue()),
//                    transaction.getObjectId(GlobalTransaction.Fields.ID.getValue()));
//
//        } catch (Exception ex) {
//            upDoc.put("message", Return_Status_Codes_Payment.PAYMENT_ERROR_VERIFICATION_REQUEST_FAILED.getCode());
//            upDoc.put(GlobalTransaction.Fields.TRANSACTIONSTATUS.getValue(), TransactionStatus.CALLBACK_VALIDITY_EXCEPTION_BANK.getValue());
//            upDoc.put(GlobalTransaction.Fields.ERROR.getValue(), logCode + " mellatVerify 2 : " + ex.toString());
//
//            String message = logCode + " mellatVerify 2 : " + " ,transactionID: " +
//                    (transaction.getObjectId(GlobalTransaction.Fields.ID.getValue())).toString() +
//                    " ," + GlobalTransaction.Fields.ERROR.getValue() + " : " + ex.toString() +
//                    " ," + GlobalTransaction.Fields.TRANSACTIONSTATUS.getValue() +
//                    " : " + TransactionStatus.CALLBACK_VALIDITY_EXCEPTION_BANK.getValue();
//            log.error(message);
//
//            return upDoc;
//        }
//        try {
//            if (result == null) {
//                upDoc.put("message", Return_Status_Codes_Payment.PAYMENT_ERROR_VERIFICATION_REQUEST_FAILED.getCode());
//                upDoc.put(GlobalTransaction.Fields.TRANSACTIONSTATUS.getValue(), TransactionStatus.CALLBACK_VALIDITY_EXCEPTION.getValue());
//                upDoc.put(GlobalTransaction.Fields.ERROR.getValue(), logCode
//                        + " mellatVerify 3 : verification result is null");
//
//                String message = logCode + " mellatVerify 3 : " + " ,transactionID: " +
//                        (transaction.getObjectId(GlobalTransaction.Fields.ID.getValue())).toString() +
//                        " ," + GlobalTransaction.Fields.ERROR.getValue() + " : verification result is null" +
//                        " ," + GlobalTransaction.Fields.TRANSACTIONSTATUS.getValue() +
//                        " : " + TransactionStatus.CALLBACK_VALIDITY_EXCEPTION.getValue();
//                log.error(message);
//                return upDoc;
//            }
//            if (result != null && result.get(GlobalTransaction.Fields.STATUSHOLDER.getValue()).length() > 1)
//                statusRespHolder = result.get(GlobalTransaction.Fields.STATUSHOLDER.getValue());
//
//            if (statusRespHolder.equalsIgnoreCase("0")) {
//                upDoc.put(GlobalTransaction.Fields.STATUSHOLDER.getValue(), statusRespHolder);
//                upDoc.put(GlobalTransaction.Fields.TRANSACTIONSTATUS.getValue(), TransactionStatus.CALLBACK_VALIDITY_OK.getValue());
//
//                String message = logCode + " mellatVerify 1 : " + " ,transactionID: " +
//                        (transaction.getObjectId(GlobalTransaction.Fields.ID.getValue())).toString() +
//                        " ," + GlobalTransaction.Fields.TRANSACTIONSTATUS.getValue() +
//                        " : " + TransactionStatus.CALLBACK_VALIDITY_OK.getValue() +
//                        " ," + GlobalTransaction.Fields.STATUSHOLDER.getValue() + " : " + statusRespHolder;
//                log.info(message);
//
//                return upDoc;
//            } else {
//                IReturn_Status_Codes return_status_codes = mellat.answerByMellats(statusRespHolder);
//                upDoc.put("message", return_status_codes.getCode());
//                upDoc.put(GlobalTransaction.Fields.STATUSHOLDER.getValue(), statusRespHolder);
//                upDoc.put(GlobalTransaction.Fields.ERROR.getValue(), logCode + " mellatVerify 4 : " + return_status_codes.getMessage_key());
//                upDoc.put(GlobalTransaction.Fields.TRANSACTIONSTATUS.getValue(), TransactionStatus.CALLBACK_VALIDITY_ERROR.getValue());
//
//                String message = logCode + " mellatVerify 4 : " + " ,transactionID: " +
//                        (transaction.getObjectId(GlobalTransaction.Fields.ID.getValue())).toString() +
//                        " ," + GlobalTransaction.Fields.ERROR.getValue() + " : " + return_status_codes.toString() +
//                        " ," + GlobalTransaction.Fields.TRANSACTIONSTATUS.getValue() +
//                        " : " + TransactionStatus.CALLBACK_VALIDITY_ERROR.getValue();
//                log.info(message);
//
//                return upDoc;
//            }
//        } catch (Exception ex) {
//            upDoc.put("message", Return_Status_Codes_Payment.PAYMENT_ERROR_INTERNAL_ERROR.getCode());
//            upDoc.put(GlobalTransaction.Fields.TRANSACTIONSTATUS.getValue(), TransactionStatus.CALLBACK_VALIDITY_EXCEPTION.getValue());
//            upDoc.put(GlobalTransaction.Fields.ERROR.getValue(), logCode + " mellatVerify 5 : " + ex.toString());
//
//            String message = logCode + " mellatVerify 5 : " + " ,transactionID: " +
//                    (transaction.getObjectId(GlobalTransaction.Fields.ID.getValue())).toString() +
//                    " ," + GlobalTransaction.Fields.ERROR.getValue() + " : " + ex.toString() +
//                    " ," + GlobalTransaction.Fields.TRANSACTIONSTATUS.getValue() +
//                    " : " + TransactionStatus.CALLBACK_VALIDITY_EXCEPTION.getValue();
//            log.error(message);
//
//            return upDoc;
//        }
//
//    }
//
//    public Document tejaratVerify(Document transaction, Document bank) {//todo
//        Map<String, String> result = new ConcurrentHashMap<>();
//        Document upDoc = new Document();
//        String statusRespHolder = "-1";
//        try {
//            result = tejarat.kicccPaymentsVerification(
//                    transaction.getString(GlobalTransaction.Fields.REFIDHOLDER.getValue()),
//                    bank.getString(GatewayConfig.Fields.MERCHANT.getValue()),
//                    transaction.getString(GlobalTransaction.Fields.SALE_REFRENCEID.getValue()),
//                    bank.getString(GatewayConfig.Fields.PAYMENT_URL.getValue()),
//                    transaction.getObjectId(GatewayConfig.Fields.ID.getValue()));
//
//        } catch (Exception ex) {
//            upDoc.put("message", Return_Status_Codes_Payment.PAYMENT_ERROR_VERIFICATION_REQUEST_FAILED.getCode());
//            upDoc.put(GlobalTransaction.Fields.TRANSACTIONSTATUS.getValue(), TransactionStatus.CALLBACK_VALIDITY_EXCEPTION_BANK.getValue());
//            upDoc.put(GlobalTransaction.Fields.ERROR.getValue(), logCode + " tejaratVerify 2 : " + ex.toString());
//
//            String message = logCode + " tejaratVerify 2 : " + " ,transactionID: " +
//                    (transaction.getObjectId(GlobalTransaction.Fields.ID.getValue())).toString() +
//                    " ," + GlobalTransaction.Fields.ERROR.getValue() + " : " + ex.toString() +
//                    " ," + GlobalTransaction.Fields.TRANSACTIONSTATUS.getValue() +
//                    " : " + TransactionStatus.CALLBACK_VALIDITY_EXCEPTION_BANK.getValue();
//            log.error(message);
//
//            return upDoc;
//        }
//        try {
//            if (result == null) {
//                upDoc.put("message", Return_Status_Codes_Payment.PAYMENT_ERROR_VERIFICATION_REQUEST_FAILED.getCode());
//                upDoc.put(GlobalTransaction.Fields.TRANSACTIONSTATUS.getValue(), TransactionStatus.CALLBACK_VALIDITY_EXCEPTION.getValue());
//                upDoc.put(GlobalTransaction.Fields.ERROR.getValue(), logCode
//                        + " tejaratVerify 3 : verification result is null");
//                String message = logCode + " tejaratVerify 3 : " + " ,transactionID: " +
//                        (transaction.getObjectId(GlobalTransaction.Fields.ID.getValue())).toString() +
//                        " ," + GlobalTransaction.Fields.ERROR.getValue() + " : verification result is null" +
//                        " ," + GlobalTransaction.Fields.TRANSACTIONSTATUS.getValue() +
//                        " : " + TransactionStatus.CALLBACK_VALIDITY_EXCEPTION.getValue();
//                log.error(message);
//
//                return upDoc;
//            }
//            if (result != null && result.get(GlobalTransaction.Fields.STATUSHOLDER.getValue()).length() > 1)
//                statusRespHolder = result.get(GlobalTransaction.Fields.STATUSHOLDER.getValue());
//
//            if (statusRespHolder.equalsIgnoreCase("100")) {
//                upDoc.put(GlobalTransaction.Fields.STATUSHOLDER.getValue(), statusRespHolder);
//                upDoc.put(GlobalTransaction.Fields.TRANSACTIONSTATUS.getValue(), TransactionStatus.CALLBACK_VALIDITY_OK.getValue());
//
//                String message = logCode + " tejaratVerify 1 : " + " ,transactionID: " +
//                        (transaction.getObjectId(GlobalTransaction.Fields.ID.getValue())).toString() +
//                        " ," + GlobalTransaction.Fields.TRANSACTIONSTATUS.getValue() +
//                        " : " + TransactionStatus.CALLBACK_VALIDITY_OK.getValue() +
//                        " ," + GlobalTransaction.Fields.STATUSHOLDER.getValue() + " : " + statusRespHolder;
//                log.info(message);
//
//                return upDoc;
//            } else {
//                IReturn_Status_Codes return_status_codes = tejarat.answerByTejarat(statusRespHolder);
//                upDoc.put("message", return_status_codes.getCode());
//                upDoc.put(GlobalTransaction.Fields.STATUSHOLDER.getValue(), statusRespHolder);
//                upDoc.put(GlobalTransaction.Fields.ERROR.getValue(), logCode + " tejaratVerify 4 : " + return_status_codes.getMessage_key());
//                upDoc.put(GlobalTransaction.Fields.TRANSACTIONSTATUS.getValue(), TransactionStatus.CALLBACK_VALIDITY_ERROR.getValue());
//
//                String message = logCode + " tejaratVerify 4 : " + " ,transactionID: " +
//                        (transaction.getObjectId(GlobalTransaction.Fields.ID.getValue())).toString() +
//                        " ," + GlobalTransaction.Fields.ERROR.getValue() + " : " + return_status_codes.toString() +
//                        " ," + GlobalTransaction.Fields.TRANSACTIONSTATUS.getValue() +
//                        " : " + TransactionStatus.CALLBACK_VALIDITY_ERROR.getValue();
//                log.info(message);
//
//                return upDoc;
//            }
//        } catch (Exception ex) {
//            upDoc.put("message", Return_Status_Codes_Payment.PAYMENT_ERROR_INTERNAL_ERROR.getCode());
//            upDoc.put(GlobalTransaction.Fields.TRANSACTIONSTATUS.getValue(), TransactionStatus.CALLBACK_VALIDITY_EXCEPTION.getValue());
//            upDoc.put(GlobalTransaction.Fields.ERROR.getValue(), logCode + " tejaratVerify 5 : " + ex.toString());
//
//            String message = logCode + " tejaratVerify 5 : " + " ,transactionID: " +
//                    (transaction.getObjectId(GlobalTransaction.Fields.ID.getValue())).toString() +
//                    " ," + GlobalTransaction.Fields.ERROR.getValue() + " : " + ex.toString() +
//                    " ," + GlobalTransaction.Fields.TRANSACTIONSTATUS.getValue() +
//                    " : " + TransactionStatus.CALLBACK_VALIDITY_EXCEPTION.getValue();
//            log.error(message);
//
//            return upDoc;
//        }
//
//    }
//}

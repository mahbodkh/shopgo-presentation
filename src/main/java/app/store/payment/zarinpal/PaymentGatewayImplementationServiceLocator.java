package app.store.payment.zarinpal;

public class PaymentGatewayImplementationServiceLocator extends org.apache.axis.client.Service implements PaymentGatewayImplementationService {

    // Use to get a proxy class for PaymentGatewayImplementationServicePort
    private String PaymentGatewayImplementationServicePort_address = "https://www.zarinpal.com/pg/services/WebGate/service";
    // The WSDD service name defaults to the port name.
    private String PaymentGatewayImplementationServicePortWSDDServiceName = "PaymentGatewayImplementationServicePort";
    private java.util.HashSet ports = null;

    public PaymentGatewayImplementationServiceLocator() {
    }


    public PaymentGatewayImplementationServiceLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public PaymentGatewayImplementationServiceLocator(String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    public String getPaymentGatewayImplementationServicePortAddress() {
        return PaymentGatewayImplementationServicePort_address;
    }

    public String getPaymentGatewayImplementationServicePortWSDDServiceName() {
        return PaymentGatewayImplementationServicePortWSDDServiceName;
    }

    public void setPaymentGatewayImplementationServicePortWSDDServiceName(String name) {
        PaymentGatewayImplementationServicePortWSDDServiceName = name;
    }

    public PaymentGatewayImplementationServicePortType getPaymentGatewayImplementationServicePort() throws javax.xml.rpc.ServiceException {
        java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(PaymentGatewayImplementationServicePort_address);
        } catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getPaymentGatewayImplementationServicePort(endpoint);
    }

    public PaymentGatewayImplementationServicePortType getPaymentGatewayImplementationServicePort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            PaymentGatewayImplementationServiceBindingStub _stub = new PaymentGatewayImplementationServiceBindingStub(portAddress, this);
            _stub.setPortName(getPaymentGatewayImplementationServicePortWSDDServiceName());
            return _stub;
        } catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setPaymentGatewayImplementationServicePortEndpointAddress(String address) {
        PaymentGatewayImplementationServicePort_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (PaymentGatewayImplementationServicePortType.class.isAssignableFrom(serviceEndpointInterface)) {
                PaymentGatewayImplementationServiceBindingStub _stub = new PaymentGatewayImplementationServiceBindingStub(new java.net.URL(PaymentGatewayImplementationServicePort_address), this);
                _stub.setPortName(getPaymentGatewayImplementationServicePortWSDDServiceName());
                return _stub;
            }
        } catch (Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        String inputPortName = portName.getLocalPart();
        if ("PaymentGatewayImplementationServicePort".equals(inputPortName)) {
            return getPaymentGatewayImplementationServicePort();
        } else {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://zarinpal.com/", "PaymentGatewayImplementationService");
    }

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://zarinpal.com/", "PaymentGatewayImplementationServicePort"));
        }
        return ports.iterator();
    }

    /**
     * Set the endpoint address for the specified port name.
     */
    public void setEndpointAddress(String portName, String address) throws javax.xml.rpc.ServiceException {

        if ("PaymentGatewayImplementationServicePort".equals(portName)) {
            setPaymentGatewayImplementationServicePortEndpointAddress(address);
        } else { // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
     * Set the endpoint address for the specified port name.
     */
    public void setEndpointAddress(javax.xml.namespace.QName portName, String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
/**
 * PaymentGatewayImplementationServicePortType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package app.store.payment.zarinpal;

public interface PaymentGatewayImplementationServicePortType extends java.rmi.Remote {
    public void paymentRequest(String merchantID, int amount, String description, String email, String mobile, String callbackURL, javax.xml.rpc.holders.IntHolder status, javax.xml.rpc.holders.StringHolder authority) throws java.rmi.RemoteException;
    public void paymentRequestWithExtra(String merchantID, int amount, String description, String additionalData, String email, String mobile, String callbackURL, javax.xml.rpc.holders.IntHolder status, javax.xml.rpc.holders.StringHolder authority) throws java.rmi.RemoteException;
    public void paymentVerification(String merchantID, String authority, int amount, javax.xml.rpc.holders.IntHolder status, javax.xml.rpc.holders.LongHolder refID) throws java.rmi.RemoteException;
    public void paymentVerificationWithExtra(String merchantID, String authority, int amount, javax.xml.rpc.holders.IntHolder status, javax.xml.rpc.holders.LongHolder refID, javax.xml.rpc.holders.StringHolder extraDetail) throws java.rmi.RemoteException;
    public void getUnverifiedTransactions(String merchantID, javax.xml.rpc.holders.IntHolder status, javax.xml.rpc.holders.StringHolder authorities) throws java.rmi.RemoteException;
    public int refreshAuthority(String merchantID, String authority, int expireIn) throws java.rmi.RemoteException;
}

/**
 * PaymentGatewayImplementationService.java
 * <p>
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package app.store.payment.zarinpal;

public interface PaymentGatewayImplementationService extends javax.xml.rpc.Service {
    public String getPaymentGatewayImplementationServicePortAddress();

    public PaymentGatewayImplementationServicePortType getPaymentGatewayImplementationServicePort() throws javax.xml.rpc.ServiceException;

    public PaymentGatewayImplementationServicePortType getPaymentGatewayImplementationServicePort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}

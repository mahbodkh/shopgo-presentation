package app.store;

import app.store.persistance.CategoryRepository;
import app.store.persistance.ProductRepository;
import app.store.persistance.domain.Product;
import org.bson.Document;
import org.bson.types.ObjectId;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.lang.reflect.Type;

@RunWith(SpringRunner.class)
@SpringBootTest(
        classes = {StoreApplicationTests.class},
        webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT,
        properties = "spring.cloud.config.enabled=false")
@ActiveProfiles("test")
public class ProductRestController_InsertProductTest {

    @LocalServerPort
    private int port;

    TestRestTemplate restTemplate = new TestRestTemplate();

    HttpHeaders headers = new HttpHeaders();

    final String BASE_URL = "http://localhost:";
    final String PRODUCT_ADD_URL = "/product/add";

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private CategoryRepository categoryRepository;

    @Before
    public void start() {
        productRepository.deleteAll();
        categoryRepository.deleteAll();
        insertTestData();
    }

    @Test
    public void insertProductSimple() {
        System.out.printf("Its ok");

        HttpEntity<?> entity = new HttpEntity<>(headers);
        ResponseEntity<Product> response = restTemplate.exchange(
                BASE_URL + port + PRODUCT_ADD_URL
                , HttpMethod.POST
                , entity
                , new ParameterizedTypeReference<Product>() {
                    @Override
                    public Type getType() {
                        return super.getType();
                    }
                }
        );
        Product posts = response.getBody();
        assert response.getStatusCode() == HttpStatus.ACCEPTED;
        assert posts != null;


    }

    @After
    public void end() {
        productRepository.deleteAll();
        categoryRepository.deleteAll();
    }


    private void insertTestData() {
        Document category = new Document();
        ObjectId categoryObjectId = new ObjectId();
        category.put(CategoryRepository.Fields.ID.getValue(), categoryObjectId);
        category.put(CategoryRepository.Fields.NAME.getValue(), "CATEGORY_TEST");
        category.put(CategoryRepository.Fields.CODE.getValue(), 201);
        category.put(CategoryRepository.Fields.DESCRIPTION.getValue(), "CATEGORY_DESCRIPTION_TEST");
        categoryRepository.insert(category);

        Document product1 = new Document();
        product1.put(ProductRepository.Fields.NAME.getValue(), "PRODUCT_TEST");
        product1.put(ProductRepository.Fields.PRICE.getValue(), 2000);
        product1.put(ProductRepository.Fields.CATEGORY.getValue(), categoryObjectId);
        product1.put(ProductRepository.Fields.CATEGORIES.getValue(), category);
        product1.put(ProductRepository.Fields.WEIGHT.getValue(), 2);
        product1.put(ProductRepository.Fields.WIDTH.getValue(), 2);
        product1.put(ProductRepository.Fields.HEIGHT.getValue(), 2);
        product1.put(ProductRepository.Fields.COUNT.getValue(), 5);
        productRepository.insert(product1);

        Document product2 = new Document();
        product2.put(ProductRepository.Fields.NAME.getValue(), "PRODUCT_TEST2");
        product2.put(ProductRepository.Fields.PRICE.getValue(), 2000);
        product2.put(ProductRepository.Fields.CATEGORY.getValue(), categoryObjectId);
        product2.put(ProductRepository.Fields.CATEGORIES.getValue(), category);
        product2.put(ProductRepository.Fields.WEIGHT.getValue(), 2);
        product2.put(ProductRepository.Fields.WIDTH.getValue(), 2);
        product2.put(ProductRepository.Fields.HEIGHT.getValue(), 2);
        product2.put(ProductRepository.Fields.COUNT.getValue(), 5);
        productRepository.insert(product2);

    }
}
